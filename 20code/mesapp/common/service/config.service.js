let BASE_URL = ''


if (process.env.NODE_ENV == 'development') {
    // BASE_URL = 'http://127.0.0.1:8080/jeecg-boot' // 开发环境、、
	// BASE_URL = 'http://47.113.229.131:8099/' // 奕彬 益哲渝
	// BASE_URL = 'http://192.168.0.105:9999/' // 老胡 益哲渝
    BASE_URL = 'http://127.0.0.1:9999/' // 开发环境、
	// BASE_URL = 'http://47.113.229.131:8099/' // 奕彬 益哲渝
	// BASE_URL = 'http://192.168.0.147:9999/' // 奕彬 益哲渝
	// BASE_URL = 'http://localhost:9999/' // 奕彬 本地
	// BASE_URL = 'http://192.168.0.120:9999/' // 曹总 益哲渝
	// BASE_URL = 'http://192.168.3.8:9999/' // 曹总
	// BASE_URL = 'http://192.168.10.2:9999/' // YZYMES
} else {
	// BASE_URL = 'http://boot.jeecg.org:8080/jeecg-boot' // 生产环境
	BASE_URL = 'http://192.168.10.2:9999/' // YZYMES
}
let staticDomainURL = BASE_URL+ '/sys/common/static';

const configService = {
	apiUrl: BASE_URL,
	staticDomainURL: staticDomainURL
};

export default configService
