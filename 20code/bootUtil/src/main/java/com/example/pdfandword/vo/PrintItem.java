package com.example.pdfandword.vo;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="测试DEMO对象", description="测试DEMO")
public class PrintItem {
    private String data01;//二维码
    private String data02;//序号
    private String data03;//工序
    private String data04;//操作员
    private String data05;//数量
    private String data06;
    private String data07;
    private String data08;
    private String data09;
    private String data10;
    private String data11;
    private String data12;
    private String data13;
    private String data14;
    private String data15;
    private String data16;
    private String data17;
    private String data18;
    private String data19;
    private String data20;
    private String data21;
    private String data22;
    private String data23;
    private String data24;
    private String data25;
    private String data26;
    private String data27;
    private String data28;
    private String data29;
    private String data30;
    private String data31;
    private String data32;
    private String data33;
    private String data34;
    private String data35;
    private String data36;
    private String data37;
    private String data38;
    private String data39;
    private String data40;
    private String data41;
    private String data42;
    private String data43;
    private String data44;
    private String data45;
    private String data46;
    private String data47;
    private String data48;
    private String data49;
    private String data50;
    private String data51;
    private String data52;
    private String data53;
    private String data54;
    private String data55;
    private String data56;
    private String data57;
    private String data58;
    private String data59;
    private String data60;
    private String data61;
    private String data62;
    private String data63;
    private String data64;
    private String data65;
    private String data66;
    private String data67;
    private String data68;
    private String data69;
    private String data70;
    private String data71;
    private String data72;
    private String data73;
}
