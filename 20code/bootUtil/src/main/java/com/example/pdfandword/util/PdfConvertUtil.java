package com.example.pdfandword.util;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.ComThread;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

import java.io.File;

public class PdfConvertUtil {
    public static boolean word2PDF(String inputFile, String pdfFile) {
        ActiveXComponent app = new ActiveXComponent("Word.Application");
        try {
            app.setProperty("Visible", false);
            Dispatch docs = app.getProperty("Documents").toDispatch();

            Dispatch doc = Dispatch.call(docs, "Open", new Object[]{inputFile, false, true}).toDispatch();
            Dispatch.call(doc, "ExportAsFixedFormat", new Object[]{pdfFile, 17});
            Dispatch.call(doc, "Close", new Object[]{false});
            app.invoke("Quit", 0);
            return true;
        } catch (Exception var6) {
            app.invoke("Quit", 0);
            return false;
        }
    }
    public boolean printword(String inputFile) {
        synchronized (this) {
            if (inputFile.isEmpty() || inputFile.length() < 1) {
                System.out.println("无文档文件");
                return false;
            }
            System.out.println("开始打印：" + inputFile);
            ComThread.InitSTA();
            ActiveXComponent word = new ActiveXComponent("Word.Application");//创建 ActiveX部件对象,这里是Word的
            Dispatch doc = null;
            Dispatch.put(word, "Visible", new Variant(false));
            Dispatch docs = word.getProperty("Documents").toDispatch();
            doc = Dispatch.call(docs, "Open", inputFile).toDispatch();

            try {
                Dispatch.call(doc, "PrintOut");//打印
                System.out.println("完成打印：" + inputFile);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("打印失败");
            } finally {
                try {
                    if (doc != null) {
                        Dispatch.call(doc, "Close", new Variant(true));//word文档关闭
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                    return false;
                }
                //退出
                word.invoke("Quit", new Variant[0]);
                //释放资源
                ComThread.Release();
                ComThread.quitMainSTA();
            }
            return  true;
        }
    }
    public static boolean  printexcel(String inputFile) {
        boolean returnFlg = false;
        ComThread.InitSTA();
        ActiveXComponent xl = new ActiveXComponent("Excel.Application");
        try {
            // 不打开文档
//			Dispatch.put(xl, "Visible", new Variant(true));
            Dispatch.put(xl, "Visible", new Variant(false));
            Dispatch workbooks = xl.getProperty("Workbooks").toDispatch();
            // win下路径处理(把根目录前的斜杠删掉)
//			filePath = filePath.replace("D:/","D:/");
            // 判断文件是否存在
            boolean fileExistFlg = fileExist(inputFile);
            if (fileExistFlg) {
                Dispatch excel=Dispatch.call(workbooks,"Open",inputFile).toDispatch();
                Dispatch.get(excel,"PrintOut");
                returnFlg = true;

                Dispatch.call(excel, "Close", new Variant(true));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 始终释放资源
            xl.invoke("Quit", new Variant[] {});
            ComThread.Release();
        }
        return returnFlg;
    }
    public static boolean excel2PDF(String inputFile, String pdfFile) {
        ComThread.InitSTA(true);
        ActiveXComponent app = new ActiveXComponent("Excel.Application");
        try {
            app.setProperty("Visible", false);
            app.setProperty("AutomationSecurity", new Variant(3));
            Dispatch excels = app.getProperty("Workbooks").toDispatch();
            Dispatch excel = Dispatch.invoke(excels, "Open", 1, new Object[]{inputFile, new Variant(false), new Variant(false)}, new int[9]).toDispatch();
            Dispatch.invoke(excel, "ExportAsFixedFormat", 1, new Object[]{new Variant(0), pdfFile, new Variant(0)}, new int[1]);
            Dispatch.call(excel, "Close", new Object[]{false});
            if (app != null) {
                app.invoke("Quit", new Variant[0]);
                app = null;
            }

            ComThread.Release();
            return true;
        } catch (Exception var6) {
            app.invoke("Quit");
            return false;
        }
    }

    public static boolean ppt2PDF(String inputFile, String pdfFile) {
        ActiveXComponent app = new ActiveXComponent("PowerPoint.Application");

        try {
            Dispatch ppts = app.getProperty("Presentations").toDispatch();
            Dispatch ppt = Dispatch.call(ppts, "Open", new Object[]{inputFile, true, true, false}).toDispatch();
            Dispatch.call(ppt, "SaveAs", new Object[]{pdfFile, 32});
            Dispatch.call(ppt, "Close");
            app.invoke("Quit");
            return true;
        } catch (Exception var6) {
            app.invoke("Quit");
            return false;
        }
    }
    /**
     * 判断文件是否存在.
     * @param filePath  文件路径
     * @return
     */
    private static boolean fileExist(String filePath) {
        boolean flag = false;
        try {
            File file = new File(filePath);
            flag = file.exists();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }
}
