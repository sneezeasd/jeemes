package org.jeecg.modules.mes.users.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.users.entity.MesUserInfo;
import org.jeecg.modules.mes.users.service.IMesUserInfoService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 用户管理
 * @Author: jeecg-boot
 * @Date: 2021-02-24
 * @Version: V1.0
 */
@Api(tags = "用户管理")
@RestController
@RequestMapping("/mytest/mesUserInfo")
@Slf4j
public class MesUserInfoController extends JeecgController<MesUserInfo, IMesUserInfoService> {
    @Autowired
    private IMesUserInfoService mesUserInfoService;

    /**
     * 分页列表查询
     *
     * @param mesUserInfo
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "用户管理-分页列表查询")
    @ApiOperation(value = "用户管理-分页列表查询", notes = "用户管理-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(MesUserInfo mesUserInfo,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<MesUserInfo> queryWrapper = QueryGenerator.initQueryWrapper(mesUserInfo, req.getParameterMap());
        Page<MesUserInfo> page = new Page<MesUserInfo>(pageNo, pageSize);
        IPage<MesUserInfo> pageList = mesUserInfoService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

    /**
     * 添加
     *
     * @param mesUserInfo
     * @return
     */
    @AutoLog(value = "用户管理-添加")
    @ApiOperation(value = "用户管理-添加", notes = "用户管理-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody MesUserInfo mesUserInfo) {
        mesUserInfoService.save(mesUserInfo);
        return Result.ok("添加成功！");
    }

    /**
     * 编辑
     *
     * @param mesUserInfo
     * @return
     */
    @AutoLog(value = "用户管理-编辑")
    @ApiOperation(value = "用户管理-编辑", notes = "用户管理-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody MesUserInfo mesUserInfo) {
        mesUserInfoService.updateById(mesUserInfo);
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "用户管理-通过id删除")
    @ApiOperation(value = "用户管理-通过id删除", notes = "用户管理-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        mesUserInfoService.removeById(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "用户管理-批量删除")
    @ApiOperation(value = "用户管理-批量删除", notes = "用户管理-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.mesUserInfoService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "用户管理-通过id查询")
    @ApiOperation(value = "用户管理-通过id查询", notes = "用户管理-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        MesUserInfo mesUserInfo = mesUserInfoService.getById(id);
        if (mesUserInfo == null) {
            return Result.error("未找到对应数据");
        }
        return Result.ok(mesUserInfo);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param mesUserInfo
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesUserInfo mesUserInfo) {
        return super.exportXls(request, mesUserInfo, MesUserInfo.class, "用户管理");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesUserInfo.class);
    }

}
