package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesMaterialtableItem;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataMaterialtable;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 主数据—料表管理
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface IMesChiefdataMaterialtableService extends IService<MesChiefdataMaterialtable> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(MesChiefdataMaterialtable mesChiefdataMaterialtable,List<MesMaterialtableItem> mesMaterialtableItemList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(MesChiefdataMaterialtable mesChiefdataMaterialtable,List<MesMaterialtableItem> mesMaterialtableItemList);
	
	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	
}
