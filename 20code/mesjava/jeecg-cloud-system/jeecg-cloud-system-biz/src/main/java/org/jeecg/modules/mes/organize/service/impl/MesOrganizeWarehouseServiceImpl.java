package org.jeecg.modules.mes.organize.service.impl;

import org.jeecg.modules.mes.organize.entity.MesOrganizeWarehouse;
import org.jeecg.modules.mes.organize.mapper.MesOrganizeWarehouseMapper;
import org.jeecg.modules.mes.organize.service.IMesOrganizeWarehouseService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 组织—仓库
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesOrganizeWarehouseServiceImpl extends ServiceImpl<MesOrganizeWarehouseMapper, MesOrganizeWarehouse> implements IMesOrganizeWarehouseService {

}
