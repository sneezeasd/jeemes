package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataLogisticalcompany;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataLogisticalcompanyMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataLogisticalcompanyService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—物流公司
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataLogisticalcompanyServiceImpl extends ServiceImpl<MesChiefdataLogisticalcompanyMapper, MesChiefdataLogisticalcompany> implements IMesChiefdataLogisticalcompanyService {

}
