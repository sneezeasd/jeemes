package org.jeecg.modules.mes.filecollection.controller;

import org.jeecg.common.api.vo.Result;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @Package org.jeecg.modules.mes.filecollection
 * @date 2021/4/16 14:05
 * @description
 */
@RestController
@RequestMapping("/fileCollection")
public class FileCollectionController {


    @RequestMapping("/receiveSpiFile")
    public Result<?> receiveSpiFile(HttpServletRequest request) {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
        for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
            MultipartFile file = entity.getValue();
            System.out.println(file.getName());
        }

        return Result.ok("上传成功");
    }
}
