package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataSteelmesh;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataSteelmeshMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataSteelmeshService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 钢网建档
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
@Service
public class MesChiefdataSteelmeshServiceImpl extends ServiceImpl<MesChiefdataSteelmeshMapper, MesChiefdataSteelmesh> implements IMesChiefdataSteelmeshService {

    @Autowired
    private MesChiefdataSteelmeshMapper steelmeshMapper;

    @Override
    public boolean increasePointUsageNum(String steelmeshSn) {
        return steelmeshMapper.increasePointUsageNum(steelmeshSn);
    }
}
