package org.jeecg.modules.mes.organize.service;

import org.jeecg.modules.mes.organize.entity.MesOrganizeClasstype;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 组织—日历（班别）
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
public interface IMesOrganizeClasstypeService extends IService<MesOrganizeClasstype> {

	public List<MesOrganizeClasstype> selectByMainId(String mainId);
}
