package org.jeecg.modules.biconf.biapi.dto;

import lombok.Data;
import org.jeecg.modules.biconf.entity.BiVisual;
import org.jeecg.modules.biconf.entity.BiVisualConfig;

import java.io.Serializable;

/**
 * 大屏展示DTO
 *
 * @author
 */
@Data
public class VisualDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 可视化主信息
	 */
	private BiVisual visual;

	/**
	 * 可视化配置信息
	 */
	private BiVisualConfig config;

}
