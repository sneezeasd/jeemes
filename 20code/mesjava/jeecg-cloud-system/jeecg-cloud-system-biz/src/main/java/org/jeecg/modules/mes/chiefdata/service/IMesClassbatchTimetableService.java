package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesClassbatchTimetable;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 班组—时间
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface IMesClassbatchTimetableService extends IService<MesClassbatchTimetable> {

	public List<MesClassbatchTimetable> selectByMainId(String mainId);
}
