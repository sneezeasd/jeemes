package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesSortformulaStrategyinfo;
import org.jeecg.modules.mes.chiefdata.mapper.MesSortformulaStrategyinfoMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesSortformulaStrategyinfoService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 主数据—拣货方案策略
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesSortformulaStrategyinfoServiceImpl extends ServiceImpl<MesSortformulaStrategyinfoMapper, MesSortformulaStrategyinfo> implements IMesSortformulaStrategyinfoService {
	
	@Autowired
	private MesSortformulaStrategyinfoMapper mesSortformulaStrategyinfoMapper;
	
	@Override
	public List<MesSortformulaStrategyinfo> selectByMainId(String mainId) {
		return mesSortformulaStrategyinfoMapper.selectByMainId(mainId);
	}
}
