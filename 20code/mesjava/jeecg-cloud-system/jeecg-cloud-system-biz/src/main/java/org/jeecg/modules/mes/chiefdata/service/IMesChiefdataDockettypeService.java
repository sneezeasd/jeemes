package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataDockettype;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 主数据—单据类型配置
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface IMesChiefdataDockettypeService extends IService<MesChiefdataDockettype> {

}
