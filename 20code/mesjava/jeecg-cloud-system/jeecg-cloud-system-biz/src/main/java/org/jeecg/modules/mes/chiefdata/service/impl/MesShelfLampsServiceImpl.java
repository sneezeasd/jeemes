package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesShelfLamps;
import org.jeecg.modules.mes.chiefdata.mapper.MesShelfLampsMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesShelfLampsService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 货架多色灯
 * @Author: jeecg-boot
 * @Date:   2020-10-12
 * @Version: V1.0
 */
@Service
public class MesShelfLampsServiceImpl extends ServiceImpl<MesShelfLampsMapper, MesShelfLamps> implements IMesShelfLampsService {

}
