package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesMsdcontrolruleItem;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 主数据—MSD管控规则明细
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface IMesMsdcontrolruleItemService extends IService<MesMsdcontrolruleItem> {

	public List<MesMsdcontrolruleItem> selectByMainId(String mainId);
}
