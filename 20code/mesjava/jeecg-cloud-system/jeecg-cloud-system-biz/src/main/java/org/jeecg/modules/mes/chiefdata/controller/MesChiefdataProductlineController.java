package org.jeecg.modules.mes.chiefdata.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataProductline;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataProductlineService;
import org.jeecg.modules.mes.print.conHead;
import org.jeecg.modules.mes.print.mesUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

/**
 * @Description: 主数据—产线
 * @Author: jeecg-boot
 * @Date: 2020-09-14
 * @Version: V1.0
 */
@Api(tags = "主数据—产线")
@RestController
@RequestMapping("/chiefdata/mesChiefdataProductline")
@Slf4j
public class MesChiefdataProductlineController extends JeecgController<MesChiefdataProductline, IMesChiefdataProductlineService> {
    @Autowired
    private IMesChiefdataProductlineService mesChiefdataProductlineService;

    /**
     * 分页列表查询
     *
     * @param mesChiefdataProductline
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "主数据—产线-分页列表查询")
    @ApiOperation(value = "主数据—产线-分页列表查询", notes = "主数据—产线-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(MesChiefdataProductline mesChiefdataProductline,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<MesChiefdataProductline> queryWrapper = QueryGenerator.initQueryWrapper(mesChiefdataProductline, req.getParameterMap());
        queryWrapper.orderByAsc("productline_code");
        Page<MesChiefdataProductline> page = new Page<MesChiefdataProductline>(pageNo, pageSize);
        IPage<MesChiefdataProductline> pageList = mesChiefdataProductlineService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

    @AutoLog(value = "主数据—产线-获取我的产线列表")
    @ApiOperation(value = "主数据—产线-获取我的产线列表", notes = "主数据—产线-获取我的产线列表")
    @GetMapping(value = "/getMylist")
    public Result<?> getMylist(MesChiefdataProductline mesChiefdataProductline,
                               @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                               @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                               HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<MesChiefdataProductline> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("productline_officer", sysUser.getUsername());
        Page<MesChiefdataProductline> page = new Page<MesChiefdataProductline>(pageNo, pageSize);
        IPage<MesChiefdataProductline> pageList = mesChiefdataProductlineService.page(page, queryWrapper);
        return Result.ok(pageList);
    }


    @AutoLog(value = "主数据—产线-根据类型获取产线列表")
    @ApiOperation(value = "主数据—产线-根据类型获取产线列表", notes = "主数据—产线-根据类型获取产线列表")
    @GetMapping(value = "/getListByType")
    public Result<?> getListByType(@RequestParam(name = "type", required = true) String type) {
        QueryWrapper<MesChiefdataProductline> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("productline_type", type);
        queryWrapper.orderByAsc("productline_code");
        List<MesChiefdataProductline> ret = mesChiefdataProductlineService.list(queryWrapper);
        return Result.ok(ret);
    }

    /**
     * 添加
     *
     * @param mesChiefdataProductline
     * @return
     */
    @AutoLog(value = "主数据—产线-添加")
    @ApiOperation(value = "主数据—产线-添加", notes = "主数据—产线-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody MesChiefdataProductline mesChiefdataProductline) {
        QueryWrapper<MesChiefdataProductline> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("productline_code", mesChiefdataProductline.getProductlineCode());
        List<MesChiefdataProductline> list = mesChiefdataProductlineService.list(queryWrapper);
        if (list.size() > 0) {
            return Result.error("该产线编号已存在！");
        }
        mesChiefdataProductline.setQuery2("正常");
        mesChiefdataProductlineService.save(mesChiefdataProductline);
        return Result.ok("添加成功！");
    }

    /**
     * 编辑
     *
     * @param mesChiefdataProductline
     * @return
     */
    @AutoLog(value = "主数据—产线-编辑")
    @ApiOperation(value = "主数据—产线-编辑", notes = "主数据—产线-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody MesChiefdataProductline mesChiefdataProductline) {
        mesChiefdataProductlineService.updateById(mesChiefdataProductline);
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "主数据—产线-通过id删除")
    @ApiOperation(value = "主数据—产线-通过id删除", notes = "主数据—产线-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        mesChiefdataProductlineService.removeById(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "主数据—产线-批量删除")
    @ApiOperation(value = "主数据—产线-批量删除", notes = "主数据—产线-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.mesChiefdataProductlineService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "主数据—产线-通过id查询")
    @ApiOperation(value = "主数据—产线-通过id查询", notes = "主数据—产线-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        MesChiefdataProductline mesChiefdataProductline = mesChiefdataProductlineService.getById(id);
        if (mesChiefdataProductline == null) {
            return Result.error("未找到对应数据");
        }
        return Result.ok(mesChiefdataProductline);
    }

    /**
     * 远程调用，根据产线编号去查询该产线信息
     *
     * @param productlineCode
     * @return
     */
    @GetMapping("/getproductlineCode")
    public MesChiefdataProductline queryByLineCode(@RequestParam(name = "productlineCode", required = true) String productlineCode) {
        QueryWrapper<MesChiefdataProductline> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("productline_code", productlineCode);
        return mesChiefdataProductlineService.getOne(queryWrapper);
    }


    /**
     * 异常停机/恢复
     *
     * @param id
     * @return
     */
    @AutoLog(value = "主数据—产线-异常停机/恢复")
    @ApiOperation(value = "主数据—产线-异常停机/恢复", notes = "主数据—产线-异常停机/恢复")
    @GetMapping(value = "/stopById")
    public Result<?> stopById(@RequestParam(name = "id", required = true) String id,
                              @RequestParam(name = "flag", required = true) String flag) {
        MesChiefdataProductline mesChiefdataProductline = mesChiefdataProductlineService.getById(id);
        if (mesChiefdataProductline == null) {
            return Result.error("未找到对应数据");
        }
        conHead conHead = new conHead();
        try {
            conHead.setConBy1(flag);  //信号值 30-中断  31-恢复
            conHead.setConBy2("COM1");   //串口  固定COM1
            //调用打印功能，printModel.getPrintServeaddress()为调用远程打印的服务器地址
            mesUtil.gencontract("http://" + mesChiefdataProductline.getQuery1() + "/util/sendSerialData", conHead);
        } catch (Exception e) {
            return Result.error("操作失败" + e.getMessage());
        }
        if ("30".equals(flag)) {
            mesChiefdataProductline.setQuery2("停机");
        } else if ("31".equals(flag)) {
            mesChiefdataProductline.setQuery2("正常");
        }
        mesChiefdataProductlineService.updateById(mesChiefdataProductline);
        return Result.ok("操作成功");
    }

    /**
     * 根据产线类型获取产线列表
     *
     * @param type
     */
    @AutoLog(value = "主数据—产线-根据产线类型获取产线列表")
    @ApiOperation(value = "主数据—产线-根据产线类型获取产线列表", notes = "主数据—产线-根据产线类型获取产线列表")
    @GetMapping(value = "/queryChiefDataProductLinesByType")
    public Result<?> queryChiefDataProductLinesByType(@RequestParam(name = "type") String type) {
        QueryWrapper<MesChiefdataProductline> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("productline_type", type).orderByAsc("productline_code");
        List<MesChiefdataProductline> pageList = mesChiefdataProductlineService.list(queryWrapper);
        return Result.ok(pageList);
    }


    /**
     * 导出excel
     *
     * @param request
     * @param mesChiefdataProductline
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesChiefdataProductline mesChiefdataProductline) {
        return super.exportXls(request, mesChiefdataProductline, MesChiefdataProductline.class, "主数据—产线");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesChiefdataProductline.class);
    }

}
