package org.jeecg.modules.bi.service.impl;

import org.jeecg.modules.bi.entity.zdOnlCgreportHead;
import org.jeecg.modules.bi.entity.zdOnlCgreportParam;
import org.jeecg.modules.bi.entity.zdOnlCgreportItem;
import org.jeecg.modules.bi.mapper.zdOnlCgreportParamMapper;
import org.jeecg.modules.bi.mapper.zdOnlCgreportItemMapper;
import org.jeecg.modules.bi.mapper.zdOnlCgreportHeadMapper;
import org.jeecg.modules.bi.service.IzdOnlCgreportHeadService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: onl_cgreport_head
 * @Author: wms-cloud
 * @Date:   2020-12-09
 * @Version: V1.0
 */
@Service
public class ZdOnlCgreportHeadServiceImpl extends ServiceImpl<zdOnlCgreportHeadMapper, zdOnlCgreportHead> implements IzdOnlCgreportHeadService {

	@Autowired
	private zdOnlCgreportHeadMapper zdOnlCgreportHeadMapper;
	@Autowired
	private zdOnlCgreportParamMapper zdOnlCgreportParamMapper;
	@Autowired
	private zdOnlCgreportItemMapper zdOnlCgreportItemMapper;
	
	@Override
	@Transactional
	public void saveMain(zdOnlCgreportHead zdOnlCgreportHead, List<zdOnlCgreportParam> zdOnlCgreportParamList, List<zdOnlCgreportItem> zdOnlCgreportItemList) {
		zdOnlCgreportHeadMapper.insert(zdOnlCgreportHead);
		if(zdOnlCgreportParamList !=null && zdOnlCgreportParamList.size()>0) {
			for(zdOnlCgreportParam entity: zdOnlCgreportParamList) {
				//外键设置
				entity.setCgrheadId(zdOnlCgreportHead.getId());
				zdOnlCgreportParamMapper.insert(entity);
			}
		}
		if(zdOnlCgreportItemList !=null && zdOnlCgreportItemList.size()>0) {
			for(zdOnlCgreportItem entity: zdOnlCgreportItemList) {
				//外键设置
				entity.setCgrheadId(zdOnlCgreportHead.getId());
				zdOnlCgreportItemMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(zdOnlCgreportHead zdOnlCgreportHead, List<zdOnlCgreportParam> zdOnlCgreportParamList, List<zdOnlCgreportItem> zdOnlCgreportItemList) {
		zdOnlCgreportHeadMapper.updateById(zdOnlCgreportHead);
		
		//1.先删除子表数据
		zdOnlCgreportParamMapper.deleteByMainId(zdOnlCgreportHead.getId());
		zdOnlCgreportItemMapper.deleteByMainId(zdOnlCgreportHead.getId());
		
		//2.子表数据重新插入
		if(zdOnlCgreportParamList !=null && zdOnlCgreportParamList.size()>0) {
			for(zdOnlCgreportParam entity: zdOnlCgreportParamList) {
				//外键设置
				entity.setCgrheadId(zdOnlCgreportHead.getId());
				zdOnlCgreportParamMapper.insert(entity);
			}
		}
		if(zdOnlCgreportItemList !=null && zdOnlCgreportItemList.size()>0) {
			for(zdOnlCgreportItem entity: zdOnlCgreportItemList) {
				//外键设置
				entity.setCgrheadId(zdOnlCgreportHead.getId());
				zdOnlCgreportItemMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		zdOnlCgreportParamMapper.deleteByMainId(id);
		zdOnlCgreportItemMapper.deleteByMainId(id);
		zdOnlCgreportHeadMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			zdOnlCgreportParamMapper.deleteByMainId(id.toString());
			zdOnlCgreportItemMapper.deleteByMainId(id.toString());
			zdOnlCgreportHeadMapper.deleteById(id);
		}
	}
	
}
