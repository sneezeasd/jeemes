package org.jeecg.modules.bi.mapper;

import java.util.List;
import org.jeecg.modules.bi.entity.zdOnlCgreportItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: onl_cgreport_item
 * @Author: wms-cloud
 * @Date:   2020-12-09
 * @Version: V1.0
 */
public interface zdOnlCgreportItemMapper extends BaseMapper<zdOnlCgreportItem> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<zdOnlCgreportItem> selectByMainId(@Param("mainId") String mainId);
}
