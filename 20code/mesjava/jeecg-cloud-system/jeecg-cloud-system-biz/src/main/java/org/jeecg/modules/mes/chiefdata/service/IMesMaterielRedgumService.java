package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesMaterielRedgum;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 主数据—辅料-红胶/锡膏
 * @Author: jeecg-boot
 * @Date: 2020-10-16
 * @Version: V1.0
 */
public interface IMesMaterielRedgumService extends IService<MesMaterielRedgum> {

    public List<MesMaterielRedgum> selectByMainId(String mainId);

    /**
     *  根据料号查询
     * @param pCode
     * @return
     */
    public MesMaterielRedgum findBypCode(String pCode);
}
