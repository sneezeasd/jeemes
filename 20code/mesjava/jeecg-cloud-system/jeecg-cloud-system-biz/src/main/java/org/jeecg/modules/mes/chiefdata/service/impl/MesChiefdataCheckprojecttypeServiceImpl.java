package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataCheckprojecttype;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataCheckprojecttypeMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataCheckprojecttypeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—检测项目类型
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataCheckprojecttypeServiceImpl extends ServiceImpl<MesChiefdataCheckprojecttypeMapper, MesChiefdataCheckprojecttype> implements IMesChiefdataCheckprojecttypeService {

}
