package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesMaterielStorage;
import org.jeecg.modules.mes.chiefdata.mapper.MesMaterielStorageMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesMaterielStorageService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 物料—仓储
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
@Service
public class MesMaterielStorageServiceImpl extends ServiceImpl<MesMaterielStorageMapper, MesMaterielStorage> implements IMesMaterielStorageService {
	
	@Autowired
	private MesMaterielStorageMapper mesMaterielStorageMapper;
	
	@Override
	public List<MesMaterielStorage> selectByMainId(String mainId) {
		return mesMaterielStorageMapper.selectByMainId(mainId);
	}
}
