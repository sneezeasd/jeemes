package org.jeecg.modules.mes.chiefdata.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.chiefdata.entity.MesAncillarytoolStir;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 制造中心-辅料搅拌
 * @Author: jeecg-boot
 * @Date:   2020-11-04
 * @Version: V1.0
 */
public interface MesAncillarytoolStirMapper extends BaseMapper<MesAncillarytoolStir> {

}
