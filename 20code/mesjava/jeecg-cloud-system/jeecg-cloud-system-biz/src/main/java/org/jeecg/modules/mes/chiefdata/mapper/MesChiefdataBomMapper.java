package org.jeecg.modules.mes.chiefdata.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataBom;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 主数据—BOM
 * @Author: jeecg-boot
 * @Date:   2020-10-20
 * @Version: V1.0
 */
public interface MesChiefdataBomMapper extends BaseMapper<MesChiefdataBom> {

}
