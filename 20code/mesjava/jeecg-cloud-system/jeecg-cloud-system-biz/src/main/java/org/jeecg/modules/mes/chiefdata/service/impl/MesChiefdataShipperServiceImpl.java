package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataShipper;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataShipperMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataShipperService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—货主
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesChiefdataShipperServiceImpl extends ServiceImpl<MesChiefdataShipperMapper, MesChiefdataShipper> implements IMesChiefdataShipperService {

}
