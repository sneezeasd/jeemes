package org.jeecg.modules.bidata.bientity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

//饼图
public class btEntity {
    @SerializedName("data")
    private List<btEntity.DataBean> data;

    public List<btEntity.DataBean> getData() {
        return data;
    }

    public void setData(List<btEntity.DataBean> data) {
        this.data = data;
    }
    public static class DataBean {
        @SerializedName("name")
        private String name;
        @SerializedName("value")
        private String value;

        @SerializedName("url")
        private String url;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

}
