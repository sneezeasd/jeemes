package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataSparematerial;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataSparematerialMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataSparematerialService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—时序备料
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataSparematerialServiceImpl extends ServiceImpl<MesChiefdataSparematerialMapper, MesChiefdataSparematerial> implements IMesChiefdataSparematerialService {

}
