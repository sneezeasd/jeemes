package org.jeecg.modules.mes.organize.service.impl;

import org.jeecg.modules.mes.organize.entity.MesOrganizeCompany;
import org.jeecg.modules.mes.organize.mapper.MesOrganizeCompanyMapper;
import org.jeecg.modules.mes.organize.service.IMesOrganizeCompanyService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 组织—公司
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Service
public class MesOrganizeCompanyServiceImpl extends ServiceImpl<MesOrganizeCompanyMapper, MesOrganizeCompany> implements IMesOrganizeCompanyService {

}
