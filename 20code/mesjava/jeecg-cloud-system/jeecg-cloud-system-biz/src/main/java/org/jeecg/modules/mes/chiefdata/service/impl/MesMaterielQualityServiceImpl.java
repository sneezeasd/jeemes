package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesMaterielQuality;
import org.jeecg.modules.mes.chiefdata.mapper.MesMaterielQualityMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesMaterielQualityService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 物料—品质
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
@Service
public class MesMaterielQualityServiceImpl extends ServiceImpl<MesMaterielQualityMapper, MesMaterielQuality> implements IMesMaterielQualityService {
	
	@Autowired
	private MesMaterielQualityMapper mesMaterielQualityMapper;
	
	@Override
	public List<MesMaterielQuality> selectByMainId(String mainId) {
		return mesMaterielQualityMapper.selectByMainId(mainId);
	}
}
