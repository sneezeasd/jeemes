package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataMaketool;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 主数据—制具信息
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface IMesChiefdataMaketoolService extends IService<MesChiefdataMaketool> {

}
