package org.jeecg.modules.mes.organize.service.impl;

import org.jeecg.modules.mes.organize.entity.MesOrganizeHoliday;
import org.jeecg.modules.mes.organize.mapper.MesOrganizeHolidayMapper;
import org.jeecg.modules.mes.organize.service.IMesOrganizeHolidayService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 组织—日历（假期）
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
@Service
public class MesOrganizeHolidayServiceImpl extends ServiceImpl<MesOrganizeHolidayMapper, MesOrganizeHoliday> implements IMesOrganizeHolidayService {
	
	@Autowired
	private MesOrganizeHolidayMapper mesOrganizeHolidayMapper;
	
	@Override
	public List<MesOrganizeHoliday> selectByMainId(String mainId) {
		return mesOrganizeHolidayMapper.selectByMainId(mainId);
	}
}
