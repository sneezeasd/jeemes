package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesAncillarytoolStir;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 制造中心-辅料搅拌
 * @Author: jeecg-boot
 * @Date:   2020-11-04
 * @Version: V1.0
 */
public interface IMesAncillarytoolStirService extends IService<MesAncillarytoolStir> {

}
