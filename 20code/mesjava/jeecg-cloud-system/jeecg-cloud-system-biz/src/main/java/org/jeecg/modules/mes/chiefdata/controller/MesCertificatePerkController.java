package org.jeecg.modules.mes.chiefdata.controller;

import org.jeecg.common.system.query.QueryGenerator;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.client.ProduceClient;
import org.jeecg.modules.mes.produce.entity.MesAncillarytoolHeat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import java.util.Arrays;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.chiefdata.entity.MesCertificateItem;
import org.jeecg.modules.mes.chiefdata.entity.MesCertificatePerk;
import org.jeecg.modules.mes.chiefdata.service.IMesCertificatePerkService;
import org.jeecg.modules.mes.chiefdata.service.IMesCertificateItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

 /**
 * @Description: 物料凭证抬头
 * @Author: jeecg-boot
 * @Date:   2020-10-12
 * @Version: V1.0
 */
@Api(tags="物料凭证抬头")
@RestController
@RequestMapping("/chiefdata/mesCertificatePerk")
@Slf4j
public class MesCertificatePerkController extends JeecgController<MesCertificatePerk, IMesCertificatePerkService> {

	@Autowired
	private IMesCertificatePerkService mesCertificatePerkService;

	@Autowired
	private IMesCertificateItemService mesCertificateItemService;




	/*---------------------------------主表处理-begin-------------------------------------*/

	/**
	 * 分页列表查询
	 * @param mesCertificatePerk
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "物料凭证抬头-分页列表查询")
	@ApiOperation(value="物料凭证抬头-分页列表查询", notes="物料凭证抬头-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesCertificatePerk mesCertificatePerk,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesCertificatePerk> queryWrapper = QueryGenerator.initQueryWrapper(mesCertificatePerk, req.getParameterMap());
		Page<MesCertificatePerk> page = new Page<MesCertificatePerk>(pageNo, pageSize);
		IPage<MesCertificatePerk> pageList = mesCertificatePerkService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
     *   添加
     * @param mesCertificatePerk
     * @return
     */
    @AutoLog(value = "物料凭证抬头-添加")
    @ApiOperation(value="物料凭证抬头-添加", notes="物料凭证抬头-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody MesCertificatePerk mesCertificatePerk) {
        mesCertificatePerkService.save(mesCertificatePerk);
        return Result.ok("添加成功！");
    }

    /**
     *  编辑
     * @param mesCertificatePerk
     * @return
     */
    @AutoLog(value = "物料凭证抬头-编辑")
    @ApiOperation(value="物料凭证抬头-编辑", notes="物料凭证抬头-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody MesCertificatePerk mesCertificatePerk) {
        mesCertificatePerkService.updateById(mesCertificatePerk);
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     * @param id
     * @return
     */
    @AutoLog(value = "物料凭证抬头-通过id删除")
    @ApiOperation(value="物料凭证抬头-通过id删除", notes="物料凭证抬头-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name="id",required=true) String id) {
        mesCertificatePerkService.delMain(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @AutoLog(value = "物料凭证抬头-批量删除")
    @ApiOperation(value="物料凭证抬头-批量删除", notes="物料凭证抬头-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
        this.mesCertificatePerkService.delBatchMain(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesCertificatePerk mesCertificatePerk) {
        return super.exportXls(request, mesCertificatePerk, MesCertificatePerk.class, "物料凭证抬头");
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesCertificatePerk.class);
    }
	/*---------------------------------主表处理-end-------------------------------------*/


    /*--------------------------------子表处理-物料凭证项目-begin----------------------------------------------*/
	/**
	 * 通过主表ID查询
	 * @return
	 */
	@AutoLog(value = "物料凭证项目-通过主表ID查询")
	@ApiOperation(value="物料凭证项目-通过主表ID查询", notes="物料凭证项目-通过主表ID查询")
	@GetMapping(value = "/listMesCertificateItemByMainId")
    public Result<?> listMesCertificateItemByMainId(MesCertificateItem mesCertificateItem,
                                                    @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                    @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                                    HttpServletRequest req) {
        QueryWrapper<MesCertificateItem> queryWrapper = QueryGenerator.initQueryWrapper(mesCertificateItem, req.getParameterMap());
        Page<MesCertificateItem> page = new Page<MesCertificateItem>(pageNo, pageSize);
        IPage<MesCertificateItem> pageList = mesCertificateItemService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

	/**
	 * 添加
	 * @param mesCertificateItem
	 * @return
	 */
	@AutoLog(value = "物料凭证项目-添加")
	@ApiOperation(value="物料凭证项目-添加", notes="物料凭证项目-添加")
	@PostMapping(value = "/addMesCertificateItem")
	public Result<?> addMesCertificateItem(@RequestBody MesCertificateItem mesCertificateItem) {
		mesCertificateItemService.save(mesCertificateItem);
		return Result.ok("添加成功！");
	}

    /**
	 * 编辑
	 * @param mesCertificateItem
	 * @return
	 */
	@AutoLog(value = "物料凭证项目-编辑")
	@ApiOperation(value="物料凭证项目-编辑", notes="物料凭证项目-编辑")
	@PutMapping(value = "/editMesCertificateItem")
	public Result<?> editMesCertificateItem(@RequestBody MesCertificateItem mesCertificateItem) {
		mesCertificateItemService.updateById(mesCertificateItem);
		return Result.ok("编辑成功!");
	}

	/**
	 * 通过id删除
	 * @param id
	 * @return
	 */
	@AutoLog(value = "物料凭证项目-通过id删除")
	@ApiOperation(value="物料凭证项目-通过id删除", notes="物料凭证项目-通过id删除")
	@DeleteMapping(value = "/deleteMesCertificateItem")
	public Result<?> deleteMesCertificateItem(@RequestParam(name="id",required=true) String id) {
		mesCertificateItemService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "物料凭证项目-批量删除")
	@ApiOperation(value="物料凭证项目-批量删除", notes="物料凭证项目-批量删除")
	@DeleteMapping(value = "/deleteBatchMesCertificateItem")
	public Result<?> deleteBatchMesCertificateItem(@RequestParam(name="ids",required=true) String ids) {
	    this.mesCertificateItemService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportMesCertificateItem")
    public ModelAndView exportMesCertificateItem(HttpServletRequest request, MesCertificateItem mesCertificateItem) {
		 // Step.1 组装查询条件
		 QueryWrapper<MesCertificateItem> queryWrapper = QueryGenerator.initQueryWrapper(mesCertificateItem, request.getParameterMap());
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

		 // Step.2 获取导出数据
		 List<MesCertificateItem> pageList = mesCertificateItemService.list(queryWrapper);
		 List<MesCertificateItem> exportList = null;

		 // 过滤选中数据
		 String selections = request.getParameter("selections");
		 if (oConvertUtils.isNotEmpty(selections)) {
			 List<String> selectionList = Arrays.asList(selections.split(","));
			 exportList = pageList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
		 } else {
			 exportList = pageList;
		 }

		 // Step.3 AutoPoi 导出Excel
		 ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
		 mv.addObject(NormalExcelConstants.FILE_NAME, "物料凭证项目"); //此处设置的filename无效 ,前端会重更新设置一下
		 mv.addObject(NormalExcelConstants.CLASS, MesCertificateItem.class);
		 mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("物料凭证项目报表", "导出人:" + sysUser.getRealname(), "物料凭证项目"));
		 mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
		 return mv;
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importMesCertificateItem/{mainId}")
    public Result<?> importMesCertificateItem(HttpServletRequest request, HttpServletResponse response, @PathVariable("mainId") String mainId) {
		 MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		 Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		 for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			 MultipartFile file = entity.getValue();// 获取上传文件对象
			 ImportParams params = new ImportParams();
			 params.setTitleRows(2);
			 params.setHeadRows(1);
			 params.setNeedSave(true);
			 try {
				 List<MesCertificateItem> list = ExcelImportUtil.importExcel(file.getInputStream(), MesCertificateItem.class, params);
				 for (MesCertificateItem temp : list) {
                    temp.setPerkId(mainId);
				 }
				 long start = System.currentTimeMillis();
				 mesCertificateItemService.saveBatch(list);
				 log.info("消耗时间" + (System.currentTimeMillis() - start) + "毫秒");
				 return Result.ok("文件导入成功！数据行数：" + list.size());
			 } catch (Exception e) {
				 log.error(e.getMessage(), e);
				 return Result.error("文件导入失败:" + e.getMessage());
			 } finally {
				 try {
					 file.getInputStream().close();
				 } catch (IOException e) {
					 e.printStackTrace();
				 }
			 }
		 }
		 return Result.error("文件导入失败！");
    }

    /*--------------------------------子表处理-物料凭证项目-end----------------------------------------------*/




}
