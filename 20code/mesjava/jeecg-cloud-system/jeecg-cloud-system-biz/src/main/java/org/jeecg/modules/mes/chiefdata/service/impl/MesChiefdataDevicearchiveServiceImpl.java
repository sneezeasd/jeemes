package org.jeecg.modules.mes.chiefdata.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataDevicearchive;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataDevicearchiveMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataDevicearchiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.epms.util.ObjectHelper;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

/**
 * @Description: 主数据—设备建档
 * @Author: jeecg-boot
 * @Date: 2020-10-16
 * @Version: V1.0
 */
@Service
public class MesChiefdataDevicearchiveServiceImpl extends ServiceImpl<MesChiefdataDevicearchiveMapper, MesChiefdataDevicearchive> implements IMesChiefdataDevicearchiveService {

    @Autowired
    private MesChiefdataDevicearchiveMapper mesChiefdataDevicearchiveMapper;

    @Override
    public MesChiefdataDevicearchive queryByDeviceSn(String deviceSn) {
        QueryWrapper<MesChiefdataDevicearchive> queryWrapper = new QueryWrapper();
        queryWrapper.eq("device_sn", deviceSn);
        List<MesChiefdataDevicearchive> mesChiefdataDevicearchives = mesChiefdataDevicearchiveMapper.selectList(queryWrapper);
        if (ObjectHelper.isEmpty(mesChiefdataDevicearchives)) {
            return null;
        }
        return mesChiefdataDevicearchives.get(0);
    }
}
