package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataClientgroup;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataClientgroupMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataClientgroupService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—客户组
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesChiefdataClientgroupServiceImpl extends ServiceImpl<MesChiefdataClientgroupMapper, MesChiefdataClientgroup> implements IMesChiefdataClientgroupService {

}
