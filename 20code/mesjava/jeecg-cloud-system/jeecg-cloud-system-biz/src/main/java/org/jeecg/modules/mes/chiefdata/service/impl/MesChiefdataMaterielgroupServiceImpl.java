package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataMaterielgroup;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataMaterielgroupMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataMaterielgroupService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—物料组
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataMaterielgroupServiceImpl extends ServiceImpl<MesChiefdataMaterielgroupMapper, MesChiefdataMaterielgroup> implements IMesChiefdataMaterielgroupService {

}
