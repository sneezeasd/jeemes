package org.jeecg.modules.mes.chiefdata.controller;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataFeeder;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataFeederService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.mes.produce.entity.MesCommandbillPitem;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 主数据—FEEDER建档
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Api(tags="主数据—FEEDER建档")
@RestController
@RequestMapping("/chiefdata/mesChiefdataFeeder")
@Slf4j
public class MesChiefdataFeederController extends JeecgController<MesChiefdataFeeder, IMesChiefdataFeederService> {
	@Autowired
	private IMesChiefdataFeederService mesChiefdataFeederService;

	/**
	 * 分页列表查询
	 *
	 * @param mesChiefdataFeeder
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "主数据—FEEDER建档-分页列表查询")
	@ApiOperation(value="主数据—FEEDER建档-分页列表查询", notes="主数据—FEEDER建档-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesChiefdataFeeder mesChiefdataFeeder,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesChiefdataFeeder> queryWrapper = QueryGenerator.initQueryWrapper(mesChiefdataFeeder, req.getParameterMap());
		Page<MesChiefdataFeeder> page = new Page<MesChiefdataFeeder>(pageNo, pageSize);
		IPage<MesChiefdataFeeder> pageList = mesChiefdataFeederService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *   添加
	 *
	 * @param mesChiefdataFeeder
	 * @return
	 */
	@AutoLog(value = "主数据—FEEDER建档-添加")
	@ApiOperation(value="主数据—FEEDER建档-添加", notes="主数据—FEEDER建档-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesChiefdataFeeder mesChiefdataFeeder) {
		mesChiefdataFeederService.save(mesChiefdataFeeder);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param mesChiefdataFeeder
	 * @return
	 */
	@AutoLog(value = "主数据—FEEDER建档-编辑")
	@ApiOperation(value="主数据—FEEDER建档-编辑", notes="主数据—FEEDER建档-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesChiefdataFeeder mesChiefdataFeeder) {
		mesChiefdataFeederService.updateById(mesChiefdataFeeder);
		return Result.ok("编辑成功!");
	}

	/**
	 * 远程调用，修改feeder使用次数
	 * @param sn 菲达sn
	 * @param sjnum 使用次数
	 * @return
	 */
	@GetMapping(value = "/editComitem")
	public boolean editfeederComitem(@RequestParam(name="sn",required=true) String sn,
									 @RequestParam(name="sjnum",required=true) String sjnum) {
		System.out.println("feeder:"+sn+" sjnum:"+sjnum);
		QueryWrapper<MesChiefdataFeeder> mqueryWrapper = new QueryWrapper<>();
		mqueryWrapper.eq("feeder_sn",sn);
		List<MesChiefdataFeeder> list = mesChiefdataFeederService.list(mqueryWrapper);
		if(list.size()==0){
			MesChiefdataFeeder mesChiefdataFeeder = new MesChiefdataFeeder();
			mesChiefdataFeeder.setFeederSn(sn);
			mesChiefdataFeeder.setQuery1(sjnum);
			mesChiefdataFeederService.save(mesChiefdataFeeder);
			return false;
		}
		MesChiefdataFeeder mesChiefdataFeeder = list.get(0);//根据sn获取feeder信息
		if (mesChiefdataFeeder.getQuery1()==null) {
			mesChiefdataFeeder.setQuery1("0");
		}
		BigDecimal query1 = new BigDecimal(mesChiefdataFeeder.getQuery1());//原已使用点数
		BigDecimal withdrawNum = query1.add(new BigDecimal(sjnum));//新已使用点数=原已使用点数 + 使用次数
		mesChiefdataFeeder.setQuery1(withdrawNum.toString());
		mesChiefdataFeederService.updateById(mesChiefdataFeeder);
		return true;
	}

	/**
	 * 远程调用，get feeder 根据feeder sn获取
	 *
	 * @param sn
	 * @return
	 */
	@GetMapping(value = "/getfeeder")
	public MesChiefdataFeeder getfeederComitem(@RequestParam(name="sn",required=true) String sn) {
		QueryWrapper<MesChiefdataFeeder> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("feeder_sn",sn);
		List<MesChiefdataFeeder> list = mesChiefdataFeederService.list(queryWrapper);
		if(list.size()>0){
			return list.get(0);
		}
		return null;
	}

	/**
	 *  保养
	 *
	 * @param mesChiefdataFeeder
	 * @return
	 */
	@AutoLog(value = "主数据—FEEDER建档-保养")
	@ApiOperation(value="主数据—FEEDER建档-保养", notes="主数据—FEEDER建档-保养")
	@PutMapping(value = "/feederFix")
	public Result<?> feederFix(@RequestBody MesChiefdataFeeder mesChiefdataFeeder) {
		mesChiefdataFeeder = mesChiefdataFeederService.getById(mesChiefdataFeeder.getId());
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		mesChiefdataFeeder.setQuery2(DateUtils.getDate("yyyy-MM-dd"));
		mesChiefdataFeeder.setQuery1("0");
		mesChiefdataFeeder.setQuery3(sysUser.getRealname());
		mesChiefdataFeederService.updateById(mesChiefdataFeeder);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—FEEDER建档-通过id删除")
	@ApiOperation(value="主数据—FEEDER建档-通过id删除", notes="主数据—FEEDER建档-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesChiefdataFeederService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "主数据—FEEDER建档-批量删除")
	@ApiOperation(value="主数据—FEEDER建档-批量删除", notes="主数据—FEEDER建档-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesChiefdataFeederService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—FEEDER建档-通过id查询")
	@ApiOperation(value="主数据—FEEDER建档-通过id查询", notes="主数据—FEEDER建档-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesChiefdataFeeder mesChiefdataFeeder = mesChiefdataFeederService.getById(id);
		if(mesChiefdataFeeder==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesChiefdataFeeder);
	}


	@AutoLog(value = "主数据—FEEDER建档-通过sn查询")
	@ApiOperation(value="主数据—FEEDER建档-通过sn查询", notes="主数据—FEEDER建档-通过sn查询")
	@GetMapping(value = "/queryBySn")
	public Result<?> queryBySn(@RequestParam(name="sn",required=true) String sn) {
		QueryWrapper<MesChiefdataFeeder> w1 = new QueryWrapper();
		w1.eq("feeder_sn",sn);
		MesChiefdataFeeder mesChiefdataFeeder = mesChiefdataFeederService.getOne(w1);
		if(mesChiefdataFeeder==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesChiefdataFeeder);
	}

	/**
	 * 导出excel
	 *
	 * @param request
	 * @param mesChiefdataFeeder
	 */
	@RequestMapping(value = "/exportXls")
	public ModelAndView exportXls(HttpServletRequest request, MesChiefdataFeeder mesChiefdataFeeder) {
		return super.exportXls(request, mesChiefdataFeeder, MesChiefdataFeeder.class, "主数据—FEEDER建档");
	}

	/**
	 * 通过excel导入数据
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/importExcel", method = RequestMethod.POST)
	public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
		return super.importExcel(request, response, MesChiefdataFeeder.class);
	}

}
