package org.jeecg.modules.mes.mesapp.service;

import org.jeecg.modules.mes.mesapp.entity.MesAppUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 主数据—用户功能
 * @Author: jeecg-boot
 * @Date:   2020-10-14
 * @Version: V1.0
 */
public interface IMesAppUserService extends IService<MesAppUser> {

}
