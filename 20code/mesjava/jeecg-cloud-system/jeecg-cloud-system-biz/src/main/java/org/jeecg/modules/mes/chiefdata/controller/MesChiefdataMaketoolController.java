package org.jeecg.modules.mes.chiefdata.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.chiefdata.entity.*;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataMaketoolService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataMaterielService;
import org.jeecg.modules.mes.chiefdata.service.IMesMaterielNetknifeService;
import org.jeecg.modules.mes.chiefdata.service.IMesMaterielRedgumService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 主数据—制具信息
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Api(tags="主数据—制具信息")
@RestController
@RequestMapping("/chiefdata/mesChiefdataMaketool")
@Slf4j
public class MesChiefdataMaketoolController extends JeecgController<MesChiefdataMaketool, IMesChiefdataMaketoolService> {
	@Autowired
	private IMesChiefdataMaketoolService mesChiefdataMaketoolService;
	 @Autowired
	 private IMesChiefdataMaterielService mesChiefdataMaterielService;
	 @Autowired
	 private IMesMaterielNetknifeService mesMaterielNetknifeService;

	/**
	 * 分页列表查询
	 *
	 * @param mesChiefdataMaketool
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "主数据—制具信息-分页列表查询")
	@ApiOperation(value="主数据—制具信息-分页列表查询", notes="主数据—制具信息-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesChiefdataMaketool mesChiefdataMaketool,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesChiefdataMaketool> queryWrapper = QueryGenerator.initQueryWrapper(mesChiefdataMaketool, req.getParameterMap());
		Page<MesChiefdataMaketool> page = new Page<MesChiefdataMaketool>(pageNo, pageSize);
		IPage<MesChiefdataMaketool> pageList = mesChiefdataMaketoolService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *   添加
	 *
	 * @param mesChiefdataMaketool
	 * @return
	 */
	@AutoLog(value = "主数据—制具信息-添加")
	@ApiOperation(value="主数据—制具信息-添加", notes="主数据—制具信息-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesChiefdataMaketool mesChiefdataMaketool) {
		mesChiefdataMaketoolService.save(mesChiefdataMaketool);
		return Result.ok("添加成功！");
	}

	 /**
	  *   制具信息添加
	  *
	  * @param mCode
	  * @return
	  */
	 @AutoLog(value = "主数据—制具信息-根据code添加")
	 @ApiOperation(value="主数据—制具信息-根据code添加", notes="主数据—制具信息-根据code添加")
	 @GetMapping(value = "/Maketooladd")
	 public String Maketooladd(@RequestParam(name = "mCode",required = true) String mCode) {
		 MesChiefdataMaketool mesChiefdataMaketool = new MesChiefdataMaketool();
		 QueryWrapper<MesChiefdataMateriel> queryWrapper = new QueryWrapper<>();
		 queryWrapper.eq("materiel_code", mCode);
		 MesChiefdataMateriel materiel = mesChiefdataMaterielService.getOne(queryWrapper);

		 QueryWrapper<MesMaterielNetknife> wrapper = new QueryWrapper<>();
		 wrapper.eq("materiel_id", materiel.getId());
		 MesMaterielNetknife netknife = mesMaterielNetknifeService.getOne(wrapper);

		 mesChiefdataMaketool.setMaketoolCode(mCode);
		 mesChiefdataMaketool.setSupplier(materiel.getSupplier());
		 mesChiefdataMaketool.setMaketoolType(materiel.getMaterielType());
		 mesChiefdataMaketool.setMaketoolName(materiel.getProductName());
		 mesChiefdataMaketool.setMaketoolGague(materiel.getGauge());
		 mesChiefdataMaketool.setMaintainNum(netknife.getMaintainNum());
		 mesChiefdataMaketool.setMaintainCycle(netknife.getMaintainCycle());
		 mesChiefdataMaketool.setRemindNum(netknife.getMaintainRemindnum());
		 mesChiefdataMaketool.setRemindDays(netknife.getMaintainReminddays());

		 mesChiefdataMaketoolService.save(mesChiefdataMaketool);
		 return "添加成功！";
	 }

	/**
	 *  编辑
	 *
	 * @param mesChiefdataMaketool
	 * @return
	 */
	@AutoLog(value = "主数据—制具信息-编辑")
	@ApiOperation(value="主数据—制具信息-编辑", notes="主数据—制具信息-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesChiefdataMaketool mesChiefdataMaketool) {
		mesChiefdataMaketoolService.updateById(mesChiefdataMaketool);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—制具信息-通过id删除")
	@ApiOperation(value="主数据—制具信息-通过id删除", notes="主数据—制具信息-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesChiefdataMaketoolService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "主数据—制具信息-批量删除")
	@ApiOperation(value="主数据—制具信息-批量删除", notes="主数据—制具信息-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesChiefdataMaketoolService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—制具信息-通过id查询")
	@ApiOperation(value="主数据—制具信息-通过id查询", notes="主数据—制具信息-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesChiefdataMaketool mesChiefdataMaketool = mesChiefdataMaketoolService.getById(id);
		if(mesChiefdataMaketool==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesChiefdataMaketool);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesChiefdataMaketool
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesChiefdataMaketool mesChiefdataMaketool) {
        return super.exportXls(request, mesChiefdataMaketool, MesChiefdataMaketool.class, "主数据—制具信息");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesChiefdataMaketool.class);
    }

}
