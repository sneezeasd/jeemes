package org.jeecg.modules.biconf.service;

import org.jeecg.modules.biconf.entity.BiVisualCategory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 大屏分类
 * @Author: jeecg-boot
 * @Date:   2020-09-09
 * @Version: V1.0
 */
public interface IBiVisualCategoryService extends IService<BiVisualCategory> {

}
