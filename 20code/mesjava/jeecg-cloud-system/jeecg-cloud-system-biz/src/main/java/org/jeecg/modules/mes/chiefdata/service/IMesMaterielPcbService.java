package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesMaterielPcb;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 物料—PCB
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
public interface IMesMaterielPcbService extends IService<MesMaterielPcb> {

	public List<MesMaterielPcb> selectByMainId(String mainId);
}
