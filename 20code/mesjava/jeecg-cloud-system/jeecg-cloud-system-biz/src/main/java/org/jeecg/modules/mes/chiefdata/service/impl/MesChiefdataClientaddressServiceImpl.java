package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataClientaddress;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataClientaddressMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataClientaddressService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—客户地址
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesChiefdataClientaddressServiceImpl extends ServiceImpl<MesChiefdataClientaddressMapper, MesChiefdataClientaddress> implements IMesChiefdataClientaddressService {

}
