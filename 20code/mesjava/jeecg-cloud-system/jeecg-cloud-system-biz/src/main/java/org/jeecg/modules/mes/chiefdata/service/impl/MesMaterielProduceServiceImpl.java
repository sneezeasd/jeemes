package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesMaterielProduce;
import org.jeecg.modules.mes.chiefdata.mapper.MesMaterielProduceMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesMaterielProduceService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 物料—生产
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
@Service
public class MesMaterielProduceServiceImpl extends ServiceImpl<MesMaterielProduceMapper, MesMaterielProduce> implements IMesMaterielProduceService {
	
	@Autowired
	private MesMaterielProduceMapper mesMaterielProduceMapper;
	
	@Override
	public List<MesMaterielProduce> selectByMainId(String mainId) {
		return mesMaterielProduceMapper.selectByMainId(mainId);
	}
}
