package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataMsdcontrolrule;
import org.jeecg.modules.mes.chiefdata.entity.MesMsdcontrolruleItem;
import org.jeecg.modules.mes.chiefdata.mapper.MesMsdcontrolruleItemMapper;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataMsdcontrolruleMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataMsdcontrolruleService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 主数据—MSD管控规则
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataMsdcontrolruleServiceImpl extends ServiceImpl<MesChiefdataMsdcontrolruleMapper, MesChiefdataMsdcontrolrule> implements IMesChiefdataMsdcontrolruleService {

	@Autowired
	private MesChiefdataMsdcontrolruleMapper mesChiefdataMsdcontrolruleMapper;
	@Autowired
	private MesMsdcontrolruleItemMapper mesMsdcontrolruleItemMapper;
	
	@Override
	@Transactional
	public void saveMain(MesChiefdataMsdcontrolrule mesChiefdataMsdcontrolrule, List<MesMsdcontrolruleItem> mesMsdcontrolruleItemList) {
		mesChiefdataMsdcontrolruleMapper.insert(mesChiefdataMsdcontrolrule);
		if(mesMsdcontrolruleItemList!=null && mesMsdcontrolruleItemList.size()>0) {
			for(MesMsdcontrolruleItem entity:mesMsdcontrolruleItemList) {
				//外键设置
				entity.setMsdRuleid(mesChiefdataMsdcontrolrule.getId());
				mesMsdcontrolruleItemMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(MesChiefdataMsdcontrolrule mesChiefdataMsdcontrolrule,List<MesMsdcontrolruleItem> mesMsdcontrolruleItemList) {
		mesChiefdataMsdcontrolruleMapper.updateById(mesChiefdataMsdcontrolrule);
		
		//1.先删除子表数据
		mesMsdcontrolruleItemMapper.deleteByMainId(mesChiefdataMsdcontrolrule.getId());
		
		//2.子表数据重新插入
		if(mesMsdcontrolruleItemList!=null && mesMsdcontrolruleItemList.size()>0) {
			for(MesMsdcontrolruleItem entity:mesMsdcontrolruleItemList) {
				//外键设置
				entity.setMsdRuleid(mesChiefdataMsdcontrolrule.getId());
				mesMsdcontrolruleItemMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		mesMsdcontrolruleItemMapper.deleteByMainId(id);
		mesChiefdataMsdcontrolruleMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesMsdcontrolruleItemMapper.deleteByMainId(id.toString());
			mesChiefdataMsdcontrolruleMapper.deleteById(id);
		}
	}
	
}
