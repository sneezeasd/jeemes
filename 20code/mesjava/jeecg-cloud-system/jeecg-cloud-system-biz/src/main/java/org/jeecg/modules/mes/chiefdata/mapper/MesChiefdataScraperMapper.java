package org.jeecg.modules.mes.chiefdata.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataScraper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 刮刀建档
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
public interface MesChiefdataScraperMapper extends BaseMapper<MesChiefdataScraper> {

    @Update("UPDATE mes_chiefdata_scraper set point_usage_num=point_usage_num+1 WHERE scraper_sn=#{scraperSn}")
    boolean increasePointUsageNum(@Param("scraperSn")String scraperSn);
}
