package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesScanDocket;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 主数据—扫描单据
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
public interface IMesScanDocketService extends IService<MesScanDocket> {

}
