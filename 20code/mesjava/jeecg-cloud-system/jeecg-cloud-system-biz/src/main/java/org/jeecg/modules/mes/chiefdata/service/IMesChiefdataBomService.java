package org.jeecg.modules.mes.chiefdata.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataBom;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

/**
 * @Description: 主数据—BOM
 * @Author: jeecg-boot
 * @Date:   2020-10-20
 * @Version: V1.0
 */
public interface IMesChiefdataBomService extends IService<MesChiefdataBom> {

	/**
	 * 删除一对多
	 */
	public void delMain (String id);

	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);


	public boolean importMesChiefdataBom(Map<String, MultipartFile> fileMap);

	public boolean replacematerial(String id);
}
