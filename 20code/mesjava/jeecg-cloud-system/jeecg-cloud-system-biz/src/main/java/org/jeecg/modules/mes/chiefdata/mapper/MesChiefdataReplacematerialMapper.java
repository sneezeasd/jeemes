package org.jeecg.modules.mes.chiefdata.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataReplacematerial;

/**
 * @Description: 主数据—替代料
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface MesChiefdataReplacematerialMapper extends BaseMapper<MesChiefdataReplacematerial> {


    @Delete("DELETE  FROM  mes_chiefdata_replacematerial  WHERE  machine_sort = #{machineSort}")
    public boolean deleteMachineSort(@Param("machineSort") String machineSort);
}
