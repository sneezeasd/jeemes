package org.jeecg.modules.mes.organize.service;

import org.jeecg.modules.mes.organize.entity.MesOrganizeHoliday;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 组织—日历（假期）
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
public interface IMesOrganizeHolidayService extends IService<MesOrganizeHoliday> {

	public List<MesOrganizeHoliday> selectByMainId(String mainId);
}
