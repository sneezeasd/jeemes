package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesMaterialtableItem;
import org.jeecg.modules.mes.chiefdata.mapper.MesMaterialtableItemMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesMaterialtableItemService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 主数据—料站表明细
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesMaterialtableItemServiceImpl extends ServiceImpl<MesMaterialtableItemMapper, MesMaterialtableItem> implements IMesMaterialtableItemService {
	
	@Autowired
	private MesMaterialtableItemMapper mesMaterialtableItemMapper;
	
	@Override
	public List<MesMaterialtableItem> selectByMainId(String mainId) {
		return mesMaterialtableItemMapper.selectByMainId(mainId);
	}
}
