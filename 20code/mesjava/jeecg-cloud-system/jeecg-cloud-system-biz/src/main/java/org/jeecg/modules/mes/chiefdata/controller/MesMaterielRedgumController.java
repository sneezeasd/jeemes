package org.jeecg.modules.mes.chiefdata.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.modules.mes.chiefdata.entity.MesMaterielRedgum;
import org.jeecg.modules.mes.chiefdata.service.IMesMaterielRedgumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "主数据—辅料-红胶/锡膏")
@RestController
@RequestMapping("/chiefdata/mesMaterielRedgum")
@Slf4j
public class MesMaterielRedgumController {

    @Autowired
    private IMesMaterielRedgumService redgumService;


    @ApiOperation("根据物料号查询辅料信息")
    @GetMapping("/getBypCode")
    private MesMaterielRedgum getBypCode(@RequestParam("pCode") String pCode) {
        return redgumService.findBypCode(pCode);
    }


}
