package org.jeecg.modules.bi.service;

import org.jeecg.modules.bi.entity.zdOnlCgreportParam;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: onl_cgreport_param
 * @Author: wms-cloud
 * @Date:   2020-12-09
 * @Version: V1.0
 */
public interface IzdOnlCgreportParamService extends IService<zdOnlCgreportParam> {

	public List<zdOnlCgreportParam> selectByMainId(String mainId);
}
