package org.jeecg.modules.mes.mesapp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.mesapp.entity.MesAppFunction;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 主数据—APP功能模块
 * @Author: jeecg-boot
 * @Date:   2020-10-14
 * @Version: V1.0
 */
public interface MesAppFunctionMapper extends BaseMapper<MesAppFunction> {

}
