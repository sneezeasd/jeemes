package org.jeecg.modules.mes.order.vo;

import java.util.List;
import org.jeecg.modules.mes.order.entity.MesOrderAskpurchase;
import org.jeecg.modules.mes.order.entity.MesAskpurchaseItem;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 订单管理—请购单
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Data
@ApiModel(value="mes_order_askpurchasePage对象", description="订单管理—请购单")
public class MesOrderAskpurchasePage {

	/**id*/
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**单号*/
	@Excel(name = "单号", width = 15)
	@ApiModelProperty(value = "单号")
	private java.lang.String askCode;
	/**请购部门*/
	@Excel(name = "请购部门", width = 15)
	@ApiModelProperty(value = "请购部门")
	private java.lang.String askDepart;
	/**请购人员*/
	@Excel(name = "请购人员", width = 15)
	@ApiModelProperty(value = "请购人员")
	private java.lang.String askPerson;
	/**请购日期*/
	@Excel(name = "请购日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "请购日期")
	private java.util.Date askDate;
	/**必须日期*/
	@Excel(name = "必须日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "必须日期")
	private java.util.Date necessaryDate;
	/**请购原因*/
	@Excel(name = "请购原因", width = 15)
	@ApiModelProperty(value = "请购原因")
	private java.lang.String askReason;
	/**审批人*/
	@Excel(name = "审批人", width = 15)
	@ApiModelProperty(value = "审批人")
	private java.lang.String verifyPerson;
	/**审批状态*/
	@Excel(name = "审批状态", width = 15)
	@ApiModelProperty(value = "审批状态")
	private java.lang.String verifyState;
	/**单据状态*/
	@Excel(name = "单据状态", width = 15)
	@ApiModelProperty(value = "单据状态")
	private java.lang.String docketState;
	/**备注*/
	@Excel(name = "备注", width = 15)
	@ApiModelProperty(value = "备注")
	private java.lang.String notes;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
	@ApiModelProperty(value = "备用1")
	private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
	@ApiModelProperty(value = "备用2")
	private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
	
	@ExcelCollection(name="订单管理—请购单子表")
	@ApiModelProperty(value = "订单管理—请购单子表")
	private List<MesAskpurchaseItem> mesAskpurchaseItemList;
	
}
