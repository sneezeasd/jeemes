package org.jeecg.common.util;

/**
 * 处理数字工具类 胡盛桐
 */
public class NumberUtils {
    /**
     * 只保留字符串中的数字和小数点
     * @param STR
     * @return
     */
    public static String getNumber_charAt(String STR){
        StringBuffer sBuffer = new StringBuffer();
        for (int i = 0; i < STR.length(); i++) {
            char ch = STR.charAt(i);
            if(48<=ch&&ch<=57||ch==46){
                sBuffer.append(ch);
            }
        }
        return sBuffer.toString();
    }
}
