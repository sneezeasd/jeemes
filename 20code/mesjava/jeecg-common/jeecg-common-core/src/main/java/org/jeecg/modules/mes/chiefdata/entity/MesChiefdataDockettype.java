package org.jeecg.modules.mes.chiefdata.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 主数据—单据类型配置
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Data
@TableName("mes_chiefdata_dockettype")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_chiefdata_dockettype对象", description="主数据—单据类型配置")
public class MesChiefdataDockettype implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**类型编码*/
	@Excel(name = "类型编码", width = 15)
    @ApiModelProperty(value = "类型编码")
    private java.lang.String typeCode;
	/**类型名称*/
	@Excel(name = "类型名称", width = 15)
    @ApiModelProperty(value = "类型名称")
    private java.lang.String typeName;
	/**单据前缀*/
	@Excel(name = "单据前缀", width = 15)
    @ApiModelProperty(value = "单据前缀")
    private java.lang.String docketPrefix;
	/**出入库类型*/
	@Excel(name = "出入库类型", width = 15, dicCode = "corssware_type")
	@Dict(dicCode = "corssware_type")
    @ApiModelProperty(value = "出入库类型")
    private java.lang.String corsswareType;
	/**ERP单别*/
	@Excel(name = "ERP单别", width = 15)
    @ApiModelProperty(value = "ERP单别")
    private java.lang.String erpType;
	/**是否送检*/
	@Excel(name = "是否送检", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "是否送检")
    private java.lang.String ifSnedcheck;
	/**交接标志*/
	@Excel(name = "交接标志", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "交接标志")
    private java.lang.String handoverToken;
	/**ERP同步标志*/
	@Excel(name = "ERP同步标志", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "ERP同步标志")
    private java.lang.String erpToken;
	/**检测阶别*/
	@Excel(name = "检测阶别", width = 15, dicCode = "check_grade")
	@Dict(dicCode = "check_grade")
    @ApiModelProperty(value = "检测阶别")
    private java.lang.String checkGrade;
	/**检验类型*/
	@Excel(name = "检验类型", width = 15, dicCode = "check_type")
	@Dict(dicCode = "check_type")
    @ApiModelProperty(value = "检验类型")
    private java.lang.String checkType;
	/**管控超收标志*/
	@Excel(name = "管控超收标志", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "管控超收标志")
    private java.lang.String controlOverget;
	/**管控超发标志*/
	@Excel(name = "管控超发标志", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "管控超发标志")
    private java.lang.String controlOversend;
	/**PDA显示标志*/
	@Excel(name = "PDA显示标志", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "PDA显示标志")
    private java.lang.String pdaShow;
	/**交接方式*/
	@Excel(name = "交接方式", width = 15, dicCode = "handover_manner")
	@Dict(dicCode = "handover_manner")
    @ApiModelProperty(value = "交接方式")
    private java.lang.String handoverManner;
	/**生成规则*/
	@Excel(name = "生成规则", width = 15)
    @ApiModelProperty(value = "生成规则")
    private java.lang.String createRule;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
