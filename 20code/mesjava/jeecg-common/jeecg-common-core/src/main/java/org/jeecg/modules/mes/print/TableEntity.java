package org.jeecg.modules.mes.print;

/**
 * TODO
 *
 * @author xuhow
 * @version 1.0.0
 * @date 2020/10/12 20:04
 */
public class TableEntity {

    private String query01;
    private String query02;
    private String query03;
    private String query04;
    private String query05;
    private String query06;
    private String query07;
    private String query08;
    private String query09;
    private String query010;


    public String getQuery01() {
        return query01;
    }

    public void setQuery01(String query01) {
        this.query01 = query01;
    }

    public String getQuery02() {
        return query02;
    }

    public void setQuery02(String query02) {
        this.query02 = query02;
    }

    public String getQuery03() {
        return query03;
    }

    public void setQuery03(String query03) {
        this.query03 = query03;
    }

    public String getQuery04() {
        return query04;
    }

    public void setQuery04(String query04) {
        this.query04 = query04;
    }

    public String getQuery05() {
        return query05;
    }

    public void setQuery05(String query05) {
        this.query05 = query05;
    }

    public String getQuery06() {
        return query06;
    }

    public void setQuery06(String query06) {
        this.query06 = query06;
    }

    public String getQuery07() {
        return query07;
    }

    public void setQuery07(String query07) {
        this.query07 = query07;
    }

    public String getQuery08() {
        return query08;
    }

    public void setQuery08(String query08) {
        this.query08 = query08;
    }

    public String getQuery09() {
        return query09;
    }

    public void setQuery09(String query09) {
        this.query09 = query09;
    }

    public String getQuery010() {
        return query010;
    }

    public void setQuery010(String query010) {
        this.query010 = query010;
    }
}
