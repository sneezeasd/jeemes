package org.jeecg.modules.mes.storage.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 仓库管理-领料清单
 * @Author: jeecg-boot
 * @Date:   2020-11-16
 * @Version: V1.0
 */
@Data
@TableName("mes_materiel_occupy")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_materiel_occupy对象", description="仓库管理-领料清单")
public class MesMaterielOccupy implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**生产订单id*/
	@Excel(name = "生产订单id", width = 15)
    @ApiModelProperty(value = "生产订单id")
    private java.lang.String orderId;
	/**订单编号*/
	@Excel(name = "订单编号", width = 15)
    @ApiModelProperty(value = "订单编号")
    private java.lang.String orderCode;
	/**订单名称*/
	@Excel(name = "订单名称", width = 15)
    @ApiModelProperty(value = "订单名称")
    private java.lang.String orderName;
	/**物料id*/
	@Excel(name = "物料id", width = 15)
    @ApiModelProperty(value = "物料id")
    private java.lang.String materielId;
	/**物料料号*/
	@Excel(name = "物料料号", width = 15)
    @ApiModelProperty(value = "物料料号")
    private java.lang.String materielCode;
	/**物料名称*/
	@Excel(name = "物料名称", width = 15)
    @ApiModelProperty(value = "物料名称")
    private java.lang.String materielName;
	/**物料规格*/
	@Excel(name = "物料规格", width = 15)
    @ApiModelProperty(value = "物料规格")
    private java.lang.String materielGague;
	/**领用数量*/
	@Excel(name = "领用数量", width = 15)
    @ApiModelProperty(value = "领用数量")
    private java.lang.String occupyNum;
	/**领用人*/
	@Excel(name = "领用人", width = 15)
    @ApiModelProperty(value = "领用人")
    private java.lang.String occupyPerson;
	/**领用时间*/
	@Excel(name = "领用时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "领用时间")
    private java.util.Date occupyTime;
	/**单位*/
	@Excel(name = "单位", width = 15)
    @ApiModelProperty(value = "单位")
    private java.lang.String unit;
	/**未使用数量*/
	@Excel(name = "未使用数量", width = 15)
    @ApiModelProperty(value = "未使用数量")
    private java.lang.String unusedNum;
	/**发料数量*/
	@Excel(name = "发料数量", width = 15)
    @ApiModelProperty(value = "发料数量")
    private java.lang.String sendNum;
	/**退料数量*/
	@Excel(name = "退料数量", width = 15)
    @ApiModelProperty(value = "退料数量")
    private java.lang.String withdrawNum;
	/**
     * 转产数量
     */
    @Excel(name = "转产数量", width = 15)
    @ApiModelProperty(value = "转产数量")
    private java.lang.String transformNum;
    /**
     * 备用6
     */
    @Excel(name = "制令单号", width = 15)
    @ApiModelProperty(value = "制令单号")
    private java.lang.String query6;
}
