package org.jeecg.modules.mes.order.vo;

import java.util.List;
import org.jeecg.modules.mes.order.entity.MesOrderSale;
import org.jeecg.modules.mes.order.entity.MesSaleItem;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 订单管理—销售订单
 * @Author: jeecg-boot
 * @Date:   2020-11-06
 * @Version: V1.0
 */
@Data
@ApiModel(value="mes_order_salePage对象", description="订单管理—销售订单")
public class MesOrderSalePage {

	/**id*/
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**订单编号*/
	@Excel(name = "订单编号", width = 15)
	@ApiModelProperty(value = "订单编号")
	private java.lang.String orderCode;
	/**订单类型*/
	@Excel(name = "订单类型", width = 15)
	@ApiModelProperty(value = "订单类型")
	private java.lang.String orderType;
	/**订单名称*/
	@Excel(name = "订单名称", width = 15)
	@ApiModelProperty(value = "订单名称")
	private java.lang.String orderName;
	/**净价值*/
	@Excel(name = "净价值", width = 15)
	@ApiModelProperty(value = "净价值")
	private java.lang.String grossPrice;
	/**货币*/
	@Excel(name = "货币", width = 15)
	@ApiModelProperty(value = "货币")
	private java.lang.String currency;
	/**售达方id*/
	@Excel(name = "售达方id", width = 15)
	@ApiModelProperty(value = "售达方id")
	private java.lang.String saleId;
	/**售达方编号*/
	@Excel(name = "售达方编号", width = 15)
	@ApiModelProperty(value = "售达方编号")
	private java.lang.String saleCode;
	/**售达方*/
	@Excel(name = "售达方", width = 15)
	@ApiModelProperty(value = "售达方")
	private java.lang.String saleName;
	/**采购订单id*/
	@Excel(name = "采购订单id", width = 15)
	@ApiModelProperty(value = "采购订单id")
	private java.lang.String purchaseId;
	/**采购订单编号*/
	@Excel(name = "采购订单编号", width = 15)
	@ApiModelProperty(value = "采购订单编号")
	private java.lang.String purchaseCode;
	/**采购订单日期*/
	@Excel(name = "采购订单日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "采购订单日期")
	private java.util.Date purchaseDate;
	/**请求交货日期*/
	@Excel(name = "请求交货日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "请求交货日期")
	private java.util.Date deliveryDate;
	/**备注*/
	@Excel(name = "备注", width = 15)
	@ApiModelProperty(value = "备注")
	private java.lang.String notes;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
	@ApiModelProperty(value = "备用2")
	private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**是否报关（默认为0）*/
	@Excel(name = "是否报关（默认为0）", width = 15)
	@ApiModelProperty(value = "是否报关（默认为0）")
	private java.lang.String importState;

	@ExcelCollection(name="订单管理—销售订单子表")
	@ApiModelProperty(value = "订单管理—销售订单子表")
	private List<MesSaleItem> mesSaleItemList;

}
