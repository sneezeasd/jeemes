package org.jeecg.modules.mes.produce.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: mes_file_collection_aoi_detail
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
@Data
@TableName("mes_file_collection_aoi_detail")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_file_collection_aoi_detail对象", description="mes_file_collection_aoi_detail")
public class MesFileCollectionAoiDetail implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**主表id*/
	@Excel(name = "主表id", width = 15)
    @ApiModelProperty(value = "主表id")
    private String mainId;
	/**PCB编号*/
	@Excel(name = "PCB编号", width = 15)
    @ApiModelProperty(value = "PCB编号")
    private String pcbNo;
	/**拼板编号*/
	@Excel(name = "拼板编号", width = 15)
    @ApiModelProperty(value = "拼板编号")
    private String joinNo;
	/**元件位置*/
	@Excel(name = "元件位置", width = 15)
    @ApiModelProperty(value = "元件位置")
    private String compoPostion;
	/**角度*/
	@Excel(name = "角度", width = 15)
    @ApiModelProperty(value = "角度")
    private String angle;
	/**X坐标*/
	@Excel(name = "X坐标", width = 15)
    @ApiModelProperty(value = "X坐标")
    private String pointX;
	/**Y坐标*/
	@Excel(name = "Y坐标", width = 15)
    @ApiModelProperty(value = "Y坐标")
    private String pointY;
	/**NG名称*/
	@Excel(name = "NG名称", width = 15)
    @ApiModelProperty(value = "NG名称")
    private String ngName;
	/**料号*/
	@Excel(name = "料号", width = 15)
    @ApiModelProperty(value = "料号")
    private String materNo;
	/**条码*/
	@Excel(name = "条码", width = 15)
    @ApiModelProperty(value = "条码")
    private String code;
	/**图片*/
	@Excel(name = "图片", width = 15)
    @ApiModelProperty(value = "图片")
    private String pic;
}
