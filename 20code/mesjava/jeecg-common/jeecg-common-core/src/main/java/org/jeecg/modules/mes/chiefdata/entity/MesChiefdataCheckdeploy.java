package org.jeecg.modules.mes.chiefdata.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 主数据—检验配置
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Data
@TableName("mes_chiefdata_checkdeploy")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_chiefdata_checkdeploy对象", description="主数据—检验配置")
public class MesChiefdataCheckdeploy implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**检测阶别*/
	@Excel(name = "检测阶别", width = 15, dicCode = "check_grade")
	@Dict(dicCode = "check_grade")
    @ApiModelProperty(value = "检测阶别")
    private java.lang.String checkGrade;
	/**检验类型*/
	@Excel(name = "检验类型", width = 15)
    @ApiModelProperty(value = "检验类型")
    private java.lang.String examineType;
	/**不良品处理*/
	@Excel(name = "不良品处理", width = 15, dicCode = "badgood_deal")
	@Dict(dicCode = "badgood_deal")
    @ApiModelProperty(value = "不良品处理")
    private java.lang.String badgoodDeal;
	/**生成方式*/
	@Excel(name = "生成方式", width = 15, dicCode = "create_manner")
	@Dict(dicCode = "create_manner")
    @ApiModelProperty(value = "生成方式")
    private java.lang.String createManner;
	/**批次划分方式*/
	@Excel(name = "批次划分方式", width = 15)
    @ApiModelProperty(value = "批次划分方式")
    private java.lang.String batchDivide;
	/**对应值(pcs/h)*/
	@Excel(name = "对应值(pcs/h)", width = 15)
    @ApiModelProperty(value = "对应值(pcs/h)")
    private java.lang.String relativeValue;
	/**判定方式*/
	@Excel(name = "判定方式", width = 15, dicCode = "judge_manner")
	@Dict(dicCode = "judge_manner")
    @ApiModelProperty(value = "判定方式")
    private java.lang.String judgeManner;
	/**默认标志*/
	@Excel(name = "默认标志", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "默认标志")
    private java.lang.String defaultToken;
	/**自动审核*/
	@Excel(name = "自动审核", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "自动审核")
    private java.lang.String autoCheck;
	/**加严放宽标志*/
	@Excel(name = "加严放宽标志", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "加严放宽标志")
    private java.lang.String strictWidetoken;
	/**加严—>正常*/
	@Excel(name = "加严—>正常", width = 15)
    @ApiModelProperty(value = "加严—>正常")
    private java.lang.String strictNormal;
	/**正常—>放宽*/
	@Excel(name = "正常—>放宽", width = 15)
    @ApiModelProperty(value = "正常—>放宽")
    private java.lang.String normalWide;
	/**放宽—>正常*/
	@Excel(name = "放宽—>正常", width = 15)
    @ApiModelProperty(value = "放宽—>正常")
    private java.lang.String wideNormal;
	/**加严不合格批次*/
	@Excel(name = "加严不合格批次", width = 15)
    @ApiModelProperty(value = "加严不合格批次")
    private java.lang.String failedBatch;
	/**加严计算批次*/
	@Excel(name = "加严计算批次", width = 15)
    @ApiModelProperty(value = "加严计算批次")
    private java.lang.String figureBatch;
	/**单号生成规则*/
	@Excel(name = "单号生成规则", width = 15)
    @ApiModelProperty(value = "单号生成规则")
    private java.lang.String codeCreaterule;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
