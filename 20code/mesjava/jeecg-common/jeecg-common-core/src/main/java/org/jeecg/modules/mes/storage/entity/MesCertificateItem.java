package org.jeecg.modules.mes.storage.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecg.modules.mes.produce.entity.MesCommandbillPitem;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: 物料凭证项目
 * @Author: jeecg-boot
 * @Date:   2020-11-11
 * @Version: V1.0
 */
@Data
@TableName("mes_certificate_item")
@ApiModel(value="mes_certificate_perk对象", description="物料凭证项目")
public class MesCertificateItem implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**移动编号*/
	@Excel(name = "移动编号", width = 15)
	@ApiModelProperty(value = "移动编号")
	private java.lang.String mobileCode;
	/**移动类型*/
	@Excel(name = "移动类型", width = 15)
	@ApiModelProperty(value = "移动类型")
	private java.lang.String mobileType;
	/**物料id*/
	@Excel(name = "物料id", width = 15)
	@ApiModelProperty(value = "物料id")
	private java.lang.String materielId;
	/**物料编号*/
	@Excel(name = "物料编号", width = 15)
	@ApiModelProperty(value = "物料编号")
	private java.lang.String materielCode;
	/**物料名称*/
	@Excel(name = "物料名称", width = 15)
	@ApiModelProperty(value = "物料名称")
	private java.lang.String materielName;
	/**物料规格*/
	@Excel(name = "物料规格", width = 15)
	@ApiModelProperty(value = "物料规格")
	private java.lang.String materielGauge;
	/**录入数量*/
	@Excel(name = "录入数量", width = 15)
	@ApiModelProperty(value = "录入数量")
	private java.lang.String inputNum;
	/**录入项单位*/
	@Excel(name = "录入项单位", width = 15)
	@ApiModelProperty(value = "录入项单位")
	private java.lang.String inputUnit;
	/**工厂id*/
	@Excel(name = "工厂id", width = 15)
	@ApiModelProperty(value = "工厂id")
	private java.lang.String factoryId;
	/**工厂编号*/
	@Excel(name = "工厂编号", width = 15)
	@ApiModelProperty(value = "工厂编号")
	private java.lang.String factoryCode;
	/**工厂名称*/
	@Excel(name = "工厂名称", width = 15)
	@ApiModelProperty(value = "工厂名称")
	private java.lang.String factoryName;
	/**存储位置*/
	@Excel(name = "存储位置", width = 15)
	@ApiModelProperty(value = "存储位置")
	private java.lang.String storageSite;
	/**预留编号*/
	@Excel(name = "预留编号", width = 15)
	@ApiModelProperty(value = "预留编号")
	private java.lang.String reserveCode;
	/**抬头id*/
	@ApiModelProperty(value = "抬头id")
	private java.lang.String perkId;
	/**已检验*/
	@Excel(name = "已检验", width = 15)
    @Dict(dicCode = "yn")
	@ApiModelProperty(value = "已检验")
	private java.lang.String ifInspect;
	/**未入库数量*/
	@Excel(name = "未入库数量", width = 15)
	@ApiModelProperty(value = "未入库数量")
	private java.lang.String unstorageNum;
	/**已入库*/
	@Excel(name = "已入库", width = 15)
	@ApiModelProperty(value = "已入库")
	private java.lang.String ifStorage;
	/**
     * 制令单id
     */
    @Excel(name = "制令单id", width = 15)
    @ApiModelProperty(value = "制令单id")
    private java.lang.String commandId;
    /**
     * 备用5
     */
    @Excel(name = "指令单id", width = 15)
    @ApiModelProperty(value = "制令单id")
    private java.lang.String query5;
    /**
     * 备用6
	 */
	@Excel(name = "备用", width = 15)
	@ApiModelProperty(value = "备用")
	private java.lang.String query6;

	//	@ExcelCollection(name="制令单Bom")
	@TableField(exist = false)
	@ApiModelProperty(value = "制令单Bom")
	private List<MesCommandbillPitem> mesCommandbillPitemList;

//	@TableField(exist = false)
//	@ApiModelProperty(value = "制令单Bom对象")
//	private MesCommandbillPitem mesCommandbillPitem;
	/**
	 * 客户料号
	 */
	@Excel(name = "客户料号", width = 15)
	@ApiModelProperty(value = "客户料号")
	@TableField(exist = false)
	private java.lang.String clientCode;

	@ApiModelProperty(value = "退料数量")
	@TableField(exist = false)
	private java.lang.String returnNum;
}
