package org.jeecg.modules.mes.organize.vo;

import java.util.List;
import org.jeecg.modules.mes.organize.entity.MesOrganizeCalendar;
import org.jeecg.modules.mes.organize.entity.MesOrganizeHoliday;
import org.jeecg.modules.mes.organize.entity.MesOrganizeClasstype;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 组织—日历
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Data
@ApiModel(value="mes_organize_calendarPage对象", description="组织—日历")
public class MesOrganizeCalendarPage {

	/**id*/
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**日历编号*/
	@Excel(name = "日历编号", width = 15)
	@ApiModelProperty(value = "日历编号")
	private java.lang.String calCode;
	/**日历名称*/
	@Excel(name = "日历名称", width = 15)
	@ApiModelProperty(value = "日历名称")
	private java.lang.String calName;
	/**开始日期*/
	@Excel(name = "开始日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "开始日期")
	private java.util.Date startDate;
	/**结束日期*/
	@Excel(name = "结束日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "结束日期")
	private java.util.Date endDate;
	/**周起始*/
	@Excel(name = "周起始", width = 15)
	@ApiModelProperty(value = "周起始")
	private java.lang.String weekBegin;
	/**周末*/
	@Excel(name = "周末", width = 15)
	@ApiModelProperty(value = "周末")
	private java.lang.String workdayType;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
	
	@ExcelCollection(name="组织—日历（假期）")
	@ApiModelProperty(value = "组织—日历（假期）")
	private List<MesOrganizeHoliday> mesOrganizeHolidayList;
	@ExcelCollection(name="组织—日历（班别）")
	@ApiModelProperty(value = "组织—日历（班别）")
	private List<MesOrganizeClasstype> mesOrganizeClasstypeList;
	
}
