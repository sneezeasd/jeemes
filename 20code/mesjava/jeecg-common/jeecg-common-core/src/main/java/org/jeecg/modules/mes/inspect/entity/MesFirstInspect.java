package org.jeecg.modules.mes.inspect.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 质检中心-首件检测
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Data
@TableName("mes_first_inspect")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_first_inspect对象", description="质检中心-首件检测")
public class MesFirstInspect implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**工单*/
	@Excel(name = "工单", width = 15)
    @ApiModelProperty(value = "工单")
    private java.lang.String workBill;
	/**机种料号*/
	@Excel(name = "机种料号", width = 15)
    @ApiModelProperty(value = "机种料号")
    private java.lang.String machineCode;
	/**机种名称*/
	@Excel(name = "机种名称", width = 15)
    @ApiModelProperty(value = "机种名称")
    private java.lang.String machineName;
	/**规格*/
	@Excel(name = "规格", width = 15)
    @ApiModelProperty(value = "规格")
    private java.lang.String gague;
	/**生产阶别*/
	@Excel(name = "生产阶别", width = 15)
    @ApiModelProperty(value = "生产阶别")
    private java.lang.String produceGrade;
	/**首件检测数量*/
	@Excel(name = "首件检测数量", width = 15)
    @ApiModelProperty(value = "首件检测数量")
    private java.lang.String firstInspectnum;
	/**判定结果*/
	@Excel(name = "判定结果", width = 15)
    @ApiModelProperty(value = "判定结果")
    private java.lang.String judgeResult;
	/**实际检测数量*/
	@Excel(name = "实际检测数量", width = 15)
    @ApiModelProperty(value = "实际检测数量")
    private java.lang.String realInspectnum;
	/**产品SN*/
	@Excel(name = "产品SN", width = 15)
    @ApiModelProperty(value = "产品SN")
    private java.lang.String productSn;
	/**检测产品SN*/
	@Excel(name = "检测产品SN", width = 15)
    @ApiModelProperty(value = "检测产品SN")
    private java.lang.String inspectPsn;
	/**检测内容*/
	@Excel(name = "检测内容", width = 15)
    @ApiModelProperty(value = "检测内容")
    private java.lang.String inspectContent;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
