package org.jeecg.modules.mes.chiefdata.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 主数据—物料管控
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Data
@TableName("mes_chiefdata_materielcontrol")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_chiefdata_materielcontrol对象", description="主数据—物料管控")
public class MesChiefdataMaterielcontrol implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**机种料号*/
	@Excel(name = "机种料号", width = 15)
    @ApiModelProperty(value = "机种料号")
    private java.lang.String machineCode;
	/**管控类型*/
	@Excel(name = "管控类型", width = 15, dicCode = "mcontrol_type")
	@Dict(dicCode = "mcontrol_type")
    @ApiModelProperty(value = "管控类型")
    private java.lang.String controlType;
	/**机种名称*/
	@Excel(name = "机种名称", width = 15)
    @ApiModelProperty(value = "机种名称")
    private java.lang.String machineName;
	/**机种规格*/
	@Excel(name = "机种规格", width = 15)
    @ApiModelProperty(value = "机种规格")
    private java.lang.String machineGague;
	/**管控料号*/
	@Excel(name = "管控料号", width = 15)
    @ApiModelProperty(value = "管控料号")
    private java.lang.String controlCode;
	/**管控方式*/
	@Excel(name = "管控方式", width = 15, dicCode = "mcontrol_manner")
	@Dict(dicCode = "mcontrol_manner")
    @ApiModelProperty(value = "管控方式")
    private java.lang.String controlManner;
	/**物料名称*/
	@Excel(name = "物料名称", width = 15)
    @ApiModelProperty(value = "物料名称")
    private java.lang.String materielName;
	/**物料规格*/
	@Excel(name = "物料规格", width = 15)
    @ApiModelProperty(value = "物料规格")
    private java.lang.String materielGague;
	/**生产阶别*/
	@Excel(name = "生产阶别", width = 15)
    @ApiModelProperty(value = "生产阶别")
    private java.lang.String produceGrade;
	/**管控工序*/
	@Excel(name = "管控工序", width = 15)
    @ApiModelProperty(value = "管控工序")
    private java.lang.String controlProcess;
	/**组立顺序*/
	@Excel(name = "组立顺序", width = 15)
    @ApiModelProperty(value = "组立顺序")
    private java.lang.String groupSequence;
	/**管控数量*/
	@Excel(name = "管控数量", width = 15)
    @ApiModelProperty(value = "管控数量")
    private java.lang.String controlNum;
	/**加工面别*/
	@Excel(name = "加工面别", width = 15, dicCode = "process_face")
	@Dict(dicCode = "process_face")
    @ApiModelProperty(value = "加工面别")
    private java.lang.String processFace;
	/**条码规则*/
	@Excel(name = "条码规则", width = 15)
    @ApiModelProperty(value = "条码规则")
    private java.lang.String barcodeRule;
	/**物料来源*/
	@Excel(name = "物料来源", width = 15, dicCode = "materiel_source")
	@Dict(dicCode = "materiel_source")
    @ApiModelProperty(value = "物料来源")
    private java.lang.String materielSource;
	/**管控提示*/
	@Excel(name = "管控提示", width = 15)
    @ApiModelProperty(value = "管控提示")
    private java.lang.String controlClue;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String notes;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
