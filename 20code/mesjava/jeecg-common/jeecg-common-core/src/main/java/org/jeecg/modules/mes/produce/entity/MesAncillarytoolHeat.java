package org.jeecg.modules.mes.produce.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 制造中心-辅料回温
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
@Data
@TableName("mes_ancillarytool_heat")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_ancillarytool_heat对象", description="制造中心-辅料回温")
public class MesAncillarytoolHeat implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**辅料SN*/
	@Excel(name = "辅料SN", width = 15)
    @ApiModelProperty(value = "辅料SN")
    private java.lang.String ancillarySn;
	/**状态*/
	@Excel(name = "状态", width = 15, dicCode = "stir_state")
	@Dict(dicCode = "stir_state")
    @ApiModelProperty(value = "状态")
    private java.lang.String state;
	/**放行标志*/
	@Excel(name = "放行标志", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "放行标志")
    private java.lang.String letoutToken;
	/**辅料类型*/
	@Excel(name = "辅料类型", width = 15)
    @ApiModelProperty(value = "辅料类型")
    private java.lang.String ancillaryType;
	/**辅料料号*/
	@Excel(name = "辅料料号", width = 15)
    @ApiModelProperty(value = "辅料料号")
    private java.lang.String ancillaryCode;
	/**辅料名称*/
	@Excel(name = "辅料名称", width = 15)
    @ApiModelProperty(value = "辅料名称")
    private java.lang.String ancillaryName;
	/**辅料规格*/
	@Excel(name = "辅料规格", width = 15)
    @ApiModelProperty(value = "辅料规格")
    private java.lang.String ancillaryGague;
	/**已回温次数*/
	@Excel(name = "已回温次数", width = 15)
    @ApiModelProperty(value = "已回温次数")
    private java.lang.String heatedNum;
	/**回温次数上限*/
	@Excel(name = "回温次数上限", width = 15)
    @ApiModelProperty(value = "回温次数上限")
    private java.lang.String heatNumlimit;
	/**回温时间(分钟)*/
	@Excel(name = "回温时间(分钟)", width = 15)
    @ApiModelProperty(value = "回温时间(分钟)")
    private java.lang.String heatTime;
	/**回温开始时间*/
	@Excel(name = "回温开始时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "回温开始时间")
    private java.util.Date heatBegintime;
	/**预计结束时间*/
	@Excel(name = "预计结束时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "预计结束时间")
    private java.util.Date expectFinishtime;
	/**最大回温时间*/
	@Excel(name = "最大回温时间", width = 15)
    @ApiModelProperty(value = "最大回温时间")
    private java.lang.String maxHeatime;
	/**回温结束时间*/
	@Excel(name = "回温结束时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "回温结束时间")
    private java.util.Date endHeatime;
	/**操作员*/
	@Excel(name = "操作员", width = 15)
    @ApiModelProperty(value = "操作员")
    private java.lang.String operator;
	/**放行说明*/
	@Excel(name = "放行说明", width = 15)
    @ApiModelProperty(value = "放行说明")
    private java.lang.String openExplain;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
    /**
     * 备用5
     */
    @Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
    /**
     * 备用6
     */
    @Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
    /**
     * 制令单id
     */
    @TableField(exist = false)
    @Excel(name = "制令单id", width = 15)
    @ApiModelProperty(value = "制令单id")
    private java.lang.String commandbillId;
}
