package org.jeecg.modules.mes.produce.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: 制造中心—制令单BOM项目
 * @Author: jeecg-boot
 * @Date:   2020-12-17
 * @Version: V1.0
 */
@Data
@TableName("mes_commandbill_pitem")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_commandbill_pitem对象", description="制造中心—制令单BOM项目")
public class MesCommandbillPitem implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**物料id*/
	@Excel(name = "物料id", width = 15)
    @ApiModelProperty(value = "物料id")
    private java.lang.String materielId;
	/**物料类型*/
	@Excel(name = "物料类型", width = 15)
    @ApiModelProperty(value = "物料类型")
    private java.lang.String materielType;
	/**料号*/
	@Excel(name = "料号", width = 15)
    @ApiModelProperty(value = "料号")
    private java.lang.String materielCode;
	/**物料*/
	@Excel(name = "物料", width = 15)
    @ApiModelProperty(value = "物料")
    private java.lang.String materielName;
	/**物料规格*/
	@Excel(name = "物料规格", width = 15)
    @ApiModelProperty(value = "物料规格")
    private java.lang.String materielGauge;
	/**需求数量*/
	@Excel(name = "需求数量", width = 15)
    @ApiModelProperty(value = "需求数量")
    private java.lang.String requireNum;
	/**计量单位*/
	@Excel(name = "计量单位", width = 15)
    @ApiModelProperty(value = "计量单位")
    private java.lang.String countUnit;
	/**提货数量*/
	@Excel(name = "提货数量", width = 15)
    @ApiModelProperty(value = "提货数量")
    private java.lang.String pickupNum;
	/**承诺数量*/
	@Excel(name = "承诺数量", width = 15)
    @ApiModelProperty(value = "承诺数量")
    private java.lang.String commitNum;
	/**工厂id*/
	@Excel(name = "工厂id", width = 15)
    @ApiModelProperty(value = "工厂id")
    private java.lang.String factoryId;
	/**工厂编号*/
	@Excel(name = "工厂编号", width = 15)
    @ApiModelProperty(value = "工厂编号")
    private java.lang.String factoryCode;
	/**工厂名称*/
	@Excel(name = "工厂名称", width = 15)
    @ApiModelProperty(value = "工厂名称")
    private java.lang.String factoryName;
	/**库存地点*/
	@Excel(name = "库存地点", width = 15)
    @ApiModelProperty(value = "库存地点")
    private java.lang.String storageSite;
	/**正面用量*/
	@Excel(name = "正面用量", width = 15)
    @ApiModelProperty(value = "正面用量")
    private java.lang.String frontQuantity;
	/**库存数量*/
	@Excel(name = "库存数量", width = 15)
    @ApiModelProperty(value = "库存数量")
    private java.lang.String stockNum;
	/**已发料数量*/
	@Excel(name = "已发料数量", width = 15)
    @ApiModelProperty(value = "已发料数量")
    private java.lang.String deliveryNum;
	/**已上料数量*/
	@Excel(name = "已上料数量", width = 15)
    @ApiModelProperty(value = "已上料数量")
    private java.lang.String unglazeNum;
    /**已退料数量*/
    @Excel(name = "已退料数量", width = 15)
    @ApiModelProperty(value = "已退料数量")
    private java.lang.String withdrawNum;
	/**发料状态*/
	@Excel(name = "发料状态", width = 15)
    @ApiModelProperty(value = "发料状态")
    private java.lang.String ifDelivery;
	/**上料状态*/
	@Excel(name = "上料状态", width = 15)
    @ApiModelProperty(value = "上料状态")
    private java.lang.String ifGlaze;
	/**真实数量*/
	@Excel(name = "真实数量", width = 15)
    @ApiModelProperty(value = "真实数量")
    private java.lang.String realNum;
	/**生产订单id*/
    @Excel(name = "生产订单id", width = 15)
    @ApiModelProperty(value = "生产订单id")
    private java.lang.String proorderId;
    /**生产订单子表id*/
    @Excel(name = "生产订单子表id", width = 15)
    @ApiModelProperty(value = "生产订单子表id")
    private java.lang.String orderitemId;
	/**制令单id*/
	@Excel(name = "制令单id", width = 15)
    @ApiModelProperty(value = "制令单id")
    private java.lang.String commandbillId;
    /**制令单号*/
    @Excel(name = "制令单号", width = 15)
    @ApiModelProperty(value = "制令单号")
    private java.lang.String commandbillCode;
    /**用量*/
    @Excel(name = "用量", width = 15)
    @ApiModelProperty(value = "用量")
    private java.lang.String quantity;
    /**建议退料数量*/
    @Excel(name = "建议退料数量", width = 15)
    @ApiModelProperty(value = "建议退料数量")
    private java.lang.String adviceNum;
    /**实际退料数量*/
    @Excel(name = "实际退料数量", width = 15)
    @ApiModelProperty(value = "实际退料数量")
    private java.lang.String virtualNum;
    /**消耗数量*/
    @Excel(name = "消耗数量", width = 15)
    @ApiModelProperty(value = "消耗数量")
    private java.lang.String consumeNum;
    /**转产数量*/
    @Excel(name = "转产数量", width = 15)
    @ApiModelProperty(value = "转产数量")
    private java.lang.String transformNum;
    /**客户料号*/
//    @Excel(name = "客户料号", width = 15)
    @ApiModelProperty(value = "客户料号")
    @TableField(exist = false)
    private java.lang.String clientCode;
    /**二维码id*/
    @TableField(exist = false)
    private java.lang.String wholid;

}
