package org.jeecg.modules.mes.produce.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 机种项目配置-检查项目
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@ApiModel(value="mes_mechanism_deployinfo对象", description="机种项目配置-基本信息")
@Data
@TableName("mes_mechanism_checkproject")
public class MesMechanismCheckproject implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**项目编码*/
	@Excel(name = "项目编码", width = 15)
	@ApiModelProperty(value = "项目编码")
	private java.lang.String projectCode;
	/**项目名称*/
	@Excel(name = "项目名称", width = 15)
	@ApiModelProperty(value = "项目名称")
	private java.lang.String projectName;
	/**项目类型*/
	@Excel(name = "项目类型", width = 15)
	@ApiModelProperty(value = "项目类型")
	private java.lang.String projectType;
	/**生产阶别*/
	@Excel(name = "生产阶别", width = 15)
	@ApiModelProperty(value = "生产阶别")
	private java.lang.String produceGrade;
	/**影响因素*/
	@Excel(name = "影响因素", width = 15)
	@ApiModelProperty(value = "影响因素")
	private java.lang.String affectFactor;
	/**机种配置id*/
	@ApiModelProperty(value = "机种配置id")
	private java.lang.String mechanismId;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
	@ApiModelProperty(value = "备用2")
	private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
}
