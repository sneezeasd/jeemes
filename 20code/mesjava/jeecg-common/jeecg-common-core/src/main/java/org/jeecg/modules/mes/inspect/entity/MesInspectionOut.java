package org.jeecg.modules.mes.inspect.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 出货检验报告
 * @Author: jeecg-boot
 * @Date:   2021-03-21
 * @Version: V1.0
 */
@Data
@TableName("mes_inspection_out")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_inspection_out对象", description="出货检验报告")
public class MesInspectionOut implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**客户名称*/
	@Excel(name = "客户名称", width = 15)
    @ApiModelProperty(value = "客户名称")
    private java.lang.String customerName;
	/**订单号*/
	@Excel(name = "订单号", width = 15)
    @ApiModelProperty(value = "订单号")
    private java.lang.String orderNum;
	/**产品型号*/
	@Excel(name = "产品型号", width = 15)
    @ApiModelProperty(value = "产品型号")
    private java.lang.String productNum;
	/**订单数量*/
	@Excel(name = "订单数量", width = 15)
    @ApiModelProperty(value = "订单数量")
    private java.lang.String orderCount;
	/**生产阶别*/
	@Excel(name = "生产阶别", width = 15)
    @ApiModelProperty(value = "生产阶别")
    private java.lang.String productLevel;
    /**产线*/
    @Excel(name = "产线", width = 15)
    @ApiModelProperty(value = "产线")
    private java.lang.String productLine;
	/**本批数量*/
	@Excel(name = "本批数量", width = 15)
    @ApiModelProperty(value = "本批数量")
    private java.lang.String thisCount;
	/**抽验数量*/
	@Excel(name = "抽验数量", width = 15)
    @ApiModelProperty(value = "抽验数量")
    private java.lang.String samplingCount;
	/**检验日期*/
	@Excel(name = "检验日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "检验日期")
    private java.util.Date inspectDay;
	/**检验依据*/
	@Excel(name = "检验依据", width = 15)
    @ApiModelProperty(value = "检验依据")
    private java.lang.String inspectBasis;
	/**抽样方案*/
	@Excel(name = "抽样方案", width = 15)
    @ApiModelProperty(value = "抽样方案")
    private java.lang.String samplingPlan;
	/**外观检验*/
	@Excel(name = "外观检验", width = 15)
    @ApiModelProperty(value = "外观检验")
    private java.lang.String inspectExterior;
	/**功能测试*/
	@Excel(name = "功能测试", width = 15)
    @ApiModelProperty(value = "功能测试")
    private java.lang.String inspectFunction;
	/**尺寸规格与包装*/
	@Excel(name = "尺寸规格与包装", width = 15)
    @ApiModelProperty(value = "尺寸规格与包装")
    private java.lang.String inspectSpec;
	/**备注说明*/
	@Excel(name = "备注说明", width = 15)
    @ApiModelProperty(value = "备注说明")
    private java.lang.String badDesc;
	/**不良品数量*/
	@Excel(name = "不良品数量", width = 15)
    @ApiModelProperty(value = "不良品数量")
    private java.lang.String badNum;
	/**不良率*/
	@Excel(name = "不良率", width = 15)
    @ApiModelProperty(value = "不良率")
    private java.lang.String badRate;
	/**检验结论*/
	@Excel(name = "检验结论", width = 15)
    @ApiModelProperty(value = "检验结论")
    private java.lang.String inspectVerdict;
	/**检验员*/
	@Excel(name = "检验员", width = 15)
    @ApiModelProperty(value = "检验员")
    private java.lang.String inspectMan;
	/**审核*/
	@Excel(name = "审核", width = 15)
    @ApiModelProperty(value = "审核")
    private java.lang.String inspectExamine;
	/**确认受理人*/
	@Excel(name = "确认受理人", width = 15)
    @ApiModelProperty(value = "确认受理人")
    private java.lang.String acceptMan;
	/**确认受理时间*/
	@Excel(name = "确认受理时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "确认受理时间")
    private java.util.Date acceptTime;
	/**原因分析及处理措施*/
	@Excel(name = "原因分析及处理措施", width = 15)
    @ApiModelProperty(value = "原因分析及处理措施")
    private java.lang.String causeAnalysis;
	/**效果确认及描述*/
	@Excel(name = "效果确认及描述", width = 15)
    @ApiModelProperty(value = "效果确认及描述")
    private java.lang.String effectDesc;
	/**确认人*/
	@Excel(name = "确认人", width = 15)
    @ApiModelProperty(value = "确认人")
    private java.lang.String confirmedBy;
	/**确认时间*/
	@Excel(name = "确认时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "确认时间")
    private java.util.Date confirmedTime;
    /**照片*/
    @Excel(name = "照片", width = 15)
    @ApiModelProperty(value = "照片")
    private java.lang.String imglist;

	@TableField(exist = false)
	private List<List<MesBodLog>> mesBodLogList;
}
