package org.jeecg.modules.mes.organize.vo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;

@Data
@ApiModel(value="MesFactorySitePage", description="组织—工厂库存点")
public class MesFactorySitePage {
    /**工厂编号*/
    @Excel(name = "工厂编号", width = 15)
    @ApiModelProperty(value = "工厂编号")
    private java.lang.String facCode;
    /**名称1*/
    @Excel(name = "工厂名称", width = 15)
    @ApiModelProperty(value = "工厂名称")
    private java.lang.String facNameone;

    /**编号*/
    @Excel(name = "库存地点编号", width = 15)
    @ApiModelProperty(value = "库存地点编号")
    private java.lang.String storeCode;
    /**名称*/
    @Excel(name = "库存地点名称", width = 15)
    @ApiModelProperty(value = "库存地点名称")
    private java.lang.String storeName;

}
