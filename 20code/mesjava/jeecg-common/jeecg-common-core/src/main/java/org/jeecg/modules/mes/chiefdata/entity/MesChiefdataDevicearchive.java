package org.jeecg.modules.mes.chiefdata.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 主数据—设备建档
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
@Data
@TableName("mes_chiefdata_devicearchive")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_chiefdata_devicearchive对象", description="主数据—设备建档")
public class MesChiefdataDevicearchive implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**设备图片*/
	@Excel(name = "设备图片", width = 15)
    @ApiModelProperty(value = "设备图片")
    private java.lang.String devicePicture;
	/**设备SN*/
	@Excel(name = "设备SN", width = 15)
    @ApiModelProperty(value = "设备SN")
    private java.lang.String deviceSn;
	/**设备名称*/
	@Excel(name = "设备名称", width = 15)
    @ApiModelProperty(value = "设备名称")
    private java.lang.String deviceName;
	/**设备规格*/
	@Excel(name = "设备规格", width = 15)
    @ApiModelProperty(value = "设备规格")
    private java.lang.String deviceGague;
	/**设备类型*/
	@Excel(name = "设备类型", width = 15)
    @ApiModelProperty(value = "设备类型")
    private java.lang.String deviceType;
	/**设备型号*/
	@Excel(name = "设备型号", width = 15)
    @ApiModelProperty(value = "设备型号")
    private java.lang.String deviceModel;
	/**有效标志*/
	@Excel(name = "有效标志", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "有效标志")
    private java.lang.String validToken;
	/**使用部门*/
	@Excel(name = "使用部门", width = 15, dictTable = "sys_depart", dicText = "depart_name", dicCode = "id")
	@Dict(dictTable = "sys_depart", dicText = "depart_name", dicCode = "id")
    @ApiModelProperty(value = "使用部门")
    private java.lang.String usedDepart;
	/**责任人*/
	@Excel(name = "责任人", width = 15)
    @ApiModelProperty(value = "责任人")
    private java.lang.String principal;
	/**责任人2*/
	@Excel(name = "责任人2", width = 15)
    @ApiModelProperty(value = "责任人2")
    private java.lang.String principalTwo;
	/**责任人3*/
	@Excel(name = "责任人3", width = 15)
    @ApiModelProperty(value = "责任人3")
    private java.lang.String principalThree;
	/**设备状态*/
	@Excel(name = "设备状态", width = 15, dicCode = "device_state")
	@Dict(dicCode = "device_state")
    @ApiModelProperty(value = "设备状态")
    private java.lang.String deviceState;
	/**出厂日期*/
	@Excel(name = "出厂日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "出厂日期")
    private java.util.Date factoryDate;
	/**购买日期*/
	@Excel(name = "购买日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "购买日期")
    private java.util.Date purchaseDate;
	/**供应商id*/
	@Excel(name = "供应商id", width = 15)
    @ApiModelProperty(value = "供应商id")
    private java.lang.String supplierId;
	/**供应商*/
	@Excel(name = "供应商", width = 15)
    @ApiModelProperty(value = "供应商")
    private java.lang.String supplier;
	/**设备位置*/
	@Excel(name = "设备位置", width = 15)
    @ApiModelProperty(value = "设备位置")
    private java.lang.String deviceLocation;
	/**点检周期(天)*/
	@Excel(name = "点检周期(天)", width = 15)
    @ApiModelProperty(value = "点检周期(天)")
    private java.lang.String checkCycle;
	/**设备文件*/
	@Excel(name = "设备文件", width = 15)
    @ApiModelProperty(value = "设备文件")
    private java.lang.String deviceFile;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String notes;
	/**表名编码*/
	@Excel(name = "表名编码", width = 15)
    @ApiModelProperty(value = "表名编码")
    private java.lang.String query1;
	/**阶别*/
	@Excel(name = "阶别", width = 15)
    @ApiModelProperty(value = "阶别")
    private java.lang.String query2;
	/**文件编码*/
	@Excel(name = "文件编码", width = 15)
    @ApiModelProperty(value = "文件编码")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
    /**
     * 备用5
     */
    @Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
    /**
     * 备用6
     */
    @Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
    /**
     * 最后保养时间
     */
    @Excel(name = "最后保养时间", width = 15)
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "最后保养时间")
    private java.util.Date lastUpkeepDate;
    /**
     * 保养周期（天）
     */
    @Excel(name = "保养周期（天）", width = 15)
    @ApiModelProperty(value = "保养周期（天）")
    private java.lang.String upkeepPeriod;
    /**
     * 提醒天数
     */
    @Excel(name = "提醒天数", width = 15)
    @ApiModelProperty(value = "提醒天数")
    private java.lang.String remindDays;

}
