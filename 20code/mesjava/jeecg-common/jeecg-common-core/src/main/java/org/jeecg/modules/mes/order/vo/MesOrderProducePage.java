package org.jeecg.modules.mes.order.vo;

import java.util.List;
import org.jeecg.modules.mes.order.entity.MesOrderProduce;
import org.jeecg.modules.mes.order.entity.MesProduceItem;
import org.jeecg.modules.mes.order.entity.MesProduceProcess;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 订单管理—生产订单
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
@Data
@ApiModel(value="mes_order_producePage对象", description="订单管理—生产订单")
public class MesOrderProducePage {

	/**id*/
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**订单编号*/
	@Excel(name = "订单编号", width = 15)
	@ApiModelProperty(value = "订单编号")
	private java.lang.String orderCode;
	/**订单名称*/
	@Excel(name = "订单名称", width = 15)
	@ApiModelProperty(value = "订单名称")
	private java.lang.String orderName;
	/**订单类型*/
	@Excel(name = "订单类型", width = 15)
	@ApiModelProperty(value = "订单类型")
	private java.lang.String orderType;
	/**物料id*/
	@Excel(name = "物料id", width = 15)
	@ApiModelProperty(value = "物料id")
	private java.lang.String materielId;
	/**物料编号*/
	@Excel(name = "物料编号", width = 15)
	@ApiModelProperty(value = "物料编号")
	private java.lang.String materielCode;
	/**物料名称*/
	@Excel(name = "物料名称", width = 15)
	@ApiModelProperty(value = "物料名称")
	private java.lang.String materielName;
	/**工厂id*/
	@Excel(name = "工厂id", width = 15)
	@ApiModelProperty(value = "工厂id")
	private java.lang.String factoryId;
	/**工厂编号*/
	@Excel(name = "工厂编号", width = 15)
	@ApiModelProperty(value = "工厂编号")
	private java.lang.String factoryCode;
	/**工厂名称*/
	@Excel(name = "工厂名称", width = 15)
	@ApiModelProperty(value = "工厂名称")
	private java.lang.String factoryName;
	/**状态*/
	@Excel(name = "状态", width = 15)
	@ApiModelProperty(value = "状态")
	private java.lang.String state;
	/**总数量*/
	@Excel(name = "总数量", width = 15)
	@ApiModelProperty(value = "总数量")
	private java.lang.String grossAccount;
	/**单位*/
	@Excel(name = "单位", width = 15)
	@ApiModelProperty(value = "单位")
	private java.lang.String unit;
	/**已交货*/
	@Excel(name = "已交货", width = 15)
	@ApiModelProperty(value = "已交货")
	private java.lang.String haveDelivery;
	/**基本结束日期*/
	@Excel(name = "基本结束日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "基本结束日期")
	private java.util.Date baseEnd;
	/**计划结束日期*/
	@Excel(name = "计划结束日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "计划结束日期")
	private java.util.Date planEnd;
	/**确认结束日期*/
	@Excel(name = "确认结束日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "确认结束日期")
	private java.util.Date confirmEnd;
	/**基本开始日期*/
	@Excel(name = "基本开始日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "基本开始日期")
	private java.util.Date baseStart;
	/**计划开始日期*/
	@Excel(name = "计划开始日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "计划开始日期")
	private java.util.Date planStart;
	/**确认开始日期*/
	@Excel(name = "确认开始日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "确认开始日期")
	private java.util.Date confirmStart;
	/**计划下达日期*/
	@Excel(name = "计划下达日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "计划下达日期")
	private java.util.Date planDownreach;
	/**确认下达日期*/
	@Excel(name = "确认下达日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "确认下达日期")
	private java.util.Date confirmDownreach;
	/**销售订单id*/
	@Excel(name = "销售订单id", width = 15)
	@ApiModelProperty(value = "销售订单id")
	private java.lang.String saleorderId;
	/**销售订单编号*/
	@Excel(name = "销售订单编号", width = 15)
	@ApiModelProperty(value = "销售订单编号")
	private java.lang.String saleorderCode;
	/**销售订单名称*/
	@Excel(name = "销售订单名称", width = 15)
	@ApiModelProperty(value = "销售订单名称")
	private java.lang.String saleorderName;
	/**MRP管理员id*/
	@Excel(name = "MRP管理员id", width = 15)
	@ApiModelProperty(value = "MRP管理员id")
	private java.lang.String mrpadminId;
	/**MRP管理员编号*/
	@Excel(name = "MRP管理员编号", width = 15)
	@ApiModelProperty(value = "MRP管理员编号")
	private java.lang.String mrpadminCode;
	/**MRP管理员名称*/
	@Excel(name = "MRP管理员名称", width = 15)
	@ApiModelProperty(value = "MRP管理员名称")
	private java.lang.String mrpadminName;
	/**生产管理员id*/
	@Excel(name = "生产管理员id", width = 15)
	@ApiModelProperty(value = "生产管理员id")
	private java.lang.String proadminId;
	/**生产管理员编号*/
	@Excel(name = "生产管理员编号", width = 15)
	@ApiModelProperty(value = "生产管理员编号")
	private java.lang.String proadminCode;
	/**生产管理员名称*/
	@Excel(name = "生产管理员名称", width = 15)
	@ApiModelProperty(value = "生产管理员名称")
	private java.lang.String proadminName;
	/**备注*/
	@Excel(name = "备注", width = 15)
	@ApiModelProperty(value = "备注")
	private java.lang.String notes;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
	@ApiModelProperty(value = "备用2")
	private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
	
	@ExcelCollection(name="订单管理—生产订单子表")
	@ApiModelProperty(value = "订单管理—生产订单子表")
	private List<MesProduceItem> mesProduceItemList;
	@ExcelCollection(name="订单管理—生产订单工序子表")
	@ApiModelProperty(value = "订单管理—生产订单工序子表")
	private List<MesProduceProcess> mesProduceProcessList;
	
}
