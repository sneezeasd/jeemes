package org.jeecg.modules.mes.produce.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 制造中心-工单信息
 * @Author: jeecg-boot
 * @Date:   2020-10-20
 * @Version: V1.0
 */
@Data
@TableName("mes_workbill_info")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_workbill_info对象", description="制造中心-工单信息")
public class MesWorkbillInfo implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**工单号*/
	@Excel(name = "工单号", width = 15)
    @ApiModelProperty(value = "工单号")
    private java.lang.String workbillCode;
	/**产线id*/
	@Excel(name = "产线id", width = 15)
    @ApiModelProperty(value = "产线id")
    private java.lang.String lineId;
	/**线别*/
	@Excel(name = "线别", width = 15)
    @ApiModelProperty(value = "线别")
    private java.lang.String lineType;
	/**客户*/
	@Excel(name = "客户", width = 15)
    @ApiModelProperty(value = "客户")
    private java.lang.String client;
	/**物料id*/
	@Excel(name = "物料id", width = 15)
    @ApiModelProperty(value = "物料id")
    private java.lang.String materielId;
	/**机种料号*/
	@Excel(name = "机种料号", width = 15)
    @ApiModelProperty(value = "机种料号")
    private java.lang.String mechanismCode;
	/**机种名称*/
	@Excel(name = "机种名称", width = 15)
    @ApiModelProperty(value = "机种名称")
    private java.lang.String mechanismName;
	/**规格*/
	@Excel(name = "规格", width = 15)
    @ApiModelProperty(value = "规格")
    private java.lang.String gague;
	/**机种版本号*/
	@Excel(name = "机种版本号", width = 15)
    @ApiModelProperty(value = "机种版本号")
    private java.lang.String mechanismEdition;
	/**工单类型*/
	@Excel(name = "工单类型", width = 15)
    @ApiModelProperty(value = "工单类型")
    private java.lang.String workbillType;
	/**生产阶别*/
    @Excel(name = "生产阶别", width = 15, dicCode = "produce_grade")
    @Dict(dicCode = "produce_grade")
    @ApiModelProperty(value = "生产阶别")
    private java.lang.String produceGrade;
	/**计划数量*/
	@Excel(name = "计划数量", width = 15)
    @ApiModelProperty(value = "计划数量")
    private java.lang.String plantNum;
	/**自动下达制令单*/
	@Excel(name = "自动下达制令单", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "自动下达制令单")
    private java.lang.String automaticCommandbill;
	/**首件检测数量*/
	@Excel(name = "首件检测数量", width = 15)
    @ApiModelProperty(value = "首件检测数量")
    private java.lang.String firstChecknum;
	/**预计开工时间*/
	@Excel(name = "预计开工时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "预计开工时间")
    private java.util.Date expectBegintime;
	/**实际开工时间*/
	@Excel(name = "实际开工时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "实际开工时间")
    private java.util.Date realBegintime;
	/**计划交货时间*/
	@Excel(name = "计划交货时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "计划交货时间")
    private java.util.Date plantDelivertime;
	/**预计完工时间*/
	@Excel(name = "预计完工时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "预计完工时间")
    private java.util.Date expectFinishtime;
	/**实际完工时间*/
	@Excel(name = "实际完工时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "实际完工时间")
    private java.util.Date realFinishtime;
	/**生产订单id*/
	@Excel(name = "生产订单id", width = 15)
    @ApiModelProperty(value = "生产订单id")
    private java.lang.String produceId;
	/**生产单号*/
	@Excel(name = "生产单号", width = 15)
    @ApiModelProperty(value = "生产单号")
    private java.lang.String produceCode;
	/**包装顺序管控*/
	@Excel(name = "包装顺序管控", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "包装顺序管控")
    private java.lang.String parcelSequence;
	/***/
	@Excel(name = "号段管控", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "号段管控")
    private java.lang.String paraControl;
	/**号段条码规则*/
	@Excel(name = "号段条码规则", width = 15)
    @ApiModelProperty(value = "号段条码规则")
    private java.lang.String paraBarcode;
	/**固定字符*/
	@Excel(name = "固定字符", width = 15)
    @ApiModelProperty(value = "固定字符")
    private java.lang.String fixedCharacter;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String notes;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
