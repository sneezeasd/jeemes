package org.jeecg.modules.mes.order.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: 订单管理—采购订单子表
 * @Author: jeecg-boot
 * @Date:   2020-11-11
 * @Version: V1.0
 */
@Data
@TableName("mes_purchase_item")
@ApiModel(value="mes_order_purchase对象", description="订单管理—采购订单")
public class MesPurchaseItem implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**物料id*/
	@Excel(name = "物料id", width = 15)
	@ApiModelProperty(value = "物料id")
	private java.lang.String materielId;
	/**商品编码*/
	@Excel(name = "商品编码", width = 15)
	@ApiModelProperty(value = "商品编码")
	private java.lang.String productCode;
	/**客户料号*/
	@Excel(name = "客户料号", width = 15)
	@ApiModelProperty(value = "客户料号")
	private java.lang.String clientCode;
	/**料号*/
	@Excel(name = "料号", width = 15)
	@ApiModelProperty(value = "料号")
	private java.lang.String materielCode;
	/**物料*/
	@Excel(name = "品名", width = 15)
	@ApiModelProperty(value = "品名")
	private java.lang.String materielName;
	/**规格*/
	@Excel(name = "规格", width = 15)
	@ApiModelProperty(value = "规格")
	private java.lang.String materielGauge;
	/**误差*/
	@Excel(name = "误差", width = 15)
	@ApiModelProperty(value = "误差")
	private java.lang.String rowNum;
	/**短文本*/
//	@Excel(name = "短文本", width = 15)
	@ApiModelProperty(value = "短文本")
	private java.lang.String shortText;
	/**伏数*/
	@Excel(name = "伏数", width = 15)
	@ApiModelProperty(value = "伏数")
	private java.lang.String query6;
	/**封装*/
	@Excel(name = "封装", width = 15)
	@ApiModelProperty(value = "封装")
	private java.lang.String query5;
	/**采购数量*/
	@Excel(name = "采购数量", width = 15)
	@ApiModelProperty(value = "采购数量")
	private java.lang.String purchaseNum;
	/**未收货数量*/
	@Excel(name = "未收货数量", width = 15)
	@ApiModelProperty(value = "未收货数量")
	private java.lang.String unreceiveNum;
	/**单位*/
	@Excel(name = "单位", width = 15)
	@ApiModelProperty(value = "单位")
	private java.lang.String orderUnit;
	/**最小包装数*/
	@Excel(name = "最小包装数", width = 15)
	@ApiModelProperty(value = "最小包装数")
	private java.lang.String query4;
	/**净价*/
	@Excel(name = "净价", width = 15)
	@ApiModelProperty(value = "净价")
	private java.lang.String grossPrice;
	/**货币*/
	@Excel(name = "货币", width = 15)
    @Dict(dicCode = "current_money")
	@ApiModelProperty(value = "货币")
	private java.lang.String currency;
	/**交货日期*/
	@Excel(name = "交货日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "交货日期")
	private java.util.Date deliveryDate;
	/**物料组*/
//	@Excel(name = "物料组", width = 15)
    @Dict(dicCode = "group_name",dicText = "group_name",dictTable = "mes_chiefdata_materielgroup")
	@ApiModelProperty(value = "物料组")
	private java.lang.String materielTeam;
	/**工厂id*/
//	@Excel(name = "工厂id", width = 15)
	@ApiModelProperty(value = "工厂id")
	private java.lang.String factoryId;
	/**工厂编号*/
	@Excel(name = "工厂编号", width = 15)
	@ApiModelProperty(value = "工厂编号")
	private java.lang.String factoryCode;
	/**工厂名称*/
	@Excel(name = "工厂名称", width = 15)
	@ApiModelProperty(value = "工厂名称")
	private java.lang.String factoryName;
	/**库存地点*/
	@Excel(name = "库存地点", width = 15)
	@ApiModelProperty(value = "库存地点")
	private java.lang.String storageSite;
	/**物料标记*/
	@Excel(name = "物料标记", width = 15)
	@ApiModelProperty(value = "物料标记")
	private java.lang.String imgExgFlag;
	/**项号*/
	@Excel(name = "项号", width = 15)
	@ApiModelProperty(value = "项号")
	private java.lang.String itemNo;
	/**批次号*/
	@Excel(name = "批次号", width = 15)
	@ApiModelProperty(value = "批次号")
	private java.lang.String sectionNo;
	/**公司编码*/
	@Excel(name = "公司编码", width = 15)
	@ApiModelProperty(value = "公司编码")
	private java.lang.String companyCode;
	/**板数*/
	@Excel(name = "板数", width = 15)
	@ApiModelProperty(value = "板数")
	private java.lang.String palletNum;
	/**件数*/
	@Excel(name = "件数", width = 15)
	@ApiModelProperty(value = "件数")
	private java.lang.String pcs;
	/**包装种类*/
	@Excel(name = "包装种类", width = 15)
	@ApiModelProperty(value = "包装种类")
	private java.lang.String wrap;
	/**箱号*/
	@Excel(name = "箱号", width = 15)
	@ApiModelProperty(value = "箱号")
	private java.lang.String containerNo;
	/**净重*/
	@Excel(name = "净重", width = 15)
	@ApiModelProperty(value = "净重")
	private java.lang.String netWeight;
	/**毛重*/
	@Excel(name = "毛重", width = 15)
	@ApiModelProperty(value = "毛重")
	private java.lang.String grossWeight;
	/**需求编号*/
	@Excel(name = "需求编号", width = 15)
	@ApiModelProperty(value = "需求编号")
	private java.lang.String requireCode;
	/**申请者*/
	@Excel(name = "申请者", width = 15)
	@ApiModelProperty(value = "申请者")
	private java.lang.String applicant;
	/**采购订单id*/
//	@ApiModelProperty(value = "采购订单id")
	private java.lang.String purchaseId;
	/**收货完成*/
//	@Excel(name = "收货完成", width = 15)
	@ApiModelProperty(value = "收货完成")
	private java.lang.String ifFinish;
}
