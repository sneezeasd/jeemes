package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesWorkbillInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 制造中心-工单信息
 * @Author: jeecg-boot
 * @Date:   2020-10-20
 * @Version: V1.0
 */
public interface IMesWorkbillInfoService extends IService<MesWorkbillInfo> {

}
