package org.jeecg.modules.mes.client;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.ServiceNameConstants;
import org.jeecg.modules.mes.storage.entity.MesErrorOperationRecord;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Component
@FeignClient(contextId = "WareHouseServiceClient", value = ServiceNameConstants.WAREHOUSE_SERVICE)
public interface WareHouseClient {


    @GetMapping("storage/mesErrorOperationRecord/add")
    public Result<?> add(@RequestBody MesErrorOperationRecord mesErrorOperationRecord);
}
