package org.jeecg.modules.mes.produce.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.produce.entity.FirstPieceDipLog;
import org.jeecg.modules.mes.produce.service.IFirstPieceDipLogService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: DIP首件确认表-检查内容
 * @Author: jeecg-boot
 * @Date:   2021-03-23
 * @Version: V1.0
 */
@Api(tags="DIP首件确认表-检查内容")
@RestController
@RequestMapping("/produce/firstPieceDipLog")
@Slf4j
public class FirstPieceDipLogController extends JeecgController<FirstPieceDipLog, IFirstPieceDipLogService> {
	@Autowired
	private IFirstPieceDipLogService firstPieceDipLogService;
	
	/**
	 * 分页列表查询
	 *
	 * @param firstPieceDipLog
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "DIP首件确认表-检查内容-分页列表查询")
	@ApiOperation(value="DIP首件确认表-检查内容-分页列表查询", notes="DIP首件确认表-检查内容-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(FirstPieceDipLog firstPieceDipLog,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<FirstPieceDipLog> queryWrapper = QueryGenerator.initQueryWrapper(firstPieceDipLog, req.getParameterMap());
		Page<FirstPieceDipLog> page = new Page<FirstPieceDipLog>(pageNo, pageSize);
		IPage<FirstPieceDipLog> pageList = firstPieceDipLogService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param firstPieceDipLog
	 * @return
	 */
	@AutoLog(value = "DIP首件确认表-检查内容-添加")
	@ApiOperation(value="DIP首件确认表-检查内容-添加", notes="DIP首件确认表-检查内容-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody FirstPieceDipLog firstPieceDipLog) {
		firstPieceDipLogService.save(firstPieceDipLog);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param firstPieceDipLog
	 * @return
	 */
	@AutoLog(value = "DIP首件确认表-检查内容-编辑")
	@ApiOperation(value="DIP首件确认表-检查内容-编辑", notes="DIP首件确认表-检查内容-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody FirstPieceDipLog firstPieceDipLog) {
		firstPieceDipLogService.updateById(firstPieceDipLog);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "DIP首件确认表-检查内容-通过id删除")
	@ApiOperation(value="DIP首件确认表-检查内容-通过id删除", notes="DIP首件确认表-检查内容-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		firstPieceDipLogService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "DIP首件确认表-检查内容-批量删除")
	@ApiOperation(value="DIP首件确认表-检查内容-批量删除", notes="DIP首件确认表-检查内容-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.firstPieceDipLogService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "DIP首件确认表-检查内容-通过id查询")
	@ApiOperation(value="DIP首件确认表-检查内容-通过id查询", notes="DIP首件确认表-检查内容-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		FirstPieceDipLog firstPieceDipLog = firstPieceDipLogService.getById(id);
		if(firstPieceDipLog==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(firstPieceDipLog);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param firstPieceDipLog
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, FirstPieceDipLog firstPieceDipLog) {
        return super.exportXls(request, firstPieceDipLog, FirstPieceDipLog.class, "DIP首件确认表-检查内容");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, FirstPieceDipLog.class);
    }

}
