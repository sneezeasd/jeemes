package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesOnlineMaterieltable;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @Description: 制造中心-在线料表-子表
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface IMesOnlineMaterieltableService extends IService<MesOnlineMaterieltable> {

    public boolean importExcel(HttpServletRequest request, HttpServletResponse response);

    public List<MesOnlineMaterieltable> selectByMainId(String mainId);
}
