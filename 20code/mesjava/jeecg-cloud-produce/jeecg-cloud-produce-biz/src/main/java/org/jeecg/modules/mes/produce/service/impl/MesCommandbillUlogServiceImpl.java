package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesCommandbillUlog;
import org.jeecg.modules.mes.produce.mapper.MesCommandbillUlogMapper;
import org.jeecg.modules.mes.produce.service.IMesCommandbillUlogService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 制令单上料记录
 * @Author: jeecg-boot
 * @Date: 2021-03-04
 * @Version: V1.0
 */
@Service
public class MesCommandbillUlogServiceImpl extends ServiceImpl<MesCommandbillUlogMapper, MesCommandbillUlog> implements IMesCommandbillUlogService {

}
