package org.jeecg.modules.mes.produce.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CacheConstant;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.mes.client.StockClient;
import org.jeecg.modules.mes.client.SystemClient;
import org.jeecg.modules.mes.produce.entity.MesCourseScan;
import org.jeecg.modules.mes.produce.service.IMesCourseScanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

/**
 * @Description: 订单管理-线程扫描
 * @Author: jeecg-boot
 * @Date:   2020-12-24
 * @Version: V1.0
 */
@Api(tags="订单管理-线程扫描")
@RestController
@RequestMapping("/produce/mesCourseScan")
@Slf4j
public class MesCourseScanController extends JeecgController<MesCourseScan, IMesCourseScanService> {
	 @Autowired
	 private IMesCourseScanService mesCourseScanService;


	 @Autowired
	 SystemClient systemClient;
	 @Autowired
	 StockClient stockClient;

	 @Autowired
	 private RedisUtil redisUtil;
	/**
	 * 分页列表查询
	 *
	 * @param mesCourseScan
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "订单管理-线程扫描-分页列表查询")
	@ApiOperation(value="订单管理-线程扫描-分页列表查询", notes="订单管理-线程扫描-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesCourseScan mesCourseScan,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesCourseScan> queryWrapper = QueryGenerator.initQueryWrapper(mesCourseScan, req.getParameterMap());
		Page<MesCourseScan> page = new Page<MesCourseScan>(pageNo, pageSize);
		IPage<MesCourseScan> pageList = mesCourseScanService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *   添加
	 *
	 * @param mesCourseScan
	 * @return
	 */
//	@AutoLog(value = "订单管理-线程扫描-添加")
	@ApiOperation(value="订单管理-线程扫描-添加", notes="订单管理-线程扫描-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesCourseScan mesCourseScan, HttpServletRequest request) {
		String accessToken = request.getHeader("X-Access-Token");
		redisUtil.expire(CommonConstant.PREFIX_USER_TOKEN + accessToken, JwtUtil.EXPIRE_TIME*5 / 1000);
		redisUtil.expire(CacheConstant.SYS_USERS_CACHE_JWT +":" +accessToken, JwtUtil.EXPIRE_TIME*5 / 1000);
		boolean mark=mesCourseScanService.mainAdd(mesCourseScan);
		if (mark) {
			return Result.ok("添加成功！");
		}else {
			return Result.error("添加失败！");

		}
	}
	/**
	 * 直接更新物料消耗数据和其他数据
	 * @param mesCourseScan
	 */
	@ApiOperation(value="订单管理-线程扫描-直接更新物料消耗数据和其他数据", notes="订单管理-线程扫描-直接更新物料消耗数据和其他数据")
	@PostMapping(value = "/getxh")
	public void getxh(@RequestBody MesCourseScan mesCourseScan){
		mesCourseScanService.getxh(mesCourseScan);
	}


	/**
	 *  编辑
	 *
	 * @param mesCourseScan
	 * @return
	 */
	@AutoLog(value = "订单管理-线程扫描-编辑")
	@ApiOperation(value="订单管理-线程扫描-编辑", notes="订单管理-线程扫描-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesCourseScan mesCourseScan) {
		mesCourseScanService.updateById(mesCourseScan);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "订单管理-线程扫描-通过id删除")
	@ApiOperation(value="订单管理-线程扫描-通过id删除", notes="订单管理-线程扫描-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesCourseScanService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "订单管理-线程扫描-批量删除")
	@ApiOperation(value="订单管理-线程扫描-批量删除", notes="订单管理-线程扫描-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesCourseScanService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "订单管理-线程扫描-通过id查询")
	@ApiOperation(value="订单管理-线程扫描-通过id查询", notes="订单管理-线程扫描-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesCourseScan mesCourseScan = mesCourseScanService.getById(id);
		if(mesCourseScan==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesCourseScan);
	}

	 /**
	  * 远程调用查询
	  * @param CommandbillCode
	  * @return
	  */
	 @GetMapping(value = "/queryByCommandbillCode")
	 public List<MesCourseScan> queryByCommandbillCode(@RequestParam(name="CommandbillCode",required=true) String CommandbillCode) {
		 QueryWrapper<MesCourseScan> mesCourseScanQueryWrapper = new QueryWrapper<>();
		 mesCourseScanQueryWrapper.eq("command_code",CommandbillCode);
		 mesCourseScanQueryWrapper.isNull("query4");
		 List<MesCourseScan> list = mesCourseScanService.list(mesCourseScanQueryWrapper);
		 return list;
	 }

	 /**
	  * 远程调用修改
	  * @param mesCourseScan
	  * @return
	  */
	 @PutMapping(value = "/editItem")
	 public boolean editItem(@RequestBody MesCourseScan mesCourseScan) {
		 mesCourseScanService.updateById(mesCourseScan);
		 return true;
	 }
    /**
    * 导出excel
    *
    * @param request
    * @param mesCourseScan
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesCourseScan mesCourseScan) {
        return super.exportXls(request, mesCourseScan, MesCourseScan.class, "订单管理-线程扫描");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesCourseScan.class);
    }

}
