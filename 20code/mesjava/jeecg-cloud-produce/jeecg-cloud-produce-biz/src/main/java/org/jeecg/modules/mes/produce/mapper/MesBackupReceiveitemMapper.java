package org.jeecg.modules.mes.produce.mapper;

import java.util.List;
import org.jeecg.modules.mes.produce.entity.MesBackupReceiveitem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 备品领用-明细信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface MesBackupReceiveitemMapper extends BaseMapper<MesBackupReceiveitem> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesBackupReceiveitem> selectByMainId(@Param("mainId") String mainId);
}
