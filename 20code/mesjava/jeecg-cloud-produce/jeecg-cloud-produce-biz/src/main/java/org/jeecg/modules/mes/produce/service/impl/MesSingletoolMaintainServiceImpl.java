package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesSingletoolMaintain;
import org.jeecg.modules.mes.produce.mapper.MesSingletoolMaintainMapper;
import org.jeecg.modules.mes.produce.service.IMesSingletoolMaintainService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 制造中心-单个制具维保
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesSingletoolMaintainServiceImpl extends ServiceImpl<MesSingletoolMaintainMapper, MesSingletoolMaintain> implements IMesSingletoolMaintainService {

}
