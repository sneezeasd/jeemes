package org.jeecg.modules.mes.produce.mapper;

import java.util.List;
import org.jeecg.modules.mes.produce.entity.MesOnlineMaterieltable;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 制造中心-在线料表-子表
 * @Author: jeecg-boot
 * @Date:   2021-05-17
 * @Version: V1.0
 */
public interface MesOnlineMaterieltableMapper extends BaseMapper<MesOnlineMaterieltable> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesOnlineMaterieltable> selectByMainId(@Param("mainId") String mainId);

}
