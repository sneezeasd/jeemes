package org.jeecg.modules.mes.produce.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.produce.entity.FirstPieceSmtLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: SMT首件确认表-检查内容
 * @Author: jeecg-boot
 * @Date:   2021-03-22
 * @Version: V1.0
 */
public interface FirstPieceSmtLogMapper extends BaseMapper<FirstPieceSmtLog> {

}
