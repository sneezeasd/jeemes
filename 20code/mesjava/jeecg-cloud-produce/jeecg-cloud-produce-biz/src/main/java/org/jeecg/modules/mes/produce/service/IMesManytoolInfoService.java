package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesManytoolInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 多个制具维保-制具信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface IMesManytoolInfoService extends IService<MesManytoolInfo> {

	public List<MesManytoolInfo> selectByMainId(String mainId);
}
