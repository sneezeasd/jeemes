package org.jeecg.modules.mes.produce.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.produce.entity.MesProductionCapacityReport;
import org.jeecg.modules.mes.produce.service.IMesProductionCapacityReportService;

import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: NEW生产产能报表
 * @Author: jeecg-boot
 * @Date:   2021-05-25
 * @Version: V1.0
 */
@Api(tags="NEW生产产能报表")
@RestController
@RequestMapping("/produce/mesProductionCapacityReport")
@Slf4j
public class MesProductionCapacityReportController extends JeecgController<MesProductionCapacityReport, IMesProductionCapacityReportService> {
	@Autowired
	private IMesProductionCapacityReportService mesProductionCapacityReportService;

	 /**
	  * 分页列表查询
	  *
	  * @param mesProductionCapacityReport
	  * @param pageNo
	  * @param pageSize
	  * @param
	  * @return
	  */
	 @AutoLog(value = "生产产能报表-分页列表查询")
	 @ApiOperation(value = "生产产能报表-分页列表查询", notes = "生产产能报表-分页列表查询")
	 @GetMapping(value = "/list")
	 public Result<?> queryPageList(MesProductionCapacityReport mesProductionCapacityReport,
									@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
									@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
		 return mesProductionCapacityReportService.mesProductionCapacityReport(mesProductionCapacityReport, pageNo, pageSize);
	 }
	
	/**
	 *   添加
	 *
	 * @param mesProductionCapacityReport
	 * @return
	 */
	@AutoLog(value = "NEW生产产能报表-添加")
	@ApiOperation(value="NEW生产产能报表-添加", notes="NEW生产产能报表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesProductionCapacityReport mesProductionCapacityReport) {
		mesProductionCapacityReportService.save(mesProductionCapacityReport);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesProductionCapacityReport
	 * @return
	 */
	@AutoLog(value = "NEW生产产能报表-编辑")
	@ApiOperation(value="NEW生产产能报表-编辑", notes="NEW生产产能报表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesProductionCapacityReport mesProductionCapacityReport) {
		mesProductionCapacityReportService.updateById(mesProductionCapacityReport);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "NEW生产产能报表-通过id删除")
	@ApiOperation(value="NEW生产产能报表-通过id删除", notes="NEW生产产能报表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesProductionCapacityReportService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "NEW生产产能报表-批量删除")
	@ApiOperation(value="NEW生产产能报表-批量删除", notes="NEW生产产能报表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesProductionCapacityReportService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "NEW生产产能报表-通过id查询")
	@ApiOperation(value="NEW生产产能报表-通过id查询", notes="NEW生产产能报表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesProductionCapacityReport mesProductionCapacityReport = mesProductionCapacityReportService.getById(id);
		if(mesProductionCapacityReport==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesProductionCapacityReport);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesProductionCapacityReport
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesProductionCapacityReport mesProductionCapacityReport) {
        return super.exportXls(request, mesProductionCapacityReport, MesProductionCapacityReport.class, "NEW生产产能报表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesProductionCapacityReport.class);
    }

}
