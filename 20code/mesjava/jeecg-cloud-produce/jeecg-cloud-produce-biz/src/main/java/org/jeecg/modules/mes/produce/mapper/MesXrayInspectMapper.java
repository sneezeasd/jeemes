package org.jeecg.modules.mes.produce.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.produce.entity.MesXrayInspect;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 制造中心-X-Ray检测
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface MesXrayInspectMapper extends BaseMapper<MesXrayInspect> {

}
