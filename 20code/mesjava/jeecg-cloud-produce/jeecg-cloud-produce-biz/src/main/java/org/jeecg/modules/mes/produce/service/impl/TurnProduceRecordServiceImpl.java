package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.TurnProduceRecord;
import org.jeecg.modules.mes.produce.mapper.TurnProduceRecordMapper;
import org.jeecg.modules.mes.produce.service.ITurnProduceRecordService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 转产记录表
 * @Author: jeecg-boot
 * @Date:   2021-04-21
 * @Version: V1.0
 */
@Service
public class TurnProduceRecordServiceImpl extends ServiceImpl<TurnProduceRecordMapper, TurnProduceRecord> implements ITurnProduceRecordService {

}
