package org.jeecg.modules.mes.produce.controller;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.produce.service.IMesFileCollectionAoiService;
import org.jeecg.modules.mes.produce.service.IMesFileCollectionSpiService;
import org.jeecg.modules.mes.produce.service.IMesFileCollectionTpjService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @Package org.jeecg.modules.mes.filecollection
 * @date 2021/4/16 14:05
 * @description
 */
@RestController
@RequestMapping("/machineFile/fileCollection")
public class MesFileCollectionController {

    @Value("")
    private String fileSavePath;
    @Autowired
    private IMesFileCollectionSpiService mesFileCollectionSpiService ;
    @Autowired
    private IMesFileCollectionTpjService mesFileCollectionTpjService;
    @Autowired
    private IMesFileCollectionAoiService mesFileCollectionAoiService;

    /**
     * 接收spi的txt文件
     * @param request
     * @param line
     * @return
     */
    @PostMapping("/receiveSpiFile")
    public Result<?> receiveSpiFile(HttpServletRequest request,String line) {
        mesFileCollectionSpiService.receiveSpiFile(request,line);

        return Result.ok("上传成功");
    }

    /**
     * 接收贴片机文件
     * @param request
     * @param line
     * @return
     */
    @PostMapping("/receiveTpjFile")
    public Result<?> receiveTpjFile(HttpServletRequest request,String line){
        mesFileCollectionTpjService.receiveTpjFile(request,line);
        return Result.ok("上传成功");
    }

    /**
     * 接收aoi文件
     * @param request
     * @param line
     * @return
     */
    @PostMapping("/receiveAoiFile")
    public Result<?> receiveAoiFile(HttpServletRequest request,String line){
        mesFileCollectionAoiService.receiveAoiFile(request,line);
        return Result.ok("上传成功");
    }
}
