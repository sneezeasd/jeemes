package org.jeecg.modules.mes.produce.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionTpjFeed;

/**
 * @Description: mes_file_collection_tpj_feed
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
public interface IMesFileCollectionTpjFeedService extends IService<MesFileCollectionTpjFeed> {

}
