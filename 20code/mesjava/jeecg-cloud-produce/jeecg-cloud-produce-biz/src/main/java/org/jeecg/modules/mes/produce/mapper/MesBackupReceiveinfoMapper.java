package org.jeecg.modules.mes.produce.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.produce.entity.MesBackupReceiveinfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 备品领用-基本信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface MesBackupReceiveinfoMapper extends BaseMapper<MesBackupReceiveinfo> {

}
