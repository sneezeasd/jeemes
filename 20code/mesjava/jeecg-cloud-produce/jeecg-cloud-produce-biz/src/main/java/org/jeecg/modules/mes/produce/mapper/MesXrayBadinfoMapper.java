package org.jeecg.modules.mes.produce.mapper;

import java.util.List;
import org.jeecg.modules.mes.produce.entity.MesXrayBadinfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: X-Ray检测-不良信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface MesXrayBadinfoMapper extends BaseMapper<MesXrayBadinfo> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesXrayBadinfo> selectByMainId(@Param("mainId") String mainId);
}
