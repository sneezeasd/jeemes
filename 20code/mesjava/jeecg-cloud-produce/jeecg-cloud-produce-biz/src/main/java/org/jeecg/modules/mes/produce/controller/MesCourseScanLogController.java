package org.jeecg.modules.mes.produce.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.produce.entity.MesCourseScanLog;
import org.jeecg.modules.mes.produce.service.IMesCourseScanLogService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 物料追踪记录
 * @Author: jeecg-boot
 * @Date:   2021-03-16
 * @Version: V1.0
 */
@Api(tags="物料追踪记录")
@RestController
@RequestMapping("/produce/mesCourseScanLog")
@Slf4j
public class MesCourseScanLogController extends JeecgController<MesCourseScanLog, IMesCourseScanLogService> {
	@Autowired
	private IMesCourseScanLogService mesCourseScanLogService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesCourseScanLog
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "物料追踪记录-分页列表查询")
	@ApiOperation(value="物料追踪记录-分页列表查询", notes="物料追踪记录-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesCourseScanLog mesCourseScanLog,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesCourseScanLog> queryWrapper = QueryGenerator.initQueryWrapper(mesCourseScanLog, req.getParameterMap());
		Page<MesCourseScanLog> page = new Page<MesCourseScanLog>(pageNo, pageSize);
		IPage<MesCourseScanLog> pageList = mesCourseScanLogService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesCourseScanLog
	 * @return
	 */
	@AutoLog(value = "物料追踪记录-添加")
	@ApiOperation(value="物料追踪记录-添加", notes="物料追踪记录-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesCourseScanLog mesCourseScanLog) {
		mesCourseScanLogService.save(mesCourseScanLog);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesCourseScanLog
	 * @return
	 */
	@AutoLog(value = "物料追踪记录-编辑")
	@ApiOperation(value="物料追踪记录-编辑", notes="物料追踪记录-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesCourseScanLog mesCourseScanLog) {
		mesCourseScanLogService.updateById(mesCourseScanLog);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "物料追踪记录-通过id删除")
	@ApiOperation(value="物料追踪记录-通过id删除", notes="物料追踪记录-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesCourseScanLogService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "物料追踪记录-批量删除")
	@ApiOperation(value="物料追踪记录-批量删除", notes="物料追踪记录-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesCourseScanLogService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "物料追踪记录-通过id查询")
	@ApiOperation(value="物料追踪记录-通过id查询", notes="物料追踪记录-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesCourseScanLog mesCourseScanLog = mesCourseScanLogService.getById(id);
		if(mesCourseScanLog==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesCourseScanLog);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesCourseScanLog
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesCourseScanLog mesCourseScanLog) {
        return super.exportXls(request, mesCourseScanLog, MesCourseScanLog.class, "物料追踪记录");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesCourseScanLog.class);
    }

}
