package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesSingletoolMaintain;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 制造中心-单个制具维保
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface IMesSingletoolMaintainService extends IService<MesSingletoolMaintain> {

}
