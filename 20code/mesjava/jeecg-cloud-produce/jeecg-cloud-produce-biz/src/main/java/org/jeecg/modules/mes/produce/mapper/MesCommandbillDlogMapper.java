package org.jeecg.modules.mes.produce.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.produce.entity.MesCommandbillDlog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 制令单上料记录
 * @Author: jeecg-boot
 * @Date: 2021-03-04
 * @Version: V1.0
 */
public interface MesCommandbillDlogMapper extends BaseMapper<MesCommandbillDlog> {

}
