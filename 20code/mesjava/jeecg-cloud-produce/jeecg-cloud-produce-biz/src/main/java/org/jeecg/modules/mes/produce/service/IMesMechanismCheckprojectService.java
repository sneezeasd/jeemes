package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesMechanismCheckproject;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 机种项目配置-检查项目
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface IMesMechanismCheckprojectService extends IService<MesMechanismCheckproject> {

	public List<MesMechanismCheckproject> selectByMainId(String mainId);
}
