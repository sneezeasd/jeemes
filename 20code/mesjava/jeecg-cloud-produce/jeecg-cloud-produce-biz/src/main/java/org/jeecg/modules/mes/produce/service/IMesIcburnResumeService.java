package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesIcburnResume;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 制造中心-IC烧录
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface IMesIcburnResumeService extends IService<MesIcburnResume> {

}
