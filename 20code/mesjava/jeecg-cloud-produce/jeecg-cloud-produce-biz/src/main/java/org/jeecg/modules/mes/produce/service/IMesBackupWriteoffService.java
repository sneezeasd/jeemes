package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesBackupWriteoff;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 制造中心-备品报废
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface IMesBackupWriteoffService extends IService<MesBackupWriteoff> {

}
