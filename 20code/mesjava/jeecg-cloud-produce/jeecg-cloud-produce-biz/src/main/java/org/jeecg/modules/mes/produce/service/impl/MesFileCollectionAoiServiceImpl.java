package org.jeecg.modules.mes.produce.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.common.util.ExcelUtil;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionAoi;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionAoiDetail;
import org.jeecg.modules.mes.produce.mapper.MesFileCollectionAoiMapper;
import org.jeecg.modules.mes.produce.service.IMesFileCollectionAoiDetailService;
import org.jeecg.modules.mes.produce.service.IMesFileCollectionAoiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Description: mes_file_collection_aoi
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
@Service
public class MesFileCollectionAoiServiceImpl extends ServiceImpl<MesFileCollectionAoiMapper, MesFileCollectionAoi> implements IMesFileCollectionAoiService {

    @Autowired
    private IMesFileCollectionAoiDetailService mesFileCollectionAoiDetailService;
    @Value(value = "${jeecg.path.upload}")
    private String uploadpath;

    /**
     * 接收aoi机器文件
     * @param request request
     * @param line 产线
     */
    @Override
    public void receiveAoiFile(HttpServletRequest request, String line) {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
        for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
            MultipartFile file = entity.getValue();
            try {
                readAoiFile(file,line);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Transactional
    private void readAoiFile(MultipartFile file,String line) throws IOException {
        List<List<String>> list =   ExcelUtil.publicReadAoiExcel(file,0,0,14,uploadpath);
        if (list == null || list.size() == 0) {
            return;
        }
        MesFileCollectionAoi mesFileCollectionAoi = new MesFileCollectionAoi();
        mesFileCollectionAoi.setLine(line);
        List<MesFileCollectionAoiDetail> mesFileCollectionAoiDetailList =
                new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            List<String> dataList = list.get(i);
            if (i == 1) {
                for (int j = 0; j < dataList.size(); j++) {
                    String cellText = dataList.get(j);
                    if (j == 0 ) {
                        mesFileCollectionAoi.setPcbNo(cellText);
                    }
                    if (j == 1 ) {
                        mesFileCollectionAoi.setProduceName(cellText);
                    }
                    if (j == 2 ) {
                        mesFileCollectionAoi.setTestTime(cellText);
                    }
                    if (j == 3 ) {
                        mesFileCollectionAoi.setTbFace(cellText);
                    }
                    if (j == 4 ) {
                        mesFileCollectionAoi.setTestResult(cellText);
                    }
                    if (j == 5 ) {
                        mesFileCollectionAoi.setMachineNo(cellText);
                    }
                    if (j == 6 ) {
                        mesFileCollectionAoi.setMachineCode(cellText);
                    }
                    if (j == 7 ) {
                        mesFileCollectionAoi.setPcbNgNumber(cellText);
                    }
                    if (j == 8 ) {
                        mesFileCollectionAoi.setErrNgNumber(cellText);
                    }
                    if (j == 9 ) {
                        mesFileCollectionAoi.setOptPerson(cellText);
                    }
                    if (j == 10 ) {
                        mesFileCollectionAoi.setOrderClass(cellText);
                    }
                    if (j == 11 ) {
                        mesFileCollectionAoi.setLineType(cellText);
                    }
                    if (j == 12 ) {
                        mesFileCollectionAoi.setCompoNumber(cellText);
                    }
                    if (j == 13 ) {
                        mesFileCollectionAoi.setTestNumber(cellText);
                    }
                }
                this.save(mesFileCollectionAoi);
            }
            if (i >= 4) {
                //读取详细信息
                MesFileCollectionAoiDetail mesFileCollectionAoiDetail = new MesFileCollectionAoiDetail();
                mesFileCollectionAoiDetail.setMainId(mesFileCollectionAoi.getId());
                for (int j = 0; j < dataList.size(); j++) {
                    String cell = dataList.get(j);
                    if (j == 0) {
                        mesFileCollectionAoiDetail.setPcbNo(cell);
                    }
                    if (j == 1) {
                        mesFileCollectionAoiDetail.setJoinNo(cell);
                    }
                    if (j == 2) {
                        mesFileCollectionAoiDetail.setCompoPostion(cell);
                    }
                    if (j == 3) {
                        mesFileCollectionAoiDetail.setAngle(cell);
                    }
                    if (j == 4) {
                        mesFileCollectionAoiDetail.setPointX(cell);
                    }
                    if (j == 5) {
                        mesFileCollectionAoiDetail.setPointY(cell);
                    }
                    if (j == 6) {
                        mesFileCollectionAoiDetail.setNgName(cell);
                    }
                    if (j == 7) {
                        mesFileCollectionAoiDetail.setMaterNo(cell);
                    }
                    if (j == 8) {
                        mesFileCollectionAoiDetail.setCode(cell);
                    }
                    if (j == 9) {
                        mesFileCollectionAoiDetail.setPic(cell);
                    }
                }
                mesFileCollectionAoiDetailList.add(mesFileCollectionAoiDetail);
            }
        }

        mesFileCollectionAoiDetailService.saveBatch(mesFileCollectionAoiDetailList);


    }
}
