package org.jeecg.modules.mes.produce.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mes.produce.entity.MesCourseScan;

/**
 * @Description: 订单管理-线程扫描
 * @Author: jeecg-boot
 * @Date:   2020-12-24
 * @Version: V1.0
 */
public interface IMesCourseScanService extends IService<MesCourseScan> {

    boolean mainAdd(MesCourseScan scan);

    void getxh(MesCourseScan mesCourseScan);
}
