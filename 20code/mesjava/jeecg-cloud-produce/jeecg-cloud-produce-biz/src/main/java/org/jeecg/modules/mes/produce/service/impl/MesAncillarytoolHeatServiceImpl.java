package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesAncillarytoolHeat;
import org.jeecg.modules.mes.produce.mapper.MesAncillarytoolHeatMapper;
import org.jeecg.modules.mes.produce.service.IMesAncillarytoolHeatService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 制造中心-辅料回温
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
@Service
public class MesAncillarytoolHeatServiceImpl extends ServiceImpl<MesAncillarytoolHeatMapper, MesAncillarytoolHeat> implements IMesAncillarytoolHeatService {

}
