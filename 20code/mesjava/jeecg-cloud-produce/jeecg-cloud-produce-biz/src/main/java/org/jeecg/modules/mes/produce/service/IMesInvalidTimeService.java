package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesInvalidTime;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 制造中心-无效时间
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface IMesInvalidTimeService extends IService<MesInvalidTime> {

}
