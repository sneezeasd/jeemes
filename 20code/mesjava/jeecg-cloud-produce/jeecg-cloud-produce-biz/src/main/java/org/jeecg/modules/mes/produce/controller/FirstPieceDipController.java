package org.jeecg.modules.mes.produce.controller;

import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.produce.entity.FirstPieceDip;
import org.jeecg.modules.mes.produce.entity.FirstPieceDipLog;
import org.jeecg.modules.mes.produce.service.IFirstPieceDipLogService;
import org.jeecg.modules.mes.produce.service.IFirstPieceDipService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: DIP首件确认表
 * @Author: jeecg-boot
 * @Date:   2021-03-22
 * @Version: V1.0
 */
@Api(tags="DIP首件确认表")
@RestController
@RequestMapping("/produce/firstPieceDip")
@Slf4j
public class FirstPieceDipController extends JeecgController<FirstPieceDip, IFirstPieceDipService> {
	@Autowired
	private IFirstPieceDipService firstPieceDipService;
	@Autowired
	private IFirstPieceDipLogService firstPieceDipLogService;
	
	/**
	 * 分页列表查询
	 *
	 * @param firstPieceDip
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "DIP首件确认表-分页列表查询")
	@ApiOperation(value="DIP首件确认表-分页列表查询", notes="DIP首件确认表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(FirstPieceDip firstPieceDip,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<FirstPieceDip> queryWrapper = QueryGenerator.initQueryWrapper(firstPieceDip, req.getParameterMap());
		Page<FirstPieceDip> page = new Page<FirstPieceDip>(pageNo, pageSize);
		IPage<FirstPieceDip> pageList = firstPieceDipService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param firstPieceDip
	 * @return
	 */
	@AutoLog(value = "DIP首件确认表-添加")
	@ApiOperation(value="DIP首件确认表-添加", notes="DIP首件确认表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody FirstPieceDip firstPieceDip) {
		firstPieceDipService.saveMain(firstPieceDip);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param firstPieceDip
	 * @return
	 */
	@AutoLog(value = "DIP首件确认表-编辑")
	@ApiOperation(value="DIP首件确认表-编辑", notes="DIP首件确认表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody FirstPieceDip firstPieceDip) {
		firstPieceDipService.updateById(firstPieceDip);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "DIP首件确认表-通过id删除")
	@ApiOperation(value="DIP首件确认表-通过id删除", notes="DIP首件确认表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		firstPieceDipService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "DIP首件确认表-批量删除")
	@ApiOperation(value="DIP首件确认表-批量删除", notes="DIP首件确认表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.firstPieceDipService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "DIP首件确认表-通过id查询")
	@ApiOperation(value="DIP首件确认表-通过id查询", notes="DIP首件确认表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		FirstPieceDip firstPieceDip = firstPieceDipService.getById(id);
		if(firstPieceDip==null) {
			return Result.error("未找到对应数据");
		}else{
			QueryWrapper<FirstPieceDipLog> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("first_id",firstPieceDip.getId());
			List<FirstPieceDipLog> list = firstPieceDipLogService.list(queryWrapper);
			firstPieceDip.setFirstPieceDipLogList(list);
		}
		return Result.ok(firstPieceDip);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param firstPieceDip
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, FirstPieceDip firstPieceDip) {
        return super.exportXls(request, firstPieceDip, FirstPieceDip.class, "DIP首件确认表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, FirstPieceDip.class);
    }

}
