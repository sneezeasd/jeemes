package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesMechanismCheckproject;
import org.jeecg.modules.mes.produce.mapper.MesMechanismCheckprojectMapper;
import org.jeecg.modules.mes.produce.service.IMesMechanismCheckprojectService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 机种项目配置-检查项目
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesMechanismCheckprojectServiceImpl extends ServiceImpl<MesMechanismCheckprojectMapper, MesMechanismCheckproject> implements IMesMechanismCheckprojectService {
	
	@Autowired
	private MesMechanismCheckprojectMapper mesMechanismCheckprojectMapper;
	
	@Override
	public List<MesMechanismCheckproject> selectByMainId(String mainId) {
		return mesMechanismCheckprojectMapper.selectByMainId(mainId);
	}
}
