package org.jeecg.modules.mes.produce.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionAoiDetail;

/**
 * @Description: mes_file_collection_aoi_detail
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
public interface MesFileCollectionAoiDetailMapper extends BaseMapper<MesFileCollectionAoiDetail> {

}
