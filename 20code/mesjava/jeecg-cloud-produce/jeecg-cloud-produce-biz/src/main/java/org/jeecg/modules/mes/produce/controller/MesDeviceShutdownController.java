package org.jeecg.modules.mes.produce.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.produce.entity.MesDeviceShutdown;
import org.jeecg.modules.mes.produce.service.IMesDeviceShutdownService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 制造中心-设备停机
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Api(tags="制造中心-设备停机")
@RestController
@RequestMapping("/produce/mesDeviceShutdown")
@Slf4j
public class MesDeviceShutdownController extends JeecgController<MesDeviceShutdown, IMesDeviceShutdownService> {
	@Autowired
	private IMesDeviceShutdownService mesDeviceShutdownService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesDeviceShutdown
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "制造中心-设备停机-分页列表查询")
	@ApiOperation(value="制造中心-设备停机-分页列表查询", notes="制造中心-设备停机-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesDeviceShutdown mesDeviceShutdown,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesDeviceShutdown> queryWrapper = QueryGenerator.initQueryWrapper(mesDeviceShutdown, req.getParameterMap());
		Page<MesDeviceShutdown> page = new Page<MesDeviceShutdown>(pageNo, pageSize);
		IPage<MesDeviceShutdown> pageList = mesDeviceShutdownService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesDeviceShutdown
	 * @return
	 */
	@AutoLog(value = "制造中心-设备停机-添加")
	@ApiOperation(value="制造中心-设备停机-添加", notes="制造中心-设备停机-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesDeviceShutdown mesDeviceShutdown) {
		mesDeviceShutdownService.save(mesDeviceShutdown);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesDeviceShutdown
	 * @return
	 */
	@AutoLog(value = "制造中心-设备停机-编辑")
	@ApiOperation(value="制造中心-设备停机-编辑", notes="制造中心-设备停机-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesDeviceShutdown mesDeviceShutdown) {
		mesDeviceShutdownService.updateById(mesDeviceShutdown);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "制造中心-设备停机-通过id删除")
	@ApiOperation(value="制造中心-设备停机-通过id删除", notes="制造中心-设备停机-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesDeviceShutdownService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "制造中心-设备停机-批量删除")
	@ApiOperation(value="制造中心-设备停机-批量删除", notes="制造中心-设备停机-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesDeviceShutdownService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "制造中心-设备停机-通过id查询")
	@ApiOperation(value="制造中心-设备停机-通过id查询", notes="制造中心-设备停机-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesDeviceShutdown mesDeviceShutdown = mesDeviceShutdownService.getById(id);
		if(mesDeviceShutdown==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesDeviceShutdown);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesDeviceShutdown
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesDeviceShutdown mesDeviceShutdown) {
        return super.exportXls(request, mesDeviceShutdown, MesDeviceShutdown.class, "制造中心-设备停机");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesDeviceShutdown.class);
    }

}
