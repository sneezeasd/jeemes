package org.jeecg.modules.mes.machineFile.service.impl;


import org.jeecg.modules.mes.machineFile.mapper.MesFileCollectionSpiCopyMapper;
import org.jeecg.modules.mes.machineFile.service.IMesFileCollectionSpiCopyService;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionSpiCopy;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: spi文件采集
 * @Author: jeecg-boot
 * @Date:   2021-05-31
 * @Version: V1.0
 */
@Service
public class MesFileCollectionSpiCopyServiceImpl extends ServiceImpl<MesFileCollectionSpiCopyMapper, MesFileCollectionSpiCopy> implements IMesFileCollectionSpiCopyService {

}
