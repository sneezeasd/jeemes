package org.jeecg.modules.mes.client;

import org.jeecg.common.constant.ServiceNameConstants;
import org.jeecg.modules.mes.produce.entity.MesCourseScan;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Component
@FeignClient(contextId = "ProduceServiceClient", value = ServiceNameConstants.PRODUCE_SERVICE)
public interface ProduceClient {


    /**
     * 直接更新物料消耗数据和其他数据
     * @param mesCourseScan
     */
    @PostMapping("produce/mesCourseScan/getxh")
    public void getxh(@RequestBody MesCourseScan mesCourseScan);

}
