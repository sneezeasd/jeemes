package org.jeecg.modules.mes.storage.service;

import org.jeecg.modules.mes.storage.entity.MesStorageShelftask;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 仓库管理—上架/下架任务
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface IMesStorageShelftaskService extends IService<MesStorageShelftask> {

}
