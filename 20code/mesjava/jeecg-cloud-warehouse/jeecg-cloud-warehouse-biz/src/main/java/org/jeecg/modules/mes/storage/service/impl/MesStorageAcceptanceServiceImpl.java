package org.jeecg.modules.mes.storage.service.impl;

import org.jeecg.modules.mes.storage.entity.MesStorageAcceptance;
import org.jeecg.modules.mes.storage.entity.MesAcceptanceItem;
import org.jeecg.modules.mes.storage.entity.MesAcceptanceSite;
import org.jeecg.modules.mes.storage.mapper.MesAcceptanceItemMapper;
import org.jeecg.modules.mes.storage.mapper.MesAcceptanceSiteMapper;
import org.jeecg.modules.mes.storage.mapper.MesStorageAcceptanceMapper;
import org.jeecg.modules.mes.storage.service.IMesStorageAcceptanceService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 仓库管理—验收入库
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesStorageAcceptanceServiceImpl extends ServiceImpl<MesStorageAcceptanceMapper, MesStorageAcceptance> implements IMesStorageAcceptanceService {

	@Autowired
	private MesStorageAcceptanceMapper mesStorageAcceptanceMapper;
	@Autowired
	private MesAcceptanceItemMapper mesAcceptanceItemMapper;
	@Autowired
	private MesAcceptanceSiteMapper mesAcceptanceSiteMapper;
	
	@Override
	@Transactional
	public void saveMain(MesStorageAcceptance mesStorageAcceptance, List<MesAcceptanceItem> mesAcceptanceItemList,List<MesAcceptanceSite> mesAcceptanceSiteList) {
		mesStorageAcceptanceMapper.insert(mesStorageAcceptance);
		if(mesAcceptanceItemList!=null && mesAcceptanceItemList.size()>0) {
			for(MesAcceptanceItem entity:mesAcceptanceItemList) {
				//外键设置
				entity.setAcceptanceId(mesStorageAcceptance.getId());
				mesAcceptanceItemMapper.insert(entity);
			}
		}
		if(mesAcceptanceSiteList!=null && mesAcceptanceSiteList.size()>0) {
			for(MesAcceptanceSite entity:mesAcceptanceSiteList) {
				//外键设置
				entity.setAcceptanceId(mesStorageAcceptance.getId());
				mesAcceptanceSiteMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(MesStorageAcceptance mesStorageAcceptance,List<MesAcceptanceItem> mesAcceptanceItemList,List<MesAcceptanceSite> mesAcceptanceSiteList) {
		mesStorageAcceptanceMapper.updateById(mesStorageAcceptance);
		
		//1.先删除子表数据
		mesAcceptanceItemMapper.deleteByMainId(mesStorageAcceptance.getId());
		mesAcceptanceSiteMapper.deleteByMainId(mesStorageAcceptance.getId());
		
		//2.子表数据重新插入
		if(mesAcceptanceItemList!=null && mesAcceptanceItemList.size()>0) {
			for(MesAcceptanceItem entity:mesAcceptanceItemList) {
				//外键设置
				entity.setAcceptanceId(mesStorageAcceptance.getId());
				mesAcceptanceItemMapper.insert(entity);
			}
		}
		if(mesAcceptanceSiteList!=null && mesAcceptanceSiteList.size()>0) {
			for(MesAcceptanceSite entity:mesAcceptanceSiteList) {
				//外键设置
				entity.setAcceptanceId(mesStorageAcceptance.getId());
				mesAcceptanceSiteMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		mesAcceptanceItemMapper.deleteByMainId(id);
		mesAcceptanceSiteMapper.deleteByMainId(id);
		mesStorageAcceptanceMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesAcceptanceItemMapper.deleteByMainId(id.toString());
			mesAcceptanceSiteMapper.deleteByMainId(id.toString());
			mesStorageAcceptanceMapper.deleteById(id);
		}
	}
	
}
