package org.jeecg.modules.mes.storage.mapper;

import java.util.List;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.storage.entity.MesMaterielTraceability;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 物料追溯表
 * @Author: jeecg-boot
 * @Date:   2021-06-02
 * @Version: V1.0
 */
public interface MesMaterielTraceabilityMapper extends BaseMapper<MesMaterielTraceability> {

    List<MesMaterielTraceability> queryPageList(Page<MesMaterielTraceability> page, @Param("mesMaterielTraceability") MesMaterielTraceability mesMaterielTraceability);
}
