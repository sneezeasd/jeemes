package org.jeecg.modules.mes.client;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.ServiceNameConstants;
import org.jeecg.modules.mes.chiefdata.entity.*;
import org.jeecg.modules.system.entity.SysUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Component
@FeignClient(contextId = "CodeRuleServiceClient", value = ServiceNameConstants.SYSTEM_SERVICE)
public interface SystemClient {

    @GetMapping("mesapi/mesapp/getdocketcode")
    public String getdocketcode(@RequestParam(name = "docketype", required = false) String docketype);

    @GetMapping("mesapi/mesapp/sendMessage")
    public String sendMessage(@RequestParam(name="userName",required=true) String userName,
                              @RequestParam(name="mName",required=true) String mName);

    @GetMapping("mesapi/mesapp/sendCheckMessage")
    public String sendCheckMessage(@RequestParam(name="userName",required=true) String userName,
                                   @RequestParam(name="mName",required=true) String mName,
                                   @RequestParam(name="receiveNum",required = true) String receiveNum,
                                   @RequestParam(name="unit",required = true) String unit);

    @GetMapping("chiefdata/mesChiefdataAncillarytool/Ancillarytooladd")
    public String Ancillarytooladd(@RequestParam(name = "mCode",required = true) String mCode);

    @GetMapping("chiefdata/mesChiefdataMaketool/Maketooladd")
    public String Maketooladd(@RequestParam(name = "mCode",required = true) String mCode);


    @GetMapping("chiefdata/mesChiefdataProductline/stopById")
    public Result<?> stopById(@RequestParam(name="id",required=true) String id, @RequestParam(name="flag",required=true) String flag) ;

    @PutMapping("chiefdata/mesChiefdataFeeder/editComitem")
    public String editfeederComitem(@RequestBody MesChiefdataFeeder mesChiefdataFeeder);

    @GetMapping("chiefdata/mesChiefdataFeeder/getfeeder")
    public MesChiefdataFeeder   getfeederComitem(@RequestParam(name="sn",required=true) String sn);

    @GetMapping("chiefdata/mesPrintModel/queryByPrintType")
    public MesPrintModel queryByPrintType(@RequestParam(name="printType",required=true) String printType);

    @GetMapping(value = "chiefdata/mesChiefdataMateriel/queryByMcode")
    public MesChiefdataMateriel queryByMcode(@RequestParam(name="mCode",required=true) String mCode);

    @GetMapping("chiefdata/mesChiefdataProductline/getproductlineCode")
    public MesChiefdataProductline queryByLineCode(@RequestParam(name="productlineCode",required=true) String productlineCode);

    @GetMapping("chiefdata/mesChiefdataBom/selectByMaterialCode")
    public List<MesChiefdataBomitem> selectByMaterialCode(@RequestParam(name = "materialId", required = true)String materialId);

    @GetMapping("chiefdata/mesChiefdataBom/getMCodebomitemForIn")
    public MesChiefdataBomitem getMCodebomitemForIn(@RequestParam(name = "materielCode", required = true) String materielCode);
    /**
     *   通过角色code查询角色信息，再根据角色信息去查询关联的所有用户
     * @param roleCode
     * @return
     */
    @GetMapping("sys/role/queryRoleByCode")
    public List<SysUser> queryRoleByCode(@RequestParam(name = "roleCode", required = true) String roleCode);

    @GetMapping("sys/user/queryUserListByRoleId")
    public List<SysUser> queryUserListByRoleId(@RequestParam(name = "roleId", required = true) String roleId);
    /**
     * 根据成品料号和替代料料号查询 远程调用
     * @param machineSort
     * @param replaceCode
     * @return
     */
    @GetMapping(value = "chiefdata/mesChiefdataReplacematerial/queryReplaceCode")
    public List<MesChiefdataReplacematerial> queryReplaceCode(@RequestParam(name="machineSort",required=true) String machineSort,
                                                              @RequestParam(name="mainCode",required=false) String mainCode,
                                                              @RequestParam(name="replaceCode",required=true) String replaceCode);

}
