package org.jeecg.modules.mes.storage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.mes.order.entity.MesOutproWater;
import org.jeecg.modules.mes.storage.mapper.MesOutproWaterMapper;
import org.jeecg.modules.mes.storage.service.IMesOutproWaterService;
import org.springframework.stereotype.Service;

/**
 * @Description: 出货流水报表
 * @Author: jeecg-boot
 * @Date:   2021-03-08
 * @Version: V1.0
 */
@Service
public class MesOutproWaterServiceImpl extends ServiceImpl<MesOutproWaterMapper, MesOutproWater> implements IMesOutproWaterService {

}
