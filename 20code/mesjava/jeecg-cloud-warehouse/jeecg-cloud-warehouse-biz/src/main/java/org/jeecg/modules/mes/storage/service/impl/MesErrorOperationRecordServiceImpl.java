package org.jeecg.modules.mes.storage.service.impl;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.storage.entity.MesErrorOperationRecord;
import org.jeecg.modules.mes.storage.mapper.MesErrorOperationRecordMapper;
import org.jeecg.modules.mes.storage.service.IMesErrorOperationRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 错误操作记录
 * @Author: jeecg-boot
 * @Date:   2021-05-31
 * @Version: V1.0
 */
@Service
public class MesErrorOperationRecordServiceImpl extends ServiceImpl<MesErrorOperationRecordMapper, MesErrorOperationRecord> implements IMesErrorOperationRecordService {

    @Autowired
    private MesErrorOperationRecordMapper mesErrorOperationRecordMapper;

    @Override
    public Result<?> add(MesErrorOperationRecord mesErrorOperationRecord) {
        mesErrorOperationRecordMapper.insert(mesErrorOperationRecord);
        return Result.ok("添加成功！");
    }

}
