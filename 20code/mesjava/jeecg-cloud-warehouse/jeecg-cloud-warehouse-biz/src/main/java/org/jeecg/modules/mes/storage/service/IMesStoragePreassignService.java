package org.jeecg.modules.mes.storage.service;

import org.jeecg.modules.mes.storage.entity.MesStoragePreassign;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 仓库管理—预配明细
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface IMesStoragePreassignService extends IService<MesStoragePreassign> {

}
