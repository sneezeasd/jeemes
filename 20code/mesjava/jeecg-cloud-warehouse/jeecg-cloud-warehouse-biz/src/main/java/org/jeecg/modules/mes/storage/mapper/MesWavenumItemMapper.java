package org.jeecg.modules.mes.storage.mapper;

import java.util.List;
import org.jeecg.modules.mes.storage.entity.MesWavenumItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 仓库管理—波次单子表
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface MesWavenumItemMapper extends BaseMapper<MesWavenumItem> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesWavenumItem> selectByMainId(@Param("mainId") String mainId);
}
