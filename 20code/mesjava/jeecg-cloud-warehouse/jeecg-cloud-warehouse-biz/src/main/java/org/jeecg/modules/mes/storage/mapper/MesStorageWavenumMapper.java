package org.jeecg.modules.mes.storage.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.storage.entity.MesStorageWavenum;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 仓库管理—波次单
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface MesStorageWavenumMapper extends BaseMapper<MesStorageWavenum> {

}
