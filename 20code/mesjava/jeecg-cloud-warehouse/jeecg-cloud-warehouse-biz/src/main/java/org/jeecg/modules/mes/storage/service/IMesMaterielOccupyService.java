package org.jeecg.modules.mes.storage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mes.order.entity.MesOrderProduce;
import org.jeecg.modules.mes.storage.entity.MesMaterielOccupy;

/**
 * @Description: 仓库管理-领料清单
 * @Author: jeecg-boot
 * @Date:   2020-11-16
 * @Version: V1.0
 */
public interface IMesMaterielOccupyService extends IService<MesMaterielOccupy> {

    String addMaterielOccupyByProduce(MesOrderProduce mesOrderProduce);

    /**
     * 根据生产订单id和物料料号查询领料清单 并且更新领料表的退料数量 远程调用
     *
     * @param orderId
     * @param mCode
     * @param withdrawNum 退料数量
     * @return
     */
    public MesMaterielOccupy queryByIdAndmCodeUpdatewithdrawNum(String orderId,String mCode,String withdrawNum);

    /**
     * 根据生产订单id和物料料号查询领料清单 并且更新领料表的发料数量  远程调用
     *
     * @param orderId
     * @param mCode
     * @param sendNum 发料数量
     * @return
     */
    public MesMaterielOccupy queryByIdAndmCodeUpdateSendNum(String orderId,String mCode,String sendNum);

    /**
     * 根据生产订单id和物料料号查询领料清单 并且更新领料表的转产数量  远程调用
     *
     * @param orderId
     * @param mCode
     * @param realNum 转产数量
     * @return
     */
    public MesMaterielOccupy queryByIdAndmCodeUpdateTransformNum(String orderId,String mCode,String realNum);
}
