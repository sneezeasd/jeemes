package org.jeecg.modules.mes.storage.service.impl;

import org.jeecg.modules.mes.storage.entity.MesStorageCount;
import org.jeecg.modules.mes.storage.entity.MesCountItem;
import org.jeecg.modules.mes.storage.mapper.MesCountItemMapper;
import org.jeecg.modules.mes.storage.mapper.MesStorageCountMapper;
import org.jeecg.modules.mes.storage.service.IMesStorageCountService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 仓库管理—盘点
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesStorageCountServiceImpl extends ServiceImpl<MesStorageCountMapper, MesStorageCount> implements IMesStorageCountService {

	@Autowired
	private MesStorageCountMapper mesStorageCountMapper;
	@Autowired
	private MesCountItemMapper mesCountItemMapper;
	
	@Override
	@Transactional
	public void saveMain(MesStorageCount mesStorageCount, List<MesCountItem> mesCountItemList) {
		mesStorageCountMapper.insert(mesStorageCount);
		if(mesCountItemList!=null && mesCountItemList.size()>0) {
			for(MesCountItem entity:mesCountItemList) {
				//外键设置
				entity.setCountId(mesStorageCount.getId());
				mesCountItemMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(MesStorageCount mesStorageCount,List<MesCountItem> mesCountItemList) {
		mesStorageCountMapper.updateById(mesStorageCount);
		
		//1.先删除子表数据
		mesCountItemMapper.deleteByMainId(mesStorageCount.getId());
		
		//2.子表数据重新插入
		if(mesCountItemList!=null && mesCountItemList.size()>0) {
			for(MesCountItem entity:mesCountItemList) {
				//外键设置
				entity.setCountId(mesStorageCount.getId());
				mesCountItemMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		mesCountItemMapper.deleteByMainId(id);
		mesStorageCountMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesCountItemMapper.deleteByMainId(id.toString());
			mesStorageCountMapper.deleteById(id);
		}
	}
	
}
