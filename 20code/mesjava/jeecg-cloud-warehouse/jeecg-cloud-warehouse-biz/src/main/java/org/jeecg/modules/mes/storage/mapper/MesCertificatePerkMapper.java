package org.jeecg.modules.mes.storage.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.mes.storage.entity.MesCertificatePerk;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 物料凭证抬头
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
public interface MesCertificatePerkMapper extends BaseMapper<MesCertificatePerk> {

    @Select("select *  from mes_certificate_perk where id in (select perk_id from mes_certificate_item WHERE factory_name=#{oldCode} and query5=#{nowCode})")
    public List<MesCertificatePerk> getChangeProduce(@Param("oldCode")String oldCode,@Param("nowCode")String nowCode);
}
