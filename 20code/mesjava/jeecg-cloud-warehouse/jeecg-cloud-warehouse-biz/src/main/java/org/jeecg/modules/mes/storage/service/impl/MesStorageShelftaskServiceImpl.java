package org.jeecg.modules.mes.storage.service.impl;

import org.jeecg.modules.mes.storage.entity.MesStorageShelftask;
import org.jeecg.modules.mes.storage.mapper.MesStorageShelftaskMapper;
import org.jeecg.modules.mes.storage.service.IMesStorageShelftaskService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 仓库管理—上架/下架任务
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesStorageShelftaskServiceImpl extends ServiceImpl<MesStorageShelftaskMapper, MesStorageShelftask> implements IMesStorageShelftaskService {

}
