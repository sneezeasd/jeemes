package org.jeecg.modules.mes.storage.service.impl;

import org.jeecg.modules.mes.storage.entity.MesAcceptanceSite;
import org.jeecg.modules.mes.storage.mapper.MesAcceptanceSiteMapper;
import org.jeecg.modules.mes.storage.service.IMesAcceptanceSiteService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 仓库管理—库位子表
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesAcceptanceSiteServiceImpl extends ServiceImpl<MesAcceptanceSiteMapper, MesAcceptanceSite> implements IMesAcceptanceSiteService {
	
	@Autowired
	private MesAcceptanceSiteMapper mesAcceptanceSiteMapper;
	
	@Override
	public List<MesAcceptanceSite> selectByMainId(String mainId) {
		return mesAcceptanceSiteMapper.selectByMainId(mainId);
	}
}
