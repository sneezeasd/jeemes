package org.jeecg.modules.mes.storage.service;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.storage.entity.MesErrorOperationRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 错误操作记录
 * @Author: jeecg-boot
 * @Date:   2021-05-31
 * @Version: V1.0
 */
public interface IMesErrorOperationRecordService extends IService<MesErrorOperationRecord> {

    Result<?> add(MesErrorOperationRecord mesErrorOperationRecord);
}
