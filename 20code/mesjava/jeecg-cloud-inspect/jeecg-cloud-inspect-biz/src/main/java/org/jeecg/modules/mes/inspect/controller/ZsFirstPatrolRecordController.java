package org.jeecg.modules.mes.inspect.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.inspect.entity.ZsFirstPatrolRecord;
import org.jeecg.modules.mes.inspect.service.IZsFirstPatrolRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

 /**
 * @Description: 宗申首、巡、末件检验记录表
 * @Author: jeecg-boot
 * @Date:   2021-05-15
 * @Version: V1.0
 */
@Api(tags="宗申首、巡、末件检验记录表")
@RestController
@RequestMapping("/inspect/zsFirstPatrolRecord")
@Slf4j
public class ZsFirstPatrolRecordController extends JeecgController<ZsFirstPatrolRecord, IZsFirstPatrolRecordService> {
	@Autowired
	private IZsFirstPatrolRecordService zsFirstPatrolRecordService;
	
	/**
	 * 分页列表查询
	 *
	 * @param zsFirstPatrolRecord
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "宗申首、巡、末件检验记录表-分页列表查询")
	@ApiOperation(value="宗申首、巡、末件检验记录表-分页列表查询", notes="宗申首、巡、末件检验记录表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ZsFirstPatrolRecord zsFirstPatrolRecord,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<ZsFirstPatrolRecord> queryWrapper = QueryGenerator.initQueryWrapper(zsFirstPatrolRecord, req.getParameterMap());
		Page<ZsFirstPatrolRecord> page = new Page<ZsFirstPatrolRecord>(pageNo, pageSize);
		IPage<ZsFirstPatrolRecord> pageList = zsFirstPatrolRecordService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param zsFirstPatrolRecord
	 * @return
	 */
	@AutoLog(value = "宗申首、巡、末件检验记录表-添加")
	@ApiOperation(value="宗申首、巡、末件检验记录表-添加", notes="宗申首、巡、末件检验记录表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ZsFirstPatrolRecord zsFirstPatrolRecord) {

		zsFirstPatrolRecordService.save(zsFirstPatrolRecord);
		return Result.ok("添加成功！");

	}

	
	/**
	 *  编辑
	 *
	 * @param zsFirstPatrolRecord
	 * @return
	 */
	@AutoLog(value = "宗申首、巡、末件检验记录表-编辑")
	@ApiOperation(value="宗申首、巡、末件检验记录表-编辑", notes="宗申首、巡、末件检验记录表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ZsFirstPatrolRecord zsFirstPatrolRecord) {
		zsFirstPatrolRecordService.updateById(zsFirstPatrolRecord);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "宗申首、巡、末件检验记录表-通过id删除")
	@ApiOperation(value="宗申首、巡、末件检验记录表-通过id删除", notes="宗申首、巡、末件检验记录表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		zsFirstPatrolRecordService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "宗申首、巡、末件检验记录表-批量删除")
	@ApiOperation(value="宗申首、巡、末件检验记录表-批量删除", notes="宗申首、巡、末件检验记录表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.zsFirstPatrolRecordService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "宗申首、巡、末件检验记录表-通过id查询")
	@ApiOperation(value="宗申首、巡、末件检验记录表-通过id查询", notes="宗申首、巡、末件检验记录表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ZsFirstPatrolRecord zsFirstPatrolRecord = zsFirstPatrolRecordService.getById(id);
		if(zsFirstPatrolRecord==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(zsFirstPatrolRecord);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param zsFirstPatrolRecord
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZsFirstPatrolRecord zsFirstPatrolRecord) {
        return super.exportXls(request, zsFirstPatrolRecord, ZsFirstPatrolRecord.class, "宗申首、巡、末件检验记录表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZsFirstPatrolRecord.class);
    }

}
