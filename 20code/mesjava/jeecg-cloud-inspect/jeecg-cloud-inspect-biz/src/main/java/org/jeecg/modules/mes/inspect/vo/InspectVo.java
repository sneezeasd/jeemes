package org.jeecg.modules.mes.inspect.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class InspectVo implements Serializable {

    @ApiModelProperty(value = "月份")
    private String month;
    @ApiModelProperty(value = "不良名称")
    private String badname;
    @ApiModelProperty(value = "不良率")
    private String bll;
    @ApiModelProperty(value = "良率")
    private String lpl;
    @ApiModelProperty(value = "良品数")
    private String lps;
    @ApiModelProperty(value = "总数")
    private String zs;
    @ApiModelProperty(value = "不良数")
    private String bls;
    @ApiModelProperty(value = "目标值")
    private String goal;
    @ApiModelProperty(value = "额外率")
    private String addition;
    @ApiModelProperty(value = "部门")
    private String org;
    @ApiModelProperty(value = "产线")
    private String name;
    @ApiModelProperty(value = "产线类型")
    private String value;
}
