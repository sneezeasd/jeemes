package org.jeecg.modules.mes.inspect.service;

import org.jeecg.modules.mes.inspect.entity.MesBadnessInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 检验不良信息
 * @Author: jeecg-boot
 * @Date:   2020-10-29
 * @Version: V1.0
 */
public interface IMesBadnessInfoService extends IService<MesBadnessInfo> {

	public List<MesBadnessInfo> selectByMainId(String mainId);
}
