package org.jeecg.modules.mes.inspect.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.inspect.entity.MesBadnessHandle;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 质检中心-不良处理
 * @Author: jeecg-boot
 * @Date:   2020-10-29
 * @Version: V1.0
 */
public interface MesBadnessHandleMapper extends BaseMapper<MesBadnessHandle> {

}
