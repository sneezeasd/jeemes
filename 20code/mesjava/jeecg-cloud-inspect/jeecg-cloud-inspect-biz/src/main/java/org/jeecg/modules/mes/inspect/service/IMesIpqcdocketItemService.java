package org.jeecg.modules.mes.inspect.service;

import org.jeecg.modules.mes.inspect.entity.MesIpqcdocketItem;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 单据明细
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
public interface IMesIpqcdocketItemService extends IService<MesIpqcdocketItem> {

	public List<MesIpqcdocketItem> selectByMainId(String mainId);
}
