package org.jeecg.modules.mes.inspect.service;

import org.jeecg.modules.mes.inspect.entity.MesQualityAbnormal;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 质检中心-品质异常单
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
public interface IMesQualityAbnormalService extends IService<MesQualityAbnormal> {

}
