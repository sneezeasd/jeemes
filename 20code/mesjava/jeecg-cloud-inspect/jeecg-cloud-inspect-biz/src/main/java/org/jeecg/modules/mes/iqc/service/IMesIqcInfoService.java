package org.jeecg.modules.mes.iqc.service;

import org.jeecg.modules.mes.iqc.entity.MesIqcDefect;
import org.jeecg.modules.mes.iqc.entity.MesIqcInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mes.iqc.entity.MesIqcMaterial;
import org.jeecg.modules.mes.iqc.mapper.MesIqcDefectMapper;
import org.jeecg.modules.mes.storage.entity.MesAcceptanceItem;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @Description: 来料检验报告
 * @Author: jeecg-boot
 * @Date:   2021-03-19
 * @Version: V1.0
 */
public interface IMesIqcInfoService extends IService<MesIqcInfo> {

    public List<MesIqcDefect> selectDefectByMainId(String mainId);

    public List<MesIqcMaterial> selectMaterialByMainId(String mainId);

    public void saveMain(MesIqcInfo info1,List<MesIqcDefect> defectlist,MesIqcMaterial info3);

}
