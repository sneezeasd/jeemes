package org.jeecg.modules.mes.inspect.service.impl;

import org.jeecg.modules.mes.inspect.entity.MaterielRetreatReason;
import org.jeecg.modules.mes.inspect.mapper.MaterielRetreatReasonMapper;
import org.jeecg.modules.mes.inspect.service.IMaterielRetreatReasonService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 退料原因表
 * @Author: jeecg-boot
 * @Date:   2021-04-25
 * @Version: V1.0
 */
@Service
public class MaterielRetreatReasonServiceImpl extends ServiceImpl<MaterielRetreatReasonMapper, MaterielRetreatReason> implements IMaterielRetreatReasonService {
	
	@Autowired
	private MaterielRetreatReasonMapper materielRetreatReasonMapper;
	
	@Override
	public List<MaterielRetreatReason> selectByMainId(String mainId) {
		return materielRetreatReasonMapper.selectByMainId(mainId);
	}
}
