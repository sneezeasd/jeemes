package org.jeecg.modules.mes.inspect.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.inspect.entity.MesBodLog;
import org.jeecg.modules.mes.inspect.entity.MesInspectionOut;
import org.jeecg.modules.mes.inspect.service.IMesBodLogService;
import org.jeecg.modules.mes.inspect.service.IMesInspectionOutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

 /**
 * @Description: 出货检验报告
 * @Author: jeecg-boot
 * @Date:   2021-03-20
 * @Version: V1.0
 */
@Api(tags="出货检验报告")
@RestController
@RequestMapping("/inspect/mesInspectionOut")
@Slf4j
public class MesInspectionOutController extends JeecgController<MesInspectionOut, IMesInspectionOutService> {
	@Autowired
	private IMesInspectionOutService mesInspectionOutService;
	@Autowired
	private IMesBodLogService mesBodLogService;

	/**
	 * 分页列表查询
	 *
	 * @param mesInspectionOut
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "出货检验报告-分页列表查询")
	@ApiOperation(value="出货检验报告-分页列表查询", notes="出货检验报告-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesInspectionOut mesInspectionOut,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesInspectionOut> queryWrapper = QueryGenerator.initQueryWrapper(mesInspectionOut, req.getParameterMap());
		Page<MesInspectionOut> page = new Page<MesInspectionOut>(pageNo, pageSize);
		IPage<MesInspectionOut> pageList = mesInspectionOutService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesInspectionOut
	 * @return
	 */
	@AutoLog(value = "出货检验报告-添加")
	@ApiOperation(value="出货检验报告-添加", notes="出货检验报告-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesInspectionOut mesInspectionOut) {
		mesInspectionOutService.save(mesInspectionOut);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesInspectionOut
	 * @return
	 */
	@AutoLog(value = "出货检验报告-编辑")
	@ApiOperation(value="出货检验报告-编辑", notes="出货检验报告-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesInspectionOut mesInspectionOut) {
		mesInspectionOutService.updateById(mesInspectionOut);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "出货检验报告-通过id删除")
	@ApiOperation(value="出货检验报告-通过id删除", notes="出货检验报告-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesInspectionOutService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "出货检验报告-批量删除")
	@ApiOperation(value="出货检验报告-批量删除", notes="出货检验报告-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesInspectionOutService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "出货检验报告-通过id查询")
	@ApiOperation(value="出货检验报告-通过id查询", notes="出货检验报告-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesInspectionOut mesInspectionOut = mesInspectionOutService.getById(id);
		if(mesInspectionOut==null) {
			return Result.error("未找到对应数据");
		}else {
			QueryWrapper<MesBodLog> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("out_id",mesInspectionOut.getId());
			List<MesBodLog> list = mesBodLogService.list(queryWrapper);
			//每10条一个数组
			mesInspectionOut.setMesBodLogList(getSubList(10,list));

		}
		return Result.ok(mesInspectionOut);
	}

	 /**
	  * 将list拆分成指定数量的小list
	  * 注: 使用的subList方式,返回的是list的内部类,不可做元素的删除,修改,添加操作
	  * @param length 数量
	  * @param list 大list
	  */
	 public static List<List<MesBodLog>> getSubList(int length, List<MesBodLog> list){
		 int size = list.size();
		 int temp = size / length + 1;
		 boolean result = size % length == 0;
		 List<List<MesBodLog>> subList = new ArrayList<>();
		 for (int i = 0; i < temp; i++) {
			 if (i == temp - 1) {
				 if (result) {
					 break;
				 }
				 subList.add(list.subList(length * i, size)) ;
			 } else {
				 subList.add(list.subList(length * i, length * (i + 1))) ;
			 }
		 }
		 return subList;
	 }

    /**
    * 导出excel
    *
    * @param request
    * @param mesInspectionOut
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesInspectionOut mesInspectionOut) {
        return super.exportXls(request, mesInspectionOut, MesInspectionOut.class, "出货检验报告");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesInspectionOut.class);
    }

}
