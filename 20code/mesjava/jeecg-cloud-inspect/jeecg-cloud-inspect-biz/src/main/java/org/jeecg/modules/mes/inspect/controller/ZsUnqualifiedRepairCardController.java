package org.jeecg.modules.mes.inspect.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.inspect.entity.ZsUnqualifiedRepairCard;
import org.jeecg.modules.mes.inspect.service.IZsUnqualifiedRepairCardService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 宗申测试不合格反修卡
 * @Author: jeecg-boot
 * @Date: 2021-05-15
 * @Version: V1.0
 */
@Api(tags = "宗申测试不合格反修卡")
@RestController
@RequestMapping("/inspect/zsUnqualifiedRepairCard")
@Slf4j
public class ZsUnqualifiedRepairCardController extends JeecgController<ZsUnqualifiedRepairCard, IZsUnqualifiedRepairCardService> {
    @Autowired
    private IZsUnqualifiedRepairCardService zsUnqualifiedRepairCardService;

    /**
     * 根据反修卡状态查询信息
     */
    @AutoLog(value = "宗申测试不合格反修卡-根据反修卡状态查询信息")
    @ApiOperation(value = "宗申测试不合格反修卡-根据反修卡状态查询信息", notes = "宗申测试不合格反修卡-根据反修卡状态查询信息")
    @GetMapping(value = "/findUnqualifiedRepairByStatus")
    public Result<?> findUnqualifiedRepairByStatus(@RequestParam(name = "status") String status,
                                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        Page<ZsUnqualifiedRepairCard> page = new Page<ZsUnqualifiedRepairCard>(pageNo, pageSize);
        return zsUnqualifiedRepairCardService.findUnqualifiedRepairByStatus(status, page);
    }

    /**
     * 根据条码和状态查询反修卡信息
     */
    @AutoLog(value = "宗申测试不合格反修卡-根据条码和状态查询反修卡信息")
    @ApiOperation(value = "宗申测试不合格反修卡-根据条码和状态查询反修卡信息", notes = "宗申测试不合格反修卡-根据条码和状态查询反修卡信息")
    @GetMapping(value = "/findUnqualifiedRepairByStatusBarcode")
    public Result<?> findUnqualifiedRepairByStatusBarcode(@RequestParam(name = "status") String status,
                                                          @RequestParam(name = "barcode") String barcode) {
        return zsUnqualifiedRepairCardService.findUnqualifiedRepairByStatusBarcode(status, barcode);
    }

    /**
     * 分页列表查询
     *
     * @param zsUnqualifiedRepairCard
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "宗申测试不合格反修卡-分页列表查询")
    @ApiOperation(value = "宗申测试不合格反修卡-分页列表查询", notes = "宗申测试不合格反修卡-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZsUnqualifiedRepairCard zsUnqualifiedRepairCard,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        if (com.epms.util.ObjectHelper.isNotEmpty(zsUnqualifiedRepairCard) && com.epms.util.ObjectHelper.isEmpty(zsUnqualifiedRepairCard.getType())) {
            zsUnqualifiedRepairCard.setType("repair_yzy");
        }
        QueryWrapper<ZsUnqualifiedRepairCard> queryWrapper = QueryGenerator.initQueryWrapper(zsUnqualifiedRepairCard, req.getParameterMap());
        Page<ZsUnqualifiedRepairCard> page = new Page<ZsUnqualifiedRepairCard>(pageNo, pageSize);
        return zsUnqualifiedRepairCardService.queryPageList(page, queryWrapper);
    }

    /**
     * 分页列表查询
     *
     * @param zsUnqualifiedRepairCard
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "宗申测试不合格反修卡-分页列表查询")
    @ApiOperation(value = "宗申测试不合格反修卡-分页列表查询", notes = "宗申测试不合格反修卡-分页列表查询")
    @GetMapping(value = "/lists")
    public Result<?> queryPageLists(ZsUnqualifiedRepairCard zsUnqualifiedRepairCard,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<ZsUnqualifiedRepairCard> queryWrapper = QueryGenerator.initQueryWrapper(zsUnqualifiedRepairCard, req.getParameterMap());
        queryWrapper.ne("status","confirm_complete");
        Page<ZsUnqualifiedRepairCard> page = new Page<ZsUnqualifiedRepairCard>(pageNo, pageSize);
        return zsUnqualifiedRepairCardService.queryPageList(page, queryWrapper);
    }

    /**
     * 添加
     *
     * @param zsUnqualifiedRepairCard
     * @return
     */
    @AutoLog(value = "宗申测试不合格反修卡-添加")
    @ApiOperation(value = "宗申测试不合格反修卡-添加", notes = "宗申测试不合格反修卡-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZsUnqualifiedRepairCard zsUnqualifiedRepairCard) {
        return zsUnqualifiedRepairCardService.add(zsUnqualifiedRepairCard);
    }

    /**
     * 编辑
     *
     * @param zsUnqualifiedRepairCard
     * @return
     */
    @AutoLog(value = "宗申测试不合格反修卡-编辑")
    @ApiOperation(value = "宗申测试不合格反修卡-编辑", notes = "宗申测试不合格反修卡-编辑")
    @PostMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZsUnqualifiedRepairCard zsUnqualifiedRepairCard) {
        return zsUnqualifiedRepairCardService.edit(zsUnqualifiedRepairCard);
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "宗申测试不合格反修卡-通过id删除")
    @ApiOperation(value = "宗申测试不合格反修卡-通过id删除", notes = "宗申测试不合格反修卡-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id") String id) {
        return zsUnqualifiedRepairCardService.delete(id);
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "宗申测试不合格反修卡-批量删除")
    @ApiOperation(value = "宗申测试不合格反修卡-批量删除", notes = "宗申测试不合格反修卡-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids") String ids) {
        return zsUnqualifiedRepairCardService.deleteBatch(ids);
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "宗申测试不合格反修卡-通过id查询")
    @ApiOperation(value = "宗申测试不合格反修卡-通过id查询", notes = "宗申测试不合格反修卡-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id") String id) {
        return zsUnqualifiedRepairCardService.queryById(id);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param zsUnqualifiedRepairCard
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZsUnqualifiedRepairCard zsUnqualifiedRepairCard) {
        return super.exportXls(request, zsUnqualifiedRepairCard, ZsUnqualifiedRepairCard.class, "宗申测试不合格反修卡");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZsUnqualifiedRepairCard.class);
    }

}
