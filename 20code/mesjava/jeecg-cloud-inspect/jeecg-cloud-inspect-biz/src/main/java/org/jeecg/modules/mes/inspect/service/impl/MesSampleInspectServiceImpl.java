package org.jeecg.modules.mes.inspect.service.impl;

import org.jeecg.modules.mes.inspect.entity.MesSampleInspect;
import org.jeecg.modules.mes.inspect.mapper.MesSampleInspectMapper;
import org.jeecg.modules.mes.inspect.service.IMesSampleInspectService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 品质报表—抽检
 * @Author: jeecg-boot
 * @Date:   2021-01-20
 * @Version: V1.0
 */
@Service
public class MesSampleInspectServiceImpl extends ServiceImpl<MesSampleInspectMapper, MesSampleInspect> implements IMesSampleInspectService {

}
