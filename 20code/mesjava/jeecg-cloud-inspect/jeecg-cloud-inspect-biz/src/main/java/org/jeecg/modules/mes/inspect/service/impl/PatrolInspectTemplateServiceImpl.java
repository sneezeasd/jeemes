package org.jeecg.modules.mes.inspect.service.impl;

import org.jeecg.modules.mes.inspect.entity.PatrolInspectTemplate;
import org.jeecg.modules.mes.inspect.mapper.PatrolInspectTemplateMapper;
import org.jeecg.modules.mes.inspect.service.IPatrolInspectTemplateService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 巡检模板表
 * @Author: jeecg-boot
 * @Date:   2021-03-20
 * @Version: V1.0
 */
@Service
public class PatrolInspectTemplateServiceImpl extends ServiceImpl<PatrolInspectTemplateMapper, PatrolInspectTemplate> implements IPatrolInspectTemplateService {

}
