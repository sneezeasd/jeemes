package org.jeecg.modules.mes.inspect.service;

import org.jeecg.modules.mes.inspect.entity.MesIpqcInspect;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 质检中心-IPQC检测项
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
public interface IMesIpqcInspectService extends IService<MesIpqcInspect> {

    public String inspectTotle();
    public String inspectOutTotle();
    public String inspectIqcOutSum();
}
