package org.jeecg.modules.mes.iqc.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.mes.iqc.entity.MesIqcDefect;
import org.jeecg.modules.mes.iqc.entity.MesIqcInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.mes.iqc.entity.MesIqcMaterial;

/**
 * @Description: 来料检验报告
 * @Author: jeecg-boot
 * @Date:   2021-03-19
 * @Version: V1.0
 */
public interface MesIqcInfoMapper extends BaseMapper<MesIqcInfo> {


}
