package org.jeecg.modules.mes.inspect.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.common.util.DateUtils;
import org.jeecg.modules.mes.inspect.entity.PatrolInspectLog;
import org.jeecg.modules.mes.inspect.entity.PatrolInspectMain;
import org.jeecg.modules.mes.inspect.mapper.PatrolInspectLogMapper;
import org.jeecg.modules.mes.inspect.mapper.PatrolInspectMainMapper;
import org.jeecg.modules.mes.inspect.service.IPatrolInspectMainService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.List;

/**
 * @Description: 巡检模块主表
 * @Author: jeecg-boot
 * @Date:   2021-03-21
 * @Version: V1.0
 */
@Service
public class PatrolInspectMainServiceImpl extends ServiceImpl<PatrolInspectMainMapper, PatrolInspectMain> implements IPatrolInspectMainService {

    @Autowired
    private PatrolInspectLogMapper patrolInspectLogMapper;

    @Autowired
    private PatrolInspectMainMapper patrolInspectMainMapper;


    @Transactional
    public void saveMain(List<PatrolInspectLog> inspectLogS){
        PatrolInspectLog inspectLog=inspectLogS.get(0);
        String inspectFrequen="夜班";
        if(DateUtils.JudgeTime("08:00:00","20:00:00")) {//是不是白班
            inspectFrequen="白班";
        }
        PatrolInspectMain inspectMain = new PatrolInspectMain();
        BeanUtils.copyProperties(inspectLog, inspectMain);
        inspectMain.setCreateTime(DateUtils.getDate());
        inspectMain.setInspectFrequen(inspectFrequen);
        patrolInspectMainMapper.insert(inspectMain);

        for (int i = 0; i < inspectLogS.size(); i++) {
            inspectLogS.get(i).setMainId(inspectMain.getId());
            inspectLogS.get(i).setInspectFrequen(inspectFrequen);
            patrolInspectLogMapper.insert(inspectLogS.get(i));
        }

    }

    /**
     * 判断当前时间是否在 区间内 （精确到毫秒值）
     * @param sTime	开始区间	HH:mm:ss
     * @param eTime	结束区间	HH:mm:ss
     * @return
     * @author liren
     */
    /*public static boolean JudgeTime(String sTime, String eTime) {
        long now = System.currentTimeMillis();
        String shortNow = "";
        String judge = "";
        boolean flag = false;
        try {
            // 当前时间 -> yyyy-MM-dd
            String dateShort = getDateShort(now);
            String sDate = String.valueOf(StrToLong(dateShort + " " + sTime));
            String eDate = String.valueOf(StrToLong(dateShort + " " + eTime));
            shortNow = String.valueOf(now);
            judge = "[" + sDate + "," + eDate + "]";

            // IntervalUtil.isInTheInterval工具类，详情请见：https://blog.csdn.net/qq_26465035/article/details/98626346
            flag = IntervalUtil.isInTheInterval(shortNow, judge);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }*/

   /*public static void main(String[] args) {
        System.out.println(JudgeTime("20:00:00","08:00:00"));
    }*/
    /**
     * 获取当前时间的年月日
     * @param ctime
     * @return
     * @author liren
     */
    /*public static String getDateShort(long ctime){
        if(ctime<=0){
            return "";
        }
        SimpleDateFormat format =  new SimpleDateFormat("yyyy-MM-dd");
        return format.format(new Date(ctime)).toString();
    }*/


    /**
     * 字符串 转时间戳 : yyyy-MM-dd HH:mm:ss --> 时间戳
     * @return
     * @throws ParseException
     * @author liren
     */
    /*public static long StrToLong(String str) {
        Date date = null;
        long ts = 0;
        if (StringUtils.isNotBlank(str)) {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                date = df.parse(str);
                ts = date.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return ts;
    }*/

}
