package org.jeecg.modules.mes.inspect.service.impl;

import org.jeecg.modules.mes.inspect.entity.MesAbnormalControl;
import org.jeecg.modules.mes.inspect.mapper.MesAbnormalControlMapper;
import org.jeecg.modules.mes.inspect.service.IMesAbnormalControlService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 品质报表—HOLD产品异常控制单
 * @Author: jeecg-boot
 * @Date:   2021-01-20
 * @Version: V1.0
 */
@Service
public class MesAbnormalControlServiceImpl extends ServiceImpl<MesAbnormalControlMapper, MesAbnormalControl> implements IMesAbnormalControlService {

}
