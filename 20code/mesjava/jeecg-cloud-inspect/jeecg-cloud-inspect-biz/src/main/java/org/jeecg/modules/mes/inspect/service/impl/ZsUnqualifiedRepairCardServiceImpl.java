package org.jeecg.modules.mes.inspect.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.inspect.entity.ZsUnqualifiedRepairCard;
import org.jeecg.modules.mes.inspect.mapper.ZsUnqualifiedRepairCardMapper;
import org.jeecg.modules.mes.inspect.service.IZsUnqualifiedRepairCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.epms.util.ObjectHelper;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.Date;
import java.util.List;
import java.util.Arrays;

import static org.jeecg.common.util.DateUtils.JudgeTime;

/**
 * @Description: 宗申测试不合格反修卡
 * @Author: jeecg-boot
 * @Date: 2021-05-15
 * @Version: V1.0
 */
@Service
public class ZsUnqualifiedRepairCardServiceImpl extends ServiceImpl<ZsUnqualifiedRepairCardMapper, ZsUnqualifiedRepairCard> implements IZsUnqualifiedRepairCardService {

    @Autowired
    private ZsUnqualifiedRepairCardMapper zsUnqualifiedRepairCardMapper;

    /**
     * 根据反修卡状态查询信息
     */
    @Override
    public Result<?> findUnqualifiedRepairByStatus(String status, Page<ZsUnqualifiedRepairCard> page) {
        QueryWrapper<ZsUnqualifiedRepairCard> queryWrapper = new QueryWrapper();
        queryWrapper.eq("status", status);
        IPage<ZsUnqualifiedRepairCard> pageList = zsUnqualifiedRepairCardMapper.selectPage(page, queryWrapper);
        return Result.ok(pageList);
    }

    /**
     * 根据条码和状态查询反修卡信息
     */
    @Override
    public Result<?> findUnqualifiedRepairByStatusBarcode(String status, String barcode) {
        QueryWrapper<ZsUnqualifiedRepairCard> queryWrapper = new QueryWrapper();
        queryWrapper.eq("status", status);
        queryWrapper.eq("corresponding_barcode", barcode);
        List<ZsUnqualifiedRepairCard> zsUnqualifiedRepairCard = zsUnqualifiedRepairCardMapper.selectList(queryWrapper);
        if (ObjectHelper.isNotEmpty(zsUnqualifiedRepairCard)) {
            return Result.ok(zsUnqualifiedRepairCard.get(0));
        }
        return Result.error("未找到对应数据");
    }

    /**
     * 分页列表查询
     */
    @Override
    public Result<?> queryPageList(Page<ZsUnqualifiedRepairCard> page, QueryWrapper<ZsUnqualifiedRepairCard> queryWrapper) {
        IPage<ZsUnqualifiedRepairCard> pageList = zsUnqualifiedRepairCardMapper.selectPage(page, queryWrapper);
        return Result.ok(pageList);
    }

    /**
     * 通过id查询
     */
    @Override
    public Result<?> queryById(String id) {
        ZsUnqualifiedRepairCard zsUnqualifiedRepairCard = this.getById(id);
        if (ObjectHelper.isEmpty(zsUnqualifiedRepairCard)) {
            return Result.error("未找到对应数据");
        }
        return Result.ok(zsUnqualifiedRepairCard);
    }

    /**
     * 添加
     */
    @Override
    public Result<?> add(ZsUnqualifiedRepairCard zsUnqualifiedRepairCard) {
        if (ObjectHelper.isNotEmpty(zsUnqualifiedRepairCard)) {
            if (ObjectHelper.isEmpty(zsUnqualifiedRepairCard.getHappenDate())) {
                zsUnqualifiedRepairCard.setHappenDate(new Date());
            }
            if (ObjectHelper.isEmpty(zsUnqualifiedRepairCard.getStatus())) {
                zsUnqualifiedRepairCard.setStatus("waiting_repair");
            }
            /*if (ObjectHelper.isEmpty(zsUnqualifiedRepairCard.getType())) {
                zsUnqualifiedRepairCard.setType("repair_yzy");
            }*/
            if ("repair_yzy".equals(zsUnqualifiedRepairCard.getType())) {
                String inspectFrequen="夜班";
                if(JudgeTime("08:00:00","20:00:00")) {//是不是白班
                    inspectFrequen="白班";
                }
                zsUnqualifiedRepairCard.setClassType(inspectFrequen);
                zsUnqualifiedRepairCard.setStatus("confirm_complete");
            }
            if (this.save(zsUnqualifiedRepairCard)) {
                return Result.ok("添加成功！");
            }
        }

        return Result.error("添加失败！");
    }

    /**
     * 编辑
     */
    @Override
    public Result<?> edit(ZsUnqualifiedRepairCard zsUnqualifiedRepairCard) {
        if (this.updateById(zsUnqualifiedRepairCard)) {
            return Result.ok("编辑成功!");
        }
        return Result.error("编辑失败!");
    }

    /**
     * 通过id删除
     */
    @Override
    public Result<?> delete(String id) {
        if (this.removeById(id)) {
            return Result.ok("删除成功!");
        }
        return Result.error("删除失败!");
    }

    /**
     * 批量删除
     */
    @Override
    public Result<?> deleteBatch(String ids) {
        if (this.removeByIds(Arrays.asList(ids.split(",")))) {
            return Result.ok("批量删除成功!");
        }
        return Result.ok("批量删除失败!");
    }

}
