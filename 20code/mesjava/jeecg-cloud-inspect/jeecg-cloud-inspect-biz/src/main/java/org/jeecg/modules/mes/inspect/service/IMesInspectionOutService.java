package org.jeecg.modules.mes.inspect.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.mes.inspect.entity.MesBodLog;
import org.jeecg.modules.mes.inspect.entity.MesInspectionOut;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mes.inspect.vo.InspectVo;
import org.jeecg.modules.mes.inspect.vo.ReportVo;

import java.util.List;

/**
 * @Description: 出货检验报告
 * @Author: jeecg-boot
 * @Date:   2021-03-20
 * @Version: V1.0
 */
public interface IMesInspectionOutService extends IService<MesInspectionOut> {

    public void saveMain(MesInspectionOut inspectionOut, List<MesBodLog> bodLogs);

    public List<InspectVo> selectLplList (String level, String goal,String begindate, String enddate,String linecode,String procode);

    public List<InspectVo> selectLineLplList (String level,String begindate, String enddate,String linecode,String procode);

    public List<InspectVo> selectLplListByDay (String level, String goal,String begindate, String enddate,String linecode,String procode);

    public List<InspectVo> selectLplListByWeek (String level, String goal,String begindate, String enddate,String linecode,String procode);

    public List<InspectVo> selectLplListByYear (String level, String goal,String begindate, String enddate,String linecode,String procode);

    public List<InspectVo> selectTopIssue (String level,String begindate, String enddate,String linecode,String procode);

    public List<InspectVo> selectPicLoss (String level,String begindate, String enddate,String linecode,String procode);

    public List<InspectVo> selectDeptPicLoss (String level, String dept,String begindate, String enddate,String linecode,String procode);

    //退料单
    public Page<ReportVo> selectTld(Page<ReportVo> page, String begindate);
}
