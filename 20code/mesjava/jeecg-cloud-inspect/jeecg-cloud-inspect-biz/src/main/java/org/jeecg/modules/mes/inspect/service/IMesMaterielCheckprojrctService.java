package org.jeecg.modules.mes.inspect.service;

import org.jeecg.modules.mes.inspect.entity.MesCheckprojectInfo;
import org.jeecg.modules.mes.inspect.entity.MesMaterielCheckprojrct;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 质检中心-物料检测项目
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
public interface IMesMaterielCheckprojrctService extends IService<MesMaterielCheckprojrct> {

	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);


}
