package org.jeecg.modules.mes.inspect.service.impl;

import org.jeecg.modules.mes.inspect.entity.MesCheckprojectInfo;
import org.jeecg.modules.mes.inspect.mapper.MesCheckprojectInfoMapper;
import org.jeecg.modules.mes.inspect.service.IMesCheckprojectInfoService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 检测项目信息
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Service
public class MesCheckprojectInfoServiceImpl extends ServiceImpl<MesCheckprojectInfoMapper, MesCheckprojectInfo> implements IMesCheckprojectInfoService {
	
	@Autowired
	private MesCheckprojectInfoMapper mesCheckprojectInfoMapper;
	
	@Override
	public List<MesCheckprojectInfo> selectByMainId(String mainId) {
		return mesCheckprojectInfoMapper.selectByMainId(mainId);
	}
}
