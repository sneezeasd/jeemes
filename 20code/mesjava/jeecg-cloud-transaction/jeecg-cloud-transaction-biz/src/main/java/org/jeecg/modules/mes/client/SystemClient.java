package org.jeecg.modules.mes.client;

import org.jeecg.common.constant.ServiceNameConstants;
import org.jeecg.modules.system.entity.SysUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Component
@FeignClient(contextId = "SystemServiceClient", value = ServiceNameConstants.SYSTEM_SERVICE)
public interface SystemClient {

    @GetMapping("/sys/role/queryRoleByCode")
    public List<SysUser> queryRoleByCode(@RequestParam(name = "roleCode", required = true) String roleCode);


    @GetMapping("/sys/user/queryUserListByRoleId")
    public List<SysUser> queryUserListByRoleId(@RequestParam(name = "roleId", required = true) String roleId);

    @GetMapping("mesapi/mesapp/sendMaterLackRemind")
    public String sendRemindMessage(@RequestParam(name = "userName", required = true) String userName,
                                    @RequestParam(name = "title", required = true) String title,
                                    @RequestParam(name = "content", required = true) String content);

}
