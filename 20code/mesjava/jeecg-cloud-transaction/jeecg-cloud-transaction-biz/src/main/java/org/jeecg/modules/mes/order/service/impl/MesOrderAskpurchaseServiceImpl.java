package org.jeecg.modules.mes.order.service.impl;

import org.jeecg.modules.mes.order.entity.MesOrderAskpurchase;
import org.jeecg.modules.mes.order.entity.MesAskpurchaseItem;
import org.jeecg.modules.mes.order.mapper.MesAskpurchaseItemMapper;
import org.jeecg.modules.mes.order.mapper.MesOrderAskpurchaseMapper;
import org.jeecg.modules.mes.order.service.IMesOrderAskpurchaseService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 订单管理—请购单
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
@Service
public class MesOrderAskpurchaseServiceImpl extends ServiceImpl<MesOrderAskpurchaseMapper, MesOrderAskpurchase> implements IMesOrderAskpurchaseService {

	@Autowired
	private MesOrderAskpurchaseMapper mesOrderAskpurchaseMapper;
	@Autowired
	private MesAskpurchaseItemMapper mesAskpurchaseItemMapper;
	
	@Override
	@Transactional
	public void delMain(String id) {
		mesAskpurchaseItemMapper.deleteByMainId(id);
		mesOrderAskpurchaseMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesAskpurchaseItemMapper.deleteByMainId(id.toString());
			mesOrderAskpurchaseMapper.deleteById(id);
		}
	}
	
}
