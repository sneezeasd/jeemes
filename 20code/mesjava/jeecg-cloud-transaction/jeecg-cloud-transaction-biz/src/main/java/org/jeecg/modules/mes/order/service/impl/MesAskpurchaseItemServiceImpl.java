package org.jeecg.modules.mes.order.service.impl;

import org.jeecg.modules.mes.order.entity.MesAskpurchaseItem;
import org.jeecg.modules.mes.order.mapper.MesAskpurchaseItemMapper;
import org.jeecg.modules.mes.order.service.IMesAskpurchaseItemService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 订单管理—请购单子表
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
@Service
public class MesAskpurchaseItemServiceImpl extends ServiceImpl<MesAskpurchaseItemMapper, MesAskpurchaseItem> implements IMesAskpurchaseItemService {
	
	@Autowired
	private MesAskpurchaseItemMapper mesAskpurchaseItemMapper;
	
	@Override
	public List<MesAskpurchaseItem> selectByMainId(String mainId) {
		return mesAskpurchaseItemMapper.selectByMainId(mainId);
	}
}
