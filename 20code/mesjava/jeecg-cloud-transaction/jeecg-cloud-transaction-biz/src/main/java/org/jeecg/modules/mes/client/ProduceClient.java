package org.jeecg.modules.mes.client;

import org.jeecg.common.constant.ServiceNameConstants;
import org.jeecg.modules.mes.produce.entity.MesCommandbillInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Component
@FeignClient(contextId = "ProduceServiceClient", value = ServiceNameConstants.PRODUCE_SERVICE)
public interface ProduceClient {

    @GetMapping("/produce/mesCommandbillInfo/queryMesCommandBillInfoByProduceId")
    List<MesCommandbillInfo> queryMesCommandBillInfoByProduceId(@RequestParam(name = "produceIds") List<String> produceIds);

}
