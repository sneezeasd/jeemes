package org.jeecg.modules.mes.order.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.order.entity.MesOrderSale;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 订单管理—销售订单
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
public interface MesOrderSaleMapper extends BaseMapper<MesOrderSale> {

}
