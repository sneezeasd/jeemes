package org.jeecg.modules.mes.order.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.order.entity.ImpApplyMain;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 进口申请单主表
 * @Author: jeecg-boot
 * @Date:   2021-04-07
 * @Version: V1.0
 */
public interface ImpApplyMainMapper extends BaseMapper<ImpApplyMain> {

}
