package org.jeecg.modules.mes.order.service;

import org.jeecg.modules.mes.order.entity.ImpApplyBill;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 进口申请单
 * @Author: jeecg-boot
 * @Date:   2021-04-06
 * @Version: V1.0
 */
public interface IImpApplyBillService extends IService<ImpApplyBill> {



    public List<ImpApplyBill> selectByMainId(String mainId);
}
