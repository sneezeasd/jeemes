package org.jeecg.modules.mes.order.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.mes.order.entity.MesPurchaseItem;
import org.jeecg.modules.mes.order.mapper.MesPurchaseItemMapper;
import org.jeecg.modules.mes.order.service.IMesPurchaseItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description: 订单管理—采购订单子表
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
@Service
public class MesPurchaseItemServiceImpl extends ServiceImpl<MesPurchaseItemMapper, MesPurchaseItem> implements IMesPurchaseItemService {

	@Autowired
	private MesPurchaseItemMapper mesPurchaseItemMapper;

	@Override
	public List<MesPurchaseItem> selectByMainId(String mainId) {
		return mesPurchaseItemMapper.selectByMainId(mainId);
	}

	/**
	 * 更新采购订单子表未收货数量
	 * @param id
	 * @return
	 */
	public boolean uploadUnreceiveNum(String id){
		int num=mesPurchaseItemMapper.uploadUnreceiveNum(id);
		if (num>0){
			return false;
		}else {
			return true;
		}
	}
}
