package org.jeecg.modules.mes.client;

import org.jeecg.common.constant.ServiceNameConstants;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataBom;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataBomitem;
import org.jeecg.modules.mes.chiefdata.entity.MesPrintModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Component
@FeignClient(contextId = "BomServiceClient", value = ServiceNameConstants.SYSTEM_SERVICE)
public interface BomClient {

    @GetMapping("mesapi/mesapp/sendMessage")
    public String sendMessage(@RequestParam(name="userName",required=false) String userName,
                              @RequestParam(name="mName",required=false) String mName);

    @GetMapping("mesapi/mesapp/sendHeatMessage")
    public String sendHeatMessage(@RequestParam(name="userName",required=true) String userName,
                                  @RequestParam(name="orderName",required=true) String orderName,
                                  @RequestParam(name="mName",required=true) String mName);

    @GetMapping("chiefdata/mesChiefdataBom/queryByMcode")
    public MesChiefdataBom queryByMcode(@RequestParam(name="Mcode", required = true) String Mcode);

    /**
     * 根据主表物料料号查询子表信息
     * @param Mcode
     * @return
     */
    @GetMapping("chiefdata/mesChiefdataBom/queryByMcodeZu")
    public List<MesChiefdataBomitem> queryByMcodeZu(@RequestParam(name = "Mcode", required = true) String Mcode);
    /**
     * 通过BOMID查询物料数据
     *
     * @return
     */
    @GetMapping("chiefdata/mesChiefdataBom/queryMesBomitemByMainId")
    public List<MesChiefdataBomitem> queryMesBomitemByMainId(@RequestParam(name = "id", required = true) String id);
    /**
     * 通过BOMID查询半成品物料数据
     *
     * @return
     */
    @GetMapping("chiefdata/mesChiefdataBom/queryMesBomitemBCPMainId")
    public List<MesChiefdataBomitem> queryMesBomitemBCPMainId(@RequestParam(name = "id", required = true) String id);

    /**
     * 根据成品物料id查询bomid，再查询出bomitem数据 远程调用
     * @param materialId
     * @return
     */
    @GetMapping(value = "chiefdata/mesChiefdataBom/selectByMaterialId")
    public List<MesChiefdataBomitem> selectByMaterialId(@RequestParam(name = "materialId", required = true)String materialId);

    @GetMapping("mesapi/mesapp/getdocketcode")
    public String getdocketcode(@RequestParam(name="docketype",required=false) String docketype);

    @GetMapping("chiefdata/mesPrintModel/queryByPrintType")
    public MesPrintModel queryByPrintType(@RequestParam(name="printType",required=true) String printType);

}

